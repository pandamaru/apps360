<?php
/**
 * Food Controller class for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\controllers;
use \menu360\models as model;

class Food extends Controller
{
	private $self;
	private $r_model;
	private $f_model;
	private $igd_model;
	private $obj = null;

	public function __construct()
	{
		$this->r_model = new model\Restaurant;
		$this->f_model = new model\Food;
		$this->igd_model = new model\Ingredient;
	}

	public static function search_by_restaurant()
	{
		self::_check();
		$obj = new self;
		$rs_by_restaurant = $obj->_search_by_restaurant();
	}

	private function _search_by_restaurant()
	{
		$search = array(
					'restaurant_id' => $_POST['RestaurantID'],
					'lang' => $_POST['LanguageID']
				);
		$results = $this->f_model->search_by_restaurant($search);
		echo '<pre>';
		print_r($results);
		echo '</pre>';
	}

	private static function _check()
	{
		if( empty($_POST) )
		{
			self::raise_error();
		}
		if( !isset($_POST['LanguageID']) || empty($_POST['LanguageID']))
		{
			self::raise_error('empty LanguageID');
		}
		if( !isset($_POST['RestaurantID']) || empty($_POST['RestaurantID']) )
		{
			self::raise_error('empty RestaurantID');
		}

		foreach($_POST as $k=>$v)
		{
			$_POST[$k] = htmlspecialchars($v);
		}
	}


}