<?php
/**
 * Language Controller class for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\controllers;
use \menu360\models as model;

class Language extends Controller
{
	private $self;
	private $l_model = null;
	/*
	*getting the instance of Language model
	*/
	public function __construct()
	{
		$this->l_model = new model\Language;
	}

	/*
	* method for getting all available language called by
	*/
	public static function available_langs()
	{
		$obj = new self;
		$results = $obj->l_model->get_available_language();
		echo json_encode($results);	
	}
}