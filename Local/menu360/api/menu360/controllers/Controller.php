<?php
/**
 * a Simple Controller class for Apps360 ( menu360 system )
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\controllers;

abstract class Controller{

	function __construct(){}

	/*
	* method to raise the error message
	*/
	function raise_error($msg = '')
	{
		if( empty($msg) )
		{
			$msg = "undefined error";
		}
		echo json_encode(array('error' => $msg));
		exit();
	}

	/*
	* method to render the 404 page not found 
	*/
	function show_404()
	{
		echo json_encode(array('error' => '404 NOT FOUND.'));
		exit();
	}
}