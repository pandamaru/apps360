<?php
/**
 * Restaurant Controller class for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\controllers;
use \menu360\models as model;

class Restaurant extends Controller
{
	private $self;
	private $r_model;
	private $f_model;
	private $igd_model;

	/*
	* creating an instance of a relavant model on a construction method
	*/
	public function __construct()
	{
		/*instance of Restaurant model*/
		$this->r_model = new model\Restaurant;
		/*instance of Food model*/
		$this->f_model = new model\Food;
		/*instance of Ingredient model*/
		$this->igd_model = new model\Ingredient;
	}

	/*
	* the main search by keyword function 
	*/
	public static function search_keyword()
	{
		/* check for the required fields*/
		self::_by_keyword_check();
		/*create a self instance*/
		$obj = new self;
		/* call search function and getting the result from the model */
		$rs_name = $obj->_search_by_name();
		$rs_food = $obj->_search_by_food();
		$rs_ingredient = $obj->_search_by_ingredient();

		/*
		*formatting the data
		*/
		$results = array();
		$added = array();
		if( count($rs_name) )
		{
			foreach($rs_name as $k=>$row)
			{
				$results[] = $row;
				$added[] = $row['restaurant_id'];
			}
		}
		if( count($rs_food) )
		{
			foreach($rs_food as $k=>$row)
			{
				if( !in_array($row['restaurant_id'], $added) )
				{
					$results[] = $row;
				}
			}
		}

		if( count($rs_ingredient) )
		{
			foreach($rs_ingredient as $k=>$row)
			{
				if( !in_array($row['restaurant_id'], $added) )
				{
					$results[] = $row;
				}
			}
		}

		/* send restaurant data to the caller */
		echo json_encode($results);
	}
	
	/*
	* a main search by map or loation ( search by map-bound )
	*/
	function search_map()
	{
		/* check the required fields */
		self::_by_map_check();
		$obj = new self;
		$search = array(
					'min_lat' => $_POST['MinLatitude'],
					'min_lng' => $_POST['MinLongitude'], 
					'max_lat' => $_POST['MaxLatitude'], 
					'max_lng' => $_POST['MaxLongitude'], 
					'lang' => $_POST['LanguageID']
				);
		/* calling the model to perform the search function */
		$rs_maps = $obj->r_model->search_by_mapbound($search);
		/*
		*formatting the result to output
		*/
		$results = array();
		foreach($rs_maps as $k=>$row)
		{
				$results[] = $row;
		}

		/*sending the result to caller*/
		echo json_encode($results);
	}

	/*
	* for Auto-complete feature
	* getting all possible restaurant name
	*/
	public static function search_restaurant()
	{
		self::_search_restaurant();
		/*create a self instance*/
		$obj = new self;
		/* call search function and getting the result from the model */
		$search = array('name' => $_POST['SearchText'], 'search_lang' => $_POST['SearchLang']);
		$results = $obj->r_model->search_restaurant_name($search);
		echo json_encode($results);
	}

	private static function _search_restaurant()
	{
			if( empty($_POST) )
		{
			self::raise_error();
		}
		if( !isset($_POST['SearchText']) || empty($_POST['SearchText']) )
		{
			self::raise_error();
		}
		if( !isset($_POST['SearchLang']) || empty($_POST['SearchLang']) )
		{
			$_POST['SearchLang'] = $_POST['LanguageID'];
		}

		foreach($_POST as $k=>$v)
		{
			$_POST[$k] = htmlspecialchars($v);
		}
	}

	/*
	* sub-function of search-by-restaurant name
	*/
	private function _search_by_name()
	{
		$search = array('name' => $_POST['SearchText'], 'lang' => $_POST['LanguageID'], 'search_lang' => $_POST['SearchLang']);
		$results = $this->r_model->search_by_name($search);

		return $results;
	}

	/*
	* sub-function of search-by-food-name
	*/
	private function _search_by_food()
	{
		$search = array('name' => $_POST['SearchText'], 'lang' => $_POST['LanguageID'], 'search_lang' => $_POST['SearchLang']);
		$results = $this->r_model->search_by_food($search);
		return $results;
	}

	/*
	* sub-function of search-by-ingredient-name
	*/
	private function _search_by_ingredient()
	{
		$search = array('name' => $_POST['SearchText'], 'lang' => $_POST['LanguageID'], 'search_lang' => $_POST['SearchLang']);
		$results = $this->r_model->search_by_ingredient($search);
		return $results;
	}

	/*
	* to perform checking all require fields for search_by_keyword()
	*/
	private static function _by_keyword_check()
	{
		if( empty($_POST) )
		{
			self::raise_error();
		}
		if( !isset($_POST['LanguageID']) || empty($_POST['LanguageID']))
		{
			self::raise_error();
		}
		if( !isset($_POST['SearchText']) || empty($_POST['SearchText']) )
		{
			self::raise_error();
		}
		if( !isset($_POST['SearchLang']) || empty($_POST['SearchLang']) )
		{
			$_POST['SearchLang'] = $_POST['LanguageID'];
		}

		foreach($_POST as $k=>$v)
		{
			$_POST[$k] = htmlspecialchars($v);
		}
	}

	/*
	* to perform checking all required field for search_by_map or loation
	*/
	private static function _by_map_check()
	{
		if( empty($_POST) )
		{
			self::raise_error();
		}
		if( !isset($_POST['LanguageID']) || empty($_POST['LanguageID']))
		{
			self::raise_error('empty LanguageID');
		}
		if( !isset($_POST['MinLatitude']) || empty($_POST['MinLatitude']) )
		{
			self::raise_error('empty MinLatitude');
		}
		if( !isset($_POST['MinLongitude']) || empty($_POST['MinLongitude']) )
		{
			self::raise_error('empty MinLongitude');
		}
		if( !isset($_POST['MaxLatitude']) || empty($_POST['MaxLatitude']) )
		{
			self::raise_error('empty MaxLatitude');
		}
		if( !isset($_POST['MaxLongitude']) || empty($_POST['MaxLongitude']) )
		{
			self::raise_error('empty MaxLongitude');
		}

		foreach($_POST as $k=>$v)
		{
			$_POST[$k] = htmlspecialchars($v);
		}
	}


}
