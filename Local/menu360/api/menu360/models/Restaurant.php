<?php
/**
 * Restauratn Model for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\models;

class Restaurant extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function search_by_name($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}

		$_sql = " SELECT "
					." r.id "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." rdesc.name LIKE '%{$search['name']}%' "
					." AND rdesc.language_id = '{$search['search_lang']}' "
				." ";
		$this->db->query($_sql);
		$_finds = $this->db->fetch_all();
		$restaurants = array();
		foreach($_finds as $row)
		{
			$restaurants[] = $row->id;
		}
		
		$restaurants = implode(",", $restaurants);

		$sql = "SELECT "
					." r.restaurant_type_id, r.latitude, r.longitude, "
					." r.postcode, r.url, r.phone, r.fax, "
					." rdesc.restaurant_id, rdesc.name, rdesc.address, "
					." rdesc.sub_district, rdesc.district, rdesc.province, "
					." rdesc.country "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." rdesc.restaurant_id IN({$restaurants}) "
					." AND rdesc.language_id = '{$search['lang']}' "
				." ";
		$this->db->query($sql);

		$datas = array();
		$_datas = $this->db->fetch_all();
		foreach($_datas as $row)
		{
			$datas[] = array(
				            'restaurant_type_id' => $row->restaurant_type_id,
				            'latitude' => $row->latitude,
				            'longitude' => $row->longitude,
				            'postcode' => $row->postcode,
				            'url' => $row->url,
				            'phone' => $row->phone,
				            'fax' => $row->fax,
				            'restaurant_id' => $row->restaurant_id,
				            'name' => $row->name,
				            'address' => $row->address,
				            'sub_district' => $row->sub_district,
				            'district' => $row->district,
				            'province' => $row->province,
				            'country' => $row->country
				);
		}

		return $datas;
	}

	public function search_by_food($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}
		

		$sql = "SELECT "
					." fdesc.food_id, f.restaurant_id "
				." FROM food_desc fdesc "
				." INNER JOIN food f ON f.id = fdesc.food_id " 
					." WHERE "
						." fdesc.name LIKE '%{$search['name']}%' "
						." AND fdesc.language_id = '{$search['search_lang']}' "
				." ";
		$foods = $this->db->query($sql);

		$food_array = array();
		foreach($this->db->fetch_all() as $k=>$frow)
		{
			$food_array[] = $frow->restaurant_id;
		}

		$sql = "SELECT "
					." r.restaurant_type_id, r.latitude, r.longitude, "
					." r.postcode, r.url, r.phone, r.fax, "
					." rdesc.restaurant_id, rdesc.name, rdesc.address, "
					." rdesc.sub_district, rdesc.district, rdesc.province, "
					." rdesc.country "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." r.id IN(".implode(",", $food_array).") "
					." AND rdesc.language_id = '{$search['lang']}' "
				." ";
		$results = $this->db->query($sql);

		$datas = array();
		$_datas = $this->db->fetch_all();
		foreach($_datas as $row)
		{
			$datas[] = array(
				            'restaurant_type_id' => $row->restaurant_type_id,
				            'latitude' => $row->latitude,
				            'longitude' => $row->longitude,
				            'postcode' => $row->postcode,
				            'url' => $row->url,
				            'phone' => $row->phone,
				            'fax' => $row->fax,
				            'restaurant_id' => $row->restaurant_id,
				            'name' => $row->name,
				            'address' => $row->address,
				            'sub_district' => $row->sub_district,
				            'district' => $row->district,
				            'province' => $row->province,
				            'country' => $row->country
				);
		}
		return $datas;
	}

	function search_by_ingredient($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}

		$sql = "SELECT "
					." igdesc.name, igdesc.ingredient_id "
				." FROM ingredient_desc igdesc "
				." INNER JOIN ingredient ig ON igdesc.ingredient_id = ig.id "
				." WHERE "
					." ig.main_flag = 1 "
					." AND igdesc.name LIKE '%{$search['name']}%' "
					." AND igdesc.language_id = {$search['search_lang']} "
				." ";

		$this->db->query($sql);

		if( $this->db->count() <= 0 )
		{
			return array();
		}

		$data = $this->db->fetch_all();
		$ingredients = array();
		foreach($data as $k=>$row)
		{
			$ingredients[] = $row->ingredient_id;
		}

		$ig_arr = implode(",", $ingredients);

		$sql = "SELECT "
					." fig.ingrediet_id, fig.food_id, "
					." f.restaurant_id "
				." FROM food_ingredient fig "
				." INNER JOIN food f ON f.id = fig.food_id " 
					." WHERE "
						." fig.ingredient_id IN({$ig_arr}) "
				." ";
		$foods = $this->db->query($sql);

		$food_array = array();
		foreach($this->db->fetch_all() as $k=>$frow)
		{
			$food_array[] = $frow->restaurant_id;
		}

		$sql = "SELECT "
					." r.restaurant_type_id, r.latitude, r.longitude, "
					." r.postcode, r.url, r.phone, r.fax, "
					." rdesc.restaurant_id, rdesc.name, rdesc.address, "
					." rdesc.sub_district, rdesc.district, rdesc.province, "
					." rdesc.country "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." r.id IN(".implode(",", $food_array).") "
					." AND rdesc.language_id = '{$search['lang']}' "
				." ";
		$results = $this->db->query($sql);

		$datas = array();
		$_datas = $this->db->fetch_all();
		foreach($_datas as $row)
		{
			$datas[] = array(
				            'restaurant_type_id' => $row->restaurant_type_id,
				            'latitude' => $row->latitude,
				            'longitude' => $row->longitude,
				            'postcode' => $row->postcode,
				            'url' => $row->url,
				            'phone' => $row->phone, 
				            'fax' => $row->fax,
				            'restaurant_id' => $row->restaurant_id,
				            'name' => $row->name,
				            'address' => $row->address,
				            'sub_district' => $row->sub_district,
				            'district' => $row->district,
				            'province' => $row->province,
				            'country' => $row->country
				);
		}
		return $datas;

	}

	function search_by_mapbound($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}
		
		$sql = "SELECT "
					." r.restaurant_type_id, r.latitude, r.longitude, "
					." r.postcode, r.url, r.phone, r.fax, "
					." rdesc.restaurant_id, rdesc.name, rdesc.address, "
					." rdesc.sub_district, rdesc.district, rdesc.province, "
					." rdesc.country "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." ( r.latitude BETWEEN {$search['min_lat']} AND {$search['max_lat']} ) "
					." AND ( r.longitude BETWEEN {$search['min_lng']} AND {$search['max_lng']} ) "
					." AND rdesc.language_id = '{$search['lang']}' "
				." ";

		$this->db->query($sql);

		$datas = array();
		$_datas = $this->db->fetch_all();
		foreach($_datas as $row)
		{
			$datas[] = array(
				            'restaurant_type_id' => $row->restaurant_type_id,
				            'latitude' => $row->latitude,
				            'longitude' => $row->longitude,
				            'postcode' => $row->postcode,
				            'url' => $row->url,
				            'phone' => $row->phone,
				            'fax' => $row->fax,
				            'restaurant_id' => $row->restaurant_id,
				            'name' => $row->name,
				            'address' => $row->address,
				            'sub_district' => $row->sub_district,
				            'district' => $row->district,
				            'province' => $row->province,
				            'country' => $row->country
				);
		}

		return $datas;

	}

	/*
	* AUTO-COMPLETE 
	* search for restaurant by checking atleast 4 letters
	*/
	function search_restaurant_name($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}

		/* by restaurant name */
		if( strlen($search['name']) >= 3 ) 
		{
			$_sql = " SELECT "
						." r.id, "
						."rdesc.name "
					." FROM restaurant r "
					." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
					." WHERE "
						." rdesc.name LIKE '%{$search['name']}%' "
						." OR rdesc.sub_district LIKE '%{$search['name']}%' "
						." OR rdesc.district LIKE '%{$search['name']}%' "
						." OR rdesc.province LIKE '%{$search['name']}%' "
						." OR rdesc.country LIKE '%{$search['name']}%' "
						." AND rdesc.language_id = '{$search['search_lang']}' "
					." LIMIT 10 "
					." ";
			$this->db->query($_sql);
			$_finds = $this->db->fetch_all();
			$added_r = array();
			$restaurants = array();
			$i = 0;

			$restaurants['search_for'] = $search['name'];
			foreach($_finds as $row)
			{
				$restaurants['results'][$i]['id'] = $row->id;
				$restaurants['results'][$i]['name'] = $row->name;
				$restaurants['results'][$i]['type'] = 'restaurant';
				$added_r[] = $row->id;
				$i++;
			}


			/*
			* by food name
			*/
			$sql = "SELECT "
						." fdesc.food_id, f.restaurant_id "
					." FROM food_desc fdesc "
					." INNER JOIN food f ON f.id = fdesc.food_id " 
						." WHERE "
							." fdesc.name LIKE '%{$search['name']}%' "
							." AND fdesc.language_id = '{$search['search_lang']}' "
					." ";
			$foods = $this->db->query($sql);

			$food_array = array();
			foreach($this->db->fetch_all() as $k=>$frow)
			{
				$food_array[] = $frow->restaurant_id;
			}

			$added_r_txt = implode(",", $added_r);

			$sql = "SELECT "
						." r.id, r.name  "
					." FROM restaurant r "
					." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
					." WHERE "
						." r.id IN(".implode(",", $food_array).") "
						." AND rdesc.language_id = '{$search['search_lang']}' "
						." AND r.id NOT IN({$added_r_txt}) "
					." LIMIT 10 "
					." ";
			$f_results = $this->db->query($sql);

			$datas = array();
			$_datas = $this->db->fetch_all();

			foreach($_datas as $row)
			{
				if( !in_array($row->id, $added_r))
				{
					$restaurants['results'][$i]['id'] = $row->id;
					$restaurants['results'][$i]['name'] = $row->name;
					$restaurants['results'][$i]['type'] = 'food';
					$added_r[] = $row->id;

					if( $i >= 10 ) 
					{
						return $restaurants;
					}
					
					$i++;
				}
			}

			return $restaurants;


		}


	}

	public function search_by_map($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

	public function search_by_type($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

}
