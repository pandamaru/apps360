<?php
/**
 *Language Model for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\models;

class Language extends Model
{
	/*
	* a construction method
	*/
	public function __construct(){parent::__construct();}

	/*
	* get all available languages for menu360
	*/
	public function get_available_language()
	{
		$sql = "SELECT id, name, short_name, thumbnail, used_flag FROM language WHERE used_flag = 1";
		$this->db->query($sql);
		$results = $this->db->fetch_all();
		$langs = array();
		foreach($results as $row)
		{
			$langcode = strtolower($row->short_name);
			$langs[$langcode] = array('id' => $row->id, 'name' => $row->name, 'short_name' => $row->short_name);
		}

		return $langs;
	}

}