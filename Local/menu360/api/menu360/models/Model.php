<?php
/**
 * a Simple Model class for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\models;

abstract class Model 
{	
	protected $db;
	/*
	* connect to DB when every model has been invoked
	*/
	function __construct()
	{
		global $config;
		$this->db = \menu360\libs\DB::get_instance();//($config);
		$this->db->set_all($config);
		$this->db->connect();
	}

}