<?php
/**
 *Food Model for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\models;

class Food extends Model
{
	public function __construct(){parent::__construct();}

	public function search_by_restaurant($search = null)
	{
		if( is_null($search) || empty($search) )
		{
			return array();
		}

		$sql = "SELECT "
				." f.restaurant_id, f.prices, f.native_language_id, "
				." fd.food_id, fd.language_id, fd.name, fd.description, fd.food_special, fd.cuisine, fd.currency "
			." FROM food_desc fd "
			." INNER JOIN food f ON f.id = fd.food_id "
			." WHERE "
				." fd.language_id = {$search['lang']} "
				." AND f.restaurant_id = {$search['restaurant_id']} "
			. "";
			echo $sql;
			echo '<br/><br/>';
		$this->db->query($sql);
		$results = $this->db->fetch_all();
		$foods = array();
		foreach($results as $k=>$row)
		{
			$fid = $row->food_id;
			$foods['foods'] = array(
								"{$fid}" => array(
										'restaurant_id' => $row->restaurant_id,
										'food_prices' => $row->prices,
										'currency' => $row->currency, 
										'native_language' => $row->native_language_id,
										'food_id' => $row->food_id,
										'language_id' => $row->language_id,
										'name' => $row->name,
										'description' => $row->description,
										'food_special' => $row->food_special,
										'food_cuisine' => $row->cuisine
									)
							);
			$sql = "SELECT ingredient_id, name FROM ingredient_desc WHERE language_id = {$search['lang']} AND ";
			echo $sql;
			$this->db->query($sql);
			$ing_results = $this->db->fetch_all();
			foreach($ing_results as $row)
			{
				$foods['foods'][$fid]['ingrediants'][] = $row->name;
			}
		}

		return $foods;

	}
	public function search_by_name($search = null)
	{
		$sql = "";
	}

	public function search_by_map($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

	public function search_by_type($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

}