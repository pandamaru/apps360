<?php
/**
 * a Simple DB class for Apps360
 *
 * @author      Chaiyut P <chaiyut.p@apps360.co.th>
 */
namespace menu360\libs;

class DB{

	protected $_db;
	private $db_host;
	private $db_user;
	private $db_password;
	private $db_name;
	private $db_type = "mysql"; //only mysql for now

	protected $result;

	/*
	constructor 
	*/
	public function __construct($config = NULL)
	{
		if( !is_null($config) || !empty($config) )
		{
			$this->set_all($config);
		}
	}

	/*
	* singleton method
	*/
	public static function get_instance()
	{
		return isset(static::$_db) ? static::$_db:new self;
	}

	/*
	* connect to the DB server
	*/
	function connect()
	{
		try 
		{
			$this->_db = new \PDO("{$this->db_type}:host={$this->db_host};dbname={$this->db_name}", $this->db_user, $this->db_password);
			$this->_db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ); 
			$this->_db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ); 
		}
		catch(PDOException $e) 
		{
			echo $e->getMessage();
		}
	}

	/*
	* query the SQL command both by prepare statement or not, depends on the params
	*/
	public function query($sql, $params = NULL)
	{
		$x = $this->_db->prepare("SET NAMES UTF8");
		$x->execute();

		$this->result = $this->_db->prepare($sql);
		if(!is_null($params) || !empty($params) )
		{
			$this->result->execute($params);
		}
		else
		{
			$this->result->execute();
		}
	}

	/*
	* fetch single record from the DB
	*/
	public function fetch()
	{
		$result = $this->result->fetchObject();
		return $result;
	}

	/*
	* fetch all records from the DB
	*/
	public function fetch_all()
	{
		$result = $this->result->fetchAll();
		return $result;
	}

	/*
	* count all records affected by the command
	*/
	function count()
	{
		return $this->result->rowCount();
	}

	/*
	* set DB's host name
	*/
	public function set_db_host($host = NULL)
	{
		if( is_null($host) || empty($host) )
		{
			return false;
		}
		$this->db_host = $host;
		return true;
	}

	/*
	* set DB's user
	*/
	public function set_db_user($user = NULL)
	{
		if( is_null($user) || empty($user) )
		{
			return false;
		}
		$this->db_user= $user;
		return true;
	}

	/*
	* set DB's password
	*/
	public function set_db_password($password = NULL)
	{
		if( is_null($password) || empty($password) )
		{
			return false;
		}
		$this->db_password = $password;
		return true;
	}

	/*
	* set DB's name
	*/
	public function set_db_name($dbname = NULL)
	{
		if( is_null($dbname) || empty($dbname) )
		{
			return false;
		}
		$this->db_name = $dbname;
		return true;
	}

	/*
	* set all configurations for connecting server
	*/
	public function set_all($config = array())
	{
		$accept = array('db_host', 'db_user','db_password', 'db_name');
		if( empty($config) || is_null($config) )
		{
			return false;
		}
		foreach($config as $k=>$v)
		{
			$_k = strtolower($k);
			if( in_array($_k, $accept) )
			{
				$this->{$_k} = $v;
			}
		}
		return true;
	}
}