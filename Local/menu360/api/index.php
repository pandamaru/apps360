<?php
require 'Slim/Slim.php';
require 'menu360/configs.php';

use \menu360\controllers as controllers;
use \menu360\libs as libs;

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

/*
* the root path of the API, we have nothing
*/
$app->get('/',function (){echo json_encode(array('name' => 'The Menu360 API'));});
/*
* the API for getting all available language for Menu360's system
*/
$app->get('/get_available_language', function(){controllers\Language::available_langs();});
/*
* the API for searching available restaurant via keywords( restaurants, foods, ingrediant )
*/
$app->post('/search_by_keyword',function(){ controllers\Restaurant::search_keyword(); });
/*
* the API for searching available restaurant by giving 2 point of latitude-longitude
*/
$app->post('/search_by_location',function(){ controllers\Restaurant::search_map(); });
/*
* the API for searching menu in the restaurant
*/
$app->post('/get_menu_by_restaurant', function(){ controllers\Food::search_by_restaurant();});


/*
* AUTO-COMPLLETE
* the API for getting the restaurant name (for auto-complete )
*/
$app->post('/search_restaurant',function(){ controllers\Restaurant::search_restaurant();});



/*
$app->get('/restaurants/:lang',function(){});
$app->post('/restaurant/:lang/:id',function(){});

$app->get('/foods/:lang',function(){});
$app->get('/food/:lang/:id',function(){});
*/
$app->get('/test', function(){});



$app->run();

