<html>
	<head>
		<title></title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.20"></script>
		<?php 
			$test_on = 'dev';
			$url = "http://192.168.1.110/menu360/api/";
			$api = 'search_by_keyword';
			
			$testtxt = 'resort';
			$search_lang = 1;

			$MinLatitude = $MinLongitude = $MaxLatitude = $MaxLongitude = '';

			$post_fields = array('SearchText'=>$testtxt,'LanguageID'=>1, 'SearchLang'=>$search_lang);
			if( $_POST )
			{
				$testtxt = $_POST['testtxt'];
				$search_lang = $_POST['search_lang'];

				$test_on = $_POST['test_on'];
				$url = ($test_on == 'yuth')?"http://yuth/menu360api/":$url;
				$api = $_POST['api'];

				$post_fields = array('SearchText'=>$testtxt,'LanguageID'=>1, 'SearchLang'=>$search_lang);

				if( $api == "search_by_location")
				{
					$MinLatitude = isset($_POST['MinLatitude'])?$_POST['MinLatitude']:'';
					$MinLongitude = isset($_POST['MinLongitude'])?$_POST['MinLongitude']:'';
					$MaxLatitude = isset($_POST['MaxLatitude'])?$_POST['MaxLatitude']:'';
					$MaxLongitude = isset($_POST['MaxLongitude'])?$_POST['MaxLongitude']:'';
					$post_fields = array(
										'MinLatitude'=>$MinLatitude,
										'MinLongitude'=>$MinLongitude,
										'MaxLatitude'=>$MaxLatitude,
										'MaxLongitude'=>$MaxLongitude,
										'LanguageID'=>1
									);
				}
			}

			$ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url.$api); 
            curl_setopt($ch, CURLOPT_HEADER, false); 


            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
			curl_setopt($ch, CURLOPT_POST, 1 );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			

            $return_data = curl_exec($ch); 
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
            curl_close($ch); 
		?>
		<script type="text/javascript">
			$(function(){
				var jsondata = <?php echo $return_data;?>;
				var total_data = jsondata.length;
				console.log( total_data );
				initialize = function() {
					var mapOptions = {
						zoom: 3,
						center: new google.maps.LatLng(0, 50)
					};
					
					var sw = new google.maps.LatLng(-31.203405, 125.244141);
					var ne = new google.maps.LatLng(-25.363882, 131.044922);

					var bounds = new google.maps.LatLngBounds(sw, ne);
					console.log( bounds );
					

					var map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
					for(var i = 0; i < total_data; i++)
					{
						var latlng = new google.maps.LatLng(jsondata[i].latitude,jsondata[i].longitude);
						//console.log( latlng );
						var marker = new google.maps.Marker({
							position: latlng,
							map: map,
							title: jsondata[i].name
						});
					}
					
				}();
			});
		</script>
		<style type="text/css">

		</style>
	</head>
	<body>
		<div id="map_canvas" style="width: 100%; height: 100%"></div>
		<div style="bottom:0px;right:0px;border:1px solid #000000;position:fixed;padding:10px;">
		<form method="post" action="">
			<table style="border:1px solid #cccccc;">
				<tr>
					<th colspan="2" style="background-color:#cccccc;">menu360 API</th>
				</tr>
				<tr>
					<th>Test on</th>
					<td>
						<input type="radio" name="test_on" id="test_yuth" value="yuth" <?php echo ($test_on=='yuth')?"checked='checked'":"";?>/>
						<label for="test_yuth">Yuth</label>
						<br/>
						<input type="radio" name="test_on" id="test_dev" value="dev" <?php echo ($test_on=='dev')?"checked='checked'":"";?>/>
						<label for="test_dev">192.168.1.110</label>
						<hr/>
					</td>
				</tr>
				<tr>
					<th>API</th>
					<td>
						<input type="radio" name="api" id="searcy_by_keyword" value="search_by_keyword" <?php echo ($api=='search_by_keyword')?"checked='checked'":"";?>/>
						<label for="search_by_keyword">Search by keyword</label>
						<br/>
						<label for="testtxt" style='width:40px;float:left;'>find</label>
						<input type="text" name="testtxt" id="testtxt" value="<?php echo $testtxt;?>" />
						<br/>
						<label for="search_lang" style='width:40px;float:left;'>lang</label>
						<input type="text" name="search_lang" id="search_lang" value="<?php echo $search_lang;?>" />
						<hr/>
						<input type="radio" name="api" id="search_by_location" value="search_by_location"  <?php echo ($api=='search_by_location')?"checked='checked'":"";?>/>
						<label for="searcy_by_location">Search by location</label>
						<br/>
						<label for="MinLatitude" style='width:70px;float:left;'>MinLat.</label>
						<input type="text" name="MinLatitude" id="MinLatitude" value="<?php echo $MinLatitude;?>" />
						<br/>
						<label for="MinLongitude" style='width:70px;float:left;'>MinLng.</label>
						<input type="text" name="MinLongitude" id="MinLongitude" value="<?php echo $MinLongitude;?>" />
						<br/>
						<label for="MaxLatitude" style='width:70px;float:left;'>MaxLat.</label>
						<input type="text" name="MaxLatitude" id="MaxLatitude" value="<?php echo $MaxLatitude;?>" />
						<br/>
						<label for="MaxLongitude" style='width:70px;float:left;'>MaxLng.</label>
						<input type="text" name="MaxLongitude" id="MaxLongitude" value="<?php echo $MaxLongitude;?>" />
						<hr/>
						<?php /*<input type="radio" name="api" id="searcy_by_qrcode" value="searcy_by_qrcode"  <?php echo ($api=='searcy_by_qrcode')?"checked='checked'":"";?>/>
						<label for="searcy_by_qrcode">Search by QR Code</label>*/?>
					</td>
				</tr>
				<tr>
					<th>Language</th>
					<td>
						<select name="language" id="language">
							<option value="1">1</option>
						</select>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" value="submit"/>
					</td>
				</tr>
			</table>
		</form>
		</div>
	</body>
</html>
