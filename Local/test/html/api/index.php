<?php
require 'Slim/Slim.php';
require 'menu360/configs.php';

use \menu360\controllers as controllers;
use \menu360\libs as libs;

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/',function (){echo json_encode(array('name' => 'The Menu360 API'));});

$app->post('/search_by_keyword',function(){
		controllers\Restaurant::search();
	}
);
$app->post('/search_by_map',function(){controllers\Restaurant::search_by_map();});

$app->get('/restaurants/:lang',function(){});
$app->get('/restaurant/:lang/:id',function(){});

$app->get('/foods/:lang',function(){});
$app->get('/food/:lang/:id',function(){});

$app->get('/test', function(){});



$app->run();

