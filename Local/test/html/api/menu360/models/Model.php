<?php

namespace menu360\models;

abstract class Model 
{	
	protected $db;
	function __construct()
	{
		global $config;
		$this->db = \menu360\libs\DB::get_instance();//($config);
		$this->db->set_all($config);
		$this->db->connect();
	}

}