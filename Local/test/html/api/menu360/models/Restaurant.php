<?php

namespace menu360\models;

class Restaurant extends Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function search_by_name($search = null)
	{
		$sql = "SELECT "
					." r.restaurant_type_id, r.latitude, r.longitude, "
					." r.postcode, r.url, "
					." rdesc.restaurant_id, rdesc.name, rdesc.address, "
					." rdesc.sub_district, rdesc.district, rdesc.province, "
					." rdesc.country "
				." FROM restaurant r "
				." INNER JOIN restaurant_desc rdesc ON r.id = rdesc.restaurant_id "
				." WHERE "
					." rdesc.name like '%{$search['name']}%' "
					." AND rdesc.language_id = '{$search['lang']}' "
				." ";
		//$results = $this->db->query($sql, array(':name' => $search['name'], ':lang' => $search['lang']));
		$results = $this->db->query($sql);

		/*
		//demo
		$results = array(
				'restuarant_type_id' => '1',
				'latitude' => '100.0000001',
				'longitude' => '13.00000001',
				'postcode' => '12345',
				'url' => 'http://www.eatme.com',
				'restaurant_id' => '1',
				'name' => 'Eat Me!',
				'address' => '78 7B Rishi Court',
				'sub_district' => 'Nana',
				'district' => 'Wattana',
				'province' => 'Bangkok',
				'country' => 'Thailand'
			);
		*/
		$datas = array();
		$_datas = $this->db->fetch_all();
		foreach($_datas as $row)
		{
			$datas[] = array(
				            'restaurant_type_id' => $row->restaurant_type_id,
				            'latitude' => $row->latitude,
				            'longitude' => $row->longitude,
				            'postcode' => $row->postcode,
				            'url' => $row->url,
				            'restaurant_id' => $row->restaurant_id,
				            'name' => $row->name,
				            'address' => $row->address,
				            'sub_district' => $row->sub_district,
				            'district' => $row->district,
				            'province' => $row->province,
				            'country' => $row->country
				);
		}
		return $datas;
	}

	public function search_by_food($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

	public function search_by_map($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

	public function search_by_type($search = null)
	{
		return array('func'=>__FUNCTION__);
	}

}