<?php

namespace menu360\controllers;
use \menu360\models as model;

class Restaurant extends Controller
{
	private $self;
	private $r_model;
	private $f_model;
	private $igd_model;

	public function __construct()
	{
		$this->r_model = new model\Restaurant;
		$this->f_model = new model\Food;
		$this->igd_model = new model\Ingredient;
	}

	public static function search()
	{
		self::_check();
		$obj = new self;
		$rs_name = $obj->_search_by_name();
		//$rs_food = $obj->_search_by_food();

		$results = array();
		foreach($rs_name as $k=>$row)
		{
			$results[] = $row;
		}

		echo json_encode($results);
	}

	private function _search_by_name()
	{
		$search = array('name' => $_POST['SearchText'], 'lang' => $_POST['LanguageID']);
		$results = $this->r_model->search_by_name($search);

		return $results;
	}

	private function _search_by_food()
	{
		$results = $this->r_model->search_by_food();
		return $results;
	}

	private static function _check()
	{
		if( empty($_POST) )
		{
			self::raise_error();
		}
		if( !isset($_POST['LanguageID']) || empty($_POST['LanguageID']))
		{
			self::raise_error();
		}
		if( !isset($_POST['SearchText']) || empty($_POST['SearchText']) )
		{
			self::raise_error();
		}

		foreach($_POST as $k=>$v)
		{
			$_POST[$k] = htmlspecialchars($v);
		}
	}


}