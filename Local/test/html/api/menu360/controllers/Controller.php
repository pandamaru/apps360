<?php
namespace menu360\controllers;

abstract class Controller{

	function __construct(){}

	function raise_error()
	{
		echo json_encode(array('error' => 'Menu360 API Error'));
		exit();
	}

	function show_404()
	{
		echo json_encode(array('error' => '404 NOT FOUND.'));
		exit();
	}
}