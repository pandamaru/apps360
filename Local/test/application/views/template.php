<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		
		<title>Eat360</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" href="<?php echo site_url('asset/css/reset.css') ?>">
		<link rel="stylesheet" href="<?php echo site_url('asset/css/style.css') ?>">
		<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
		
		<script src="<?php echo site_url('asset/js/instafilta.js') ?>"></script>
		<script src="<?php echo site_url('asset/js/gmaps.js') ?>"></script>
		<style type="text/css" >
			#map-canvas {
		        height: 100%;
		        margin: 0px;
		        padding: 0px
		      }
			#map-canvas #searchMap{
		        width: 70%;  
		        margin-top: 4.95%;
		      }
		</style>
		<script>
			$(function() {
				var pull 		= $('#pull');
				menu 		= $('nav ul');
				menuHeight	= menu.height();
				
				$(pull).on('click', function(e) {
					e.preventDefault();
					menu.slideToggle();
				});
				
				$(window).resize(function(){
					var w = $(window).width();
					if(w > 320 && menu.is(':hidden')) {
						menu.removeAttr('style');
					}
				});
				
			});
			
		</script>
	</head>
		
	<body>
		
      	<form id="form-submit" action="<?php echo site_url($action); ?>" method="post" style="width: 100%;height: 100%;">
			<?php require('shared/navbar.php');?>
  
	        <?php $this->load->view($content); ?>

			<?php require('shared/footer.php');?>
			
			<input type="hidden" id="view_mode" value="<?php echo $_SESSION['viewmode'] ?>" />
      	</form>
	</body>
</html>

<script type="text/javascript" >
	$('#s').keyup(function(e){
	    if(e.keyCode != 13)
	    {
	       
	    }
	});
	
	function submitform()
	{
		if($('#s').val() != '')
		{
		 	document.getElementById("form-submit").submit();			
		}
	}
</script>