
        <section class="content"> <br class="clear">
          <div id="searchbg">
            <div id="map-container"> 
              <a class="search-submit-button" href="javascript:void(0)"> 
              	<i class="fa fa-search"></i> 
              </a>
              <div id="searchtext">
                <input type="text" id="searchMap" name="searchMap" placeholder="Search for location" autocomplete="off">
              </div>
            </div>
          </div>
          <div id="pic">
            <div id="map-canvas" ></div>
          </div>
        </section>
        
		<?php require('shared/menubar.php');?>    
        <section class="special">
          <div class="wrapper">
            <ul class="promotion_list">
              <li> <a href="#"> <img src="img/food_1.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$5</span>
                <div class="meal_details">
                  <h1> <a href="#">Eggs Benedict Burger</a> </h1>
                  <h2>More information about <span class="meal_size">East London</span></h2>
                </div>
              </li>
              <li> <a href="#"> <img src="img/food_2.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$15</span>
                <div class="meal_details">
                  <h1> <a href="#">Green Chile Chicken Tacos</a> </h1>
                  <h2>More information about the food <span class="meal_size"> Mexico</span></h2>
                </div>
              </li>
              <li> <a href="#"> <img src="img/food_3.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$1</span>
                <div class="meal_details">
                  <h1> <a href="#">Choco Coockies</a> </h1>
                  <h2>More information about the food <span class="meal_size">Berlin</span></h2>
                </div>
              </li>
            </ul>
            <div class="more_listing"> <a href="#" class="more_listing_btn">View More special</a> </div>
          </div>
        </section>
        <!-- //Specials  -->
        <input type="hidden" id="api_site" value="<?php echo $api_site; ?>" > 
        
        <script type="text/javascript" >
        	$(function() {
				google.maps.event.addDomListener(window, 'load', initialize);        		
        	});
        	
        </script>