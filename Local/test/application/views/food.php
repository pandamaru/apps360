
        <section class="secondlevel">
          <div>
            <ul class="nav2">
              <li class="name">Restaurant name</li>
              <li class="start"><a href="index.html">Homepage</a></li>
              <li class="goback"><a href = "javascript:history.back()">Back to previous page</a></li>
            </ul>
          </div>
        </section>
        <section class="content"> <br class="clear">
          <div id="pic_view"> <img src="img/meal_example.jpg" alt=""> </div>
          <div id="mealname">
            <p> Lemon Millet with Grilled Asparagus and Blistered Tomatoes <span class="price">30$</span> </p>
            <div id="translate">
              <ul>
                <li> <a href="#">translate</a></li>
              </ul>
            </div>
          </div>
        </section>
       
        <aside id="menubg">
          <div id="menuwrap">
            <ul id="menu">
              <li id="food"><a class="current" href="food.html">Food</a></li>
              <li id="drink"><a href="drink.html">Drink</a></li>
              <li id="more"><a href="more.html">More</a></li>
            </ul>
          </div>
        </aside>
        <section class="special">
          <div class="wrapper">
            <ul class="promotion_list">
              <li> <a href="#"> <img src="img/food_1.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$5</span>
                <div class="meal_details">
                  <h1> <a href="#">Ginger Chicken Stir Fry</a> </h1>
                  <h2>More information about <span class="meal_size">East London</span></h2>
                </div>
              </li>
              <li> <a href="#"> <img src="img/food_2.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$15</span>
                <div class="meal_details">
                  <h1> <a href="#">Green Chile Chicken Tacos</a> </h1>
                  <h2>More information about the food <span class="meal_size"> Mexico</span></h2>
                </div>
              </li>
              <li> <a href="#"> <img src="img/food_3.jpg" alt="" title="" class="meal_img"/> </a> <span class="price">$1</span>
                <div class="meal_details">
                  <h1> <a href="#">Choco Coockies</a> </h1>
                  <h2>More information about the food <span class="meal_size">Berlin</span></h2>
                </div>
              </li>
            </ul>
            <div class="more_listing"> <a href="#" class="more_listing_btn">View More special</a> </div>
          </div>
        </section>
        <!-- //Specials  -->
