
<section class="secondlevel">
	<div>
        <ul class="nav2">
          <li class="name">Search result</li>
          <li class="start"><a href="<?php echo site_url('') ?>">Homepage</a></li>
          <li class="goback"><a href = "javascript:history.back()">Back to previous page</a></li>
        </ul>
     </div>
</section>
<section class="content_scroll">          
          <br class="clear">   
          <div id="searchrestaurant">
            <div id="form-container_restaurant"> <a class="search-submit-button" href="javascript:void(0)"> <i class="fa fa-search"></i> </a>
              <div id="searchtext">
                <input type="text" id="sclient" name="sclient" placeholder="Search for restaurant" autocomplete="off">
              </div>
            </div>
          </div>
          
          <div class="list_restaurant">
            <ul> 
				<?php foreach( $results as $result) : ?>
					<li class="resaurant_section" >
						<a href="<?php echo site_url('/index.php/Menu') ?>">
	                	<div class="restaurant_list"> <img src="<?php echo site_url('asset/img/restaurant_example.jpg') ?>" alt="" title=""/>
							<h1 class="search_restaurant" > <?php echo $result->name; ?></h1>
							<p  class="search_restaurant" >
								<?php echo $result->address; ?> </br>
								<?php echo $result->sub_district; ?>
								<?php echo $result->district; ?></br>
								<?php echo $result->province; ?></br>
								<?php echo $result->country; ?></br>
							</p>
	              		</div>
		                </a>
					</li>
				<?php endforeach; ?> 
            </ul>
          </div>
</section>
        
        
<?php require('shared/menubar.php');?>    
<section class="special">
	<div class="wrapper">
		<ul class="promotion_list">
			<li>
				<a href="#">
					<img src="<?php echo site_url('/asset/img/food_1.jpg') ?>" alt="" title="" class="meal_img"/>
				</a>
				<span class="price">$5</span>
				<div class="meal_details">
					<h1>
						<a href="#">Eggs Benedict Burger</a>
					</h1>
					<h2>More information about  <span class="meal_size">East London</span></h2>
				</div>
			</li>
			<li>
				<a href="#">
					<img src="<?php echo site_url('/asset/img/food_2.jpg') ?>" alt="" title="" class="meal_img"/>
				</a>
				<span class="price">$15</span>
				<div class="meal_details">
					<h1>
						<a href="#">Green Chile Chicken Tacos</a>
					</h1>
					<h2>More information about the food <span class="meal_size"> Mexico</span></h2>
				</div>
			</li>
			<li>
				<a href="#">
					<img src="<?php echo site_url('/asset/img/food_3.jpg') ?>" alt="" title="" class="meal_img"/>
				</a>
				<span class="price">$1</span>
				<div class="meal_details">
					<h1>
						<a href="#">Choco Coockies</a>
					</h1>
					<h2>More information about the food <span class="meal_size">Berlin</span></h2>
				</div>
			</li>
			
		</ul>
		<div class="more_listing">
			<a href="#" class="more_listing_btn">View More special</a>
		</div>
	</div>
</section>	<!-- //Specials  -->

<script type="text/javascript" >
	$(function() {
		$('#sclient').instaFilta({
	        targets: '.search_restaurant',
	        matchCssClass: '.resaurant_section'
	    });
	});	
</script>
















