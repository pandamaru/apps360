 
 <header>
	<a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('asset/img/logo.png'); ?>" alt="Eat360"></a>
	<nav class="language">  
		<select name="language" id="language" class="languageselect">
			<?php foreach( $language as $lang) : ?>
				<?php if($_SESSION["language"] == $lang['id']) 
					{ 
						$selected = "selected";
					}
					else
					{
						$selected = "";
					}
				?>
				  <option <?php echo $selected ; ?> value="<?php echo $lang['id']; ?>"><?php echo $lang['text']; ?></option>
			<?php endforeach; ?>
		</select> 
   	</nav>
</header>

<section class="top">
	<br class="clear">
</section>
<!-- // Top  -->  

<script type="text/javascript" >
    $(function() {
		$('.languageselect').change(function() {			
			  var postparam  =  $(this).val();
			  $.ajax({
				url:  'Language',
				type: 'POST',
				data: 'language=' +postparam,
				success: function(){
					if($('#view_mode').val() == 'mapview'){
						loadData();
					}
					else{
						submitform();
					}
				}
			  });		
			  
		});
	});
</script>