 <footer>
      <div class="wrapper footer">
        <ul>
          <li class="links">
            <ul>
              <li><a href="#">About</a></li>
              <li><a href="#">Terms</a></li>
              <li><a href="#">Policy</a></li>
              <li><a href="#">Contact</a></li>
            </ul>
          </li>
          <li class="about">
            <p>Find the menu of your favourite restaurant in your own language. This app is provided form Apps360.
              Check out now for more information!</p>
            <ul>
              <li><a href="http://facebook.com" class="facebook" target="_blank"></a></li>
              <li><a href="http://twitter.com" class="twitter" target="_blank"></a></li>
            </ul>
          </li>
        </ul>
      </div>
     <div class="copyrights wrapper"> Copyright � 2015 <a href="http://www.apps360.co/ target="_blank" class="ph_link" title="Apps360">apps360.co</a>. All Rights Reserved. 	 </div>
</footer>