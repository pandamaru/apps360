<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  require_once(APPPATH . "/libraries/detectlanguage.php");
  use \DetectLanguage\DetectLanguage;
  
  
  function detect_language($text){
  	$ci = &get_instance();
  	
    $detected = false;
    $count = 0;
    while((!$detected) && ($count <5))
    {
	  	DetectLanguage::setApiKey("c84b24e10e2928342fc2ee7c5ac5aa1f");
	  	$detected = DetectLanguage::detect($text);
	  	$count++;
	}    
	$ci->load->model('Language');
	$langs =  $ci->Language->getLanguage();
	$results = -1;
	foreach($langs as $lang)
	{
		if(strtolower($lang["text"]) == $detected[0]->language)
		{
			$results = $lang["id"];
			break;
		}
	}	
	
  	return $results;
  }
  
  
?>