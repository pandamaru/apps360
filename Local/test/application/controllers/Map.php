<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Map extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{		
		$this->load->model('Language');
		$this->native_session->set_userdata('viewmode','mapview');
		$lang =  $this->Language->getLanguage();
		$data = array('content'=>'map',
					  'action'=>'/index.php/Map',
					  'api_site' => site_url('/index.php/Map/') ,
					  'language' => $lang);
		$this->load->view('template',$data);	
	}
	
	public function SearchMap()
	{
		$this->load->model('Language');
		$lang =  $this->Language->getLanguage();
		
    	$url = $this->config->item('api_site') . "/search_by_location"; // Where you want to post data
    	$param = array("MinLatitude" => $_POST['MinLat'],
    				   "MinLongitude" => $_POST['MinLng'] ,
    				   "MaxLatitude" => $_POST['MaxLat'] ,
    				   "MaxLongitude" => $_POST['MaxLng'] ,
    				   "LanguageID" => $this->native_session->userdata('language'));
    				   
		$raw_data = $this->curl->simple_post($url, $param);

		//var_dump($jsondata);
		echo $raw_data;	
	}
	
	public function InfoWindow()
	{
    	$model = array("RestaurantId" => $_POST['RestaurantId'],
    				   "RestaurantName" => $_POST['RestaurantName'] ,
    				   "Image" => $_POST['Image'] ,
    				   "address" => $_POST['address'] ,
    				   "sub_district" => $_POST['sub_district'] ,
    				   "district" => $_POST['district'] ,
    				   "province" => $_POST['province'] ,
    				   "country" => $_POST['country']);
    				   
		//var_dump($jsondata);
		$data = array('result'=>$model);
		
		$this->load->view('shared/mapinfo',$data);	
	}
	
}
