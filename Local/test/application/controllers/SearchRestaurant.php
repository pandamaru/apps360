<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchRestaurant extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{		
		$search = $this->native_session->userdata('search_text');
		$this->native_session->set_userdata('viewmode','listview');
		if (isset($_POST["s"]) || isset($search))
		{
			$bIsValid = true;
			if(isset($_POST["s"]))
			{	
				if($_POST["s"] != "")
				{
					$this->native_session->set_userdata('search_text',$_POST["s"]);
				}	
				else
				{
					$bIsValid = false;
				}		
			}
			$this->native_session->set_userdata('language',$_POST["language"]);
			if($bIsValid)	
			{	
				
		    	$this->load->helper('language');
		    	$langText = detect_language($this->native_session->userdata('search_text'));
		    				                 // Initiate cURL
		    	$url = $this->config->item('api_site') . "/search_by_keyword"; // Where you want to post data
		    			    	
		    	$param = array("SearchText" => $this->native_session->userdata("search_text"), 
		    				   "SearchLang" => $langText ,
		    				   "LanguageID" => $this->native_session->userdata("language"));
			    
				$raw_data = $this->curl->simple_post($url, $param);

				$jsondata = json_decode($raw_data);
				
				$this->load->model('Language');
				$lang =  $this->Language->getLanguage();
				$data = array('content'=>'searchrestaurant',
							  'action'=>'/index.php/SearchRestaurant',
							  'language' => $lang,
							  'results' => $jsondata);
				$this->load->view('template',$data);	
				
			}	
			else
			{				
				$this->load->helper('url');
				redirect('');
			}
		}
		else
		{
			$this->load->helper('url');
			redirect('');			
		}
	}
	
	
}


