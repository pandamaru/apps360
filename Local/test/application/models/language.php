<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Language extends CI_Model{

  	var $menu = array();  //The array holding all navigation elements
	/*
	 * Render the top nav
	 */
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

	function getLanguage(){

		
		$this->menu = array
		(
			1 => 	array(
				'text'		=> 	'EN',	
				'id'	=>	2
			),
			2 => 	array(
				'text'		=> 	'TH',		
				'id'	=>	1
			)				
		); 
		$language = $this->native_session->userdata('language');
		if(!isset($language))		
		{
			$this->native_session->set_userdata('language', $this->menu[1]["id"]);			
		}
		return $this->menu;
	}	
}	
