// Note: This example requires that you consent to location sharing when
// prompted by your browser. If you see a blank space instead of the map, this
// is probably because you have denied permission for location sharing.

var map;
var searchBox;
var markers = [];
var mapOptions = {
    	zoom: 15
    };
var infowindow;
function initialize() {
  
  map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
      
  var inputText = document.getElementById('searchMap');
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(inputText);
  google.maps.event.addDomListener(inputText, 'keydown', function(e) { 
	    if (e.keyCode == 13 ) { 
	        e.preventDefault(); 
	    }
	}); 
  searchBox = new google.maps.places.SearchBox(inputText);
  
  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
      
		var iconBase = 'http://www.penceland.com/images/google_map_man.gif';
		var marker = new google.maps.Marker({
	  		position: pos,
	  		map: map,
	  		icon: iconBase
		});
      map.setCenter(pos);
      loadData();
      
      google.maps.event.addListener(map, 'bounds_changed', function() {
	    window.setTimeout(function() {
      		loadData();
	    }, 1000);
	  });

	  google.maps.event.addListener(searchBox, 'places_changed',function() {
	  	search_text();
	  });
      
    }, 
    function() 
    {
      handleNoGeolocation(true);
    });
  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function loadData()
{	
  var ne = map.getBounds().getNorthEast();
  var sw = map.getBounds().getSouthWest();
  
  var postparam
  $.ajax({
	url: $('#api_site').val() + '/SearchMap',
	type: 'POST',
	data: 'MinLat=' + sw.lat() + 
			'&MinLng=' + sw.lng() +
			'&MaxLat=' + ne.lat() +
			'&MaxLng=' + ne.lng(),
	success: function(pins){
			pushMappin(JSON.parse(pins));
		}
  });
}

function pushMappin(pins)
{
	if(markers.length > 0){
		for (var i = 0, marker; marker = markers[i]; i++) {
			      marker.setMap(null);
		}		
	}
	if(pins.length){
		for (var i = 0, pin; pin = pins[i]; i++) {
			
	      var pos = new google.maps.LatLng(pin.latitude,pin.longitude);
	      var marker = new google.maps.Marker({
	        map: map,
	        icon: 'http://terratota.com/images/categories/rest.png',
	        title: pin.name+ '|' + pin.restaurant_i,
	        position:pos
	      });
    	  makeInfoWindowEvent(map,pin, marker);
	      markers.push(marker);
		}		
	}
}	

function makeInfoWindowEvent(map, pin,marker) {
  google.maps.event.addListener(marker, 'click', function() {
  	if(infowindow != null){
		 infowindow.close();
	}
  	$.ajax({
		url: $('#api_site').val()  + '/InfoWindow' ,
		type: 'POST',
		data: 'RestaurantId=' + pin.restaurant_id + 
				'&RestaurantName=' + pin.name +
				'&Image=' + 'http://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/India_Gate_clean.jpg/100px-India_Gate_clean.jpg'+
				'&address=' + pin.address+ 
				'&sub_district=' + pin.sub_district+ 
				'&district=' + pin.district+ 
				'&province=' + pin.province+ 
				'&country=' + pin.country,
		success: function(result){
		  		infowindow = new google.maps.InfoWindow();
			    infowindow.setContent(result);
			    infowindow.open(map, marker);
			}
	  });
  });
}

function search_text()
{
	var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    
    if(markers.length > 0 )
    {			
	    for (var i = 0, marker; marker = markers[i]; i++) {
	      marker.setMap(null);
	    }
	}

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    var lat = -95;
    var lng = -180;
    for (var i = 0, place; place = places[i]; i++) {
    	if(place.geometry.location.lat() > lat ){
			lat = place.geometry.location.lat();
		}
    	if(place.geometry.location.lng() > lng ){
			lng = place.geometry.location.lng();
		}
    }
    
    var pos = new google.maps.LatLng(lat,lng);
    
    map.setCenter(pos);
    loadData();
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
}
