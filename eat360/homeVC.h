//
//  homeVC.h
//  eat360
//
//  Created by Apps360[admin] on 29/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "sideMenuProtocol.h"
#import "sideMenuSingleton.h"

@interface homeVC : UIViewController <multiLanguageProtocol, sideMenuProtocol>

@property (strong, nonatomic) IBOutlet UIImageView *searchBarBG;

@end
