//
//  sideMenuVC.h
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenuContainerViewController.h>
#import "multiLanguageProtocol.h"
//#import "languageSingleton.h"
//#import "sideMenuSingleton.h"
//#import "dataSingleton.h"
//#import "aboutVC.h"

@interface sideMenuVC : UITableViewController <multiLanguageProtocol>

@property(nonatomic) MFSideMenuContainerViewController* controller;

@end
