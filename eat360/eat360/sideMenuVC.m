//
//  sideMenuVC.m
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import "sideMenuVC.h"

@implementation sideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuStateEventOccurred:) name:MFSideMenuStateNotificationEvent object:nil];
}

- (void)setUILanguage {
    
}

- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    
    if(event == MFSideMenuStateEventMenuWillOpen) {
        _controller.panMode = MFSideMenuPanModeDefault;
    }
    
    if(event == MFSideMenuStateEventMenuWillClose) {
        //[self dismissPicker];
        _controller.panMode = MFSideMenuPanModeNone;
    }
}

@end
