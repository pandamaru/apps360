//
//  sideMenuSingleton.h
//  eat360
//
//  Created by Apps360[admin] on 29/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MFSideMenu.h>

@interface sideMenuSingleton : NSObject {
    MFSideMenuContainerViewController* sideMenuCon;
}

+ (id)Instance;
- (void)setSideMenu:(MFSideMenuContainerViewController*)_sideMenuCon;
- (void)callSideMenu;

@end
