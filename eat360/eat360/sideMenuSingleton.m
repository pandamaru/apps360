//
//  sideMenuSingleton.m
//  eat360
//
//  Created by Apps360[admin] on 29/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import "sideMenuSingleton.h"

@implementation sideMenuSingleton

+ (id)Instance {
    static sideMenuSingleton *sideMenuSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sideMenuSingletonInstance = [[self alloc] init];
    });
    
    return sideMenuSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        
    }
    
    return  self;
}

- (void)setSideMenu:(MFSideMenuContainerViewController*)_sideMenuCon {
    sideMenuCon = _sideMenuCon;
}

- (void)callSideMenu {
    if(sideMenuCon != nil) {
        [sideMenuCon toggleRightSideMenuCompletion:nil];
    }
}

@end
