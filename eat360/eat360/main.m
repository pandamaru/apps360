//
//  main.m
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
