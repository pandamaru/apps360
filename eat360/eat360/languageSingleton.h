//
//  languageSingleton.h
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface languageSingleton : NSObject {
    NSString *currentLanguage;
    NSDictionary *data;
}

+ (id)Instance;
- (void)setCurrentLanguage:(NSString*)language;
- (NSString*)getCurrentLanguage;
- (NSString*)getTextForKey:(NSString*)key;

@end
