//
//  sideMenuProtocol.h
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol sideMenuProtocol <NSObject>

- (void)setSideMenu;
- (void)callSideMenu;

@end
