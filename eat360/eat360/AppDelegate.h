//
//  AppDelegate.h
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenuContainerViewController.h>
#import <GoogleMaps/GoogleMaps.h>
#import <FBSDKCoreKit.h>
#import "languageSingleton.h"
#import "sideMenuSingleton.h"
//#import "dataSingleton.h"
//#import "appearanceSingleton.h"
#import "sideMenuVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

