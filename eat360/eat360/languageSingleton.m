//
//  languageSingleton.m
//  eat360
//
//  Created by Apps360[admin] on 25/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import "languageSingleton.h"

@implementation languageSingleton

+ (id)Instance {
    static languageSingleton *languageSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        languageSingletonInstance = [[self alloc] init];
    });
    
    return languageSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        currentLanguage = [defaults objectForKey:@"currentLanguage"];
        
        if(currentLanguage == nil) {
            NSLog(@"Default Language is NULL, set to en");
            
            [self setCurrentLanguage:@"en"];
        } else {
            [self setCurrentLanguage:currentLanguage];
        }
    }
    
    return  self;
}

- (void)setCurrentLanguage:(NSString*)language {
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_%@", @"language", language] ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    
    NSError *jsonError;
    data = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if(jsonError != nil) {
        NSLog(@"Error loading %@, set to en", language);
        
        currentLanguage = @"en";
    } else {
        currentLanguage = language;
    }
    
    [defaults setValue:currentLanguage forKey:@"currentLanguage"];
    [defaults synchronize];
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", @"Current Language is", currentLanguage]);
}

- (NSString*)getCurrentLanguage {
    return currentLanguage;
}

- (NSString*)getTextForKey:(NSString*)key {
    return [data objectForKey:key];
}

@end
