//
//  homeVC.m
//  eat360
//
//  Created by Apps360[admin] on 29/09/2015.
//  Copyright (c) 2015 Apps360. All rights reserved.
//

#import "homeVC.h"

@interface homeVC ()

@end

@implementation homeVC {
    UINavigationController *navCon;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    [_searchBarBG.image resizableImageWithCapInsets:UIEdgeInsetsMake(7, 12, 7, 12)];
    
    [self setUILanguage];
    [self setSideMenu];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setUILanguage {
    //Insert logo image to NavBar
    UIImage *Logo_Header = [UIImage imageNamed:@"Logo_Header"];
    navCon.navigationItem.titleView = [[UIImageView alloc] initWithImage:Logo_Header];
    
}

- (void)setSideMenu {
    //Insert burger image to NavBar right item
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BTN_Menu"] style:UIBarButtonItemStylePlain target:self action:@selector(callSideMenu)];
    navCon.navigationItem.rightBarButtonItem = rightItem;
}

- (void)callSideMenu {
    [[sideMenuSingleton Instance] callSideMenu];
}

@end
