//
//  settingMenuController.h
//  menu360
//
//  Created by PandaMaru on 6/12/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MFSideMenu.h>

@interface settingMenuController : NSObject {
    UIViewController* useView;
}

@property(nonatomic) UIViewController* useView;

-(id)initWithViewController:(UIViewController*)_useView;
-(void)callSettingMenu;

@end
