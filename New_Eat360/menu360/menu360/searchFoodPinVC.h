//
//  searchFoodPinVC.h
//  menu360
//
//  Created by Apps360[admin] on 1/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import <GoogleMaps/GoogleMaps.h>
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "customInfoWindow.h"
#import "restaurantTabBarVC.h"

@interface searchFoodPinVC : UIViewController <multiLanguageProtocol, GMSMapViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSMutableArray *resultArray;

@end
