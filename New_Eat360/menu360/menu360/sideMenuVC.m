//
//  sideMenuViewController.m
//  menu360
//
//  Created by PandaMaru on 6/12/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "sideMenuVC.h"

@interface sideMenuVC ()

@end

@implementation sideMenuVC {
    UITextField *dummyTextFieldForLanguage;
    UITextField *dummyTextFieldForCurrency;
    UITextField *dummyTextFieldForSetting;
    MyCountryPicker *countryPicker;
    UIPickerView *currencyPicker;
    UIPickerView *settingPicker;
    NSString *languageValue;
    NSString *currencyValue;
    NSString *settingValue;
    UIBarButtonItem *doneButtonForLanguage;
    UIBarButtonItem *doneButtonForCurrency;
    UIBarButtonItem *doneButtonForSetting;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languageValue = [[languageSingleton Instance] getCurrentLanguage];
    currencyValue = [[dataSingleton Instance] getCurrentCurrency];
    settingValue = [[APISingleton Instance] getCurrentServer];
    
    dummyTextFieldForLanguage = [[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:dummyTextFieldForLanguage];
    dummyTextFieldForCurrency = [[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:dummyTextFieldForCurrency];
    dummyTextFieldForSetting = [[UITextField alloc] initWithFrame:CGRectZero];
    [self.view addSubview:dummyTextFieldForSetting];
    
    countryPicker = [[MyCountryPicker alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    countryPicker.showsSelectionIndicator = YES;
    countryPicker.delegate = self;
    
    currencyPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    currencyPicker.showsSelectionIndicator = YES;
    currencyPicker.dataSource = self;
    currencyPicker.delegate = self;
    
    settingPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    settingPicker.showsSelectionIndicator = YES;
    settingPicker.dataSource = self;
    settingPicker.delegate = self;
    
    dummyTextFieldForLanguage.inputView = countryPicker;
    dummyTextFieldForCurrency.inputView = currencyPicker;
    dummyTextFieldForSetting.inputView = settingPicker;
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    doneButtonForLanguage = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneForLanguage)];

    UIToolbar *toolbarForLanguage = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - countryPicker.frame.size.height - 50, 320, 50)];
    NSArray *toolbarItemsForLanguage = [NSArray arrayWithObjects:flexibleSpace, doneButtonForLanguage, nil];
    [toolbarForLanguage setItems:toolbarItemsForLanguage];
    dummyTextFieldForLanguage.inputAccessoryView = toolbarForLanguage;
    
    doneButtonForCurrency = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneForCurrency)];
    
    UIToolbar *toolbarForCurrency = [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - currencyPicker.frame.size.height - 50, 320, 50)];
    NSArray *toolbarItemsForCurrency = [NSArray arrayWithObjects:flexibleSpace, doneButtonForCurrency, nil];
    [toolbarForCurrency setItems:toolbarItemsForCurrency];
    dummyTextFieldForCurrency.inputAccessoryView = toolbarForCurrency;
    
    doneButtonForSetting = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(doneForSetting)];
    
    UIToolbar *toolbarForSetting= [[UIToolbar alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - settingPicker.frame.size.height - 50, 320, 50)];
    NSArray *toolbarItemsForSetting = [NSArray arrayWithObjects:flexibleSpace, doneButtonForSetting, nil];
    [toolbarForSetting setItems:toolbarItemsForSetting];
    dummyTextFieldForSetting.inputAccessoryView = toolbarForSetting;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPicker)];
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(menuStateEventOccurred:) name:MFSideMenuStateNotificationEvent object:nil];
    
    [self setUILanguage];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    countryPicker.selectedCountryCode = languageValue;
    [currencyPicker selectRow:[[[dataSingleton Instance] currencyArray] indexOfObject:currencyValue] inComponent:0 animated:NO];
    [settingPicker selectRow:[[[APISingleton Instance] serverArray] indexOfObject:settingValue] inComponent:0 animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    [doneButtonForLanguage setTitle:[[languageSingleton Instance] getTextForKey:@"done"]];
    [doneButtonForCurrency setTitle:[[languageSingleton Instance] getTextForKey:@"done"]];
    [_UI_Language setTitle:[[languageSingleton Instance] getTextForKey:@"language"] forState:UIControlStateNormal];
    [_UI_Currency setTitle:[[languageSingleton Instance] getTextForKey:@"currency"] forState:UIControlStateNormal];
    //Set UI Lang for Setting text
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if(pickerView == currencyPicker) {
        return [[dataSingleton Instance] currencyArray].count;
    } else if(pickerView == settingPicker) {
        return [[APISingleton Instance] serverArray].count;
    }
    
    return 0;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if(pickerView == currencyPicker) {
        return [[[dataSingleton Instance] currencyArray] objectAtIndex:row];
    } else if(pickerView == settingPicker) {
        return [[[APISingleton Instance] serverArray] objectAtIndex:row];
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(pickerView == currencyPicker) {
        currencyValue = [[[dataSingleton Instance] currencyArray] objectAtIndex:row];
        NSLog(@"Select Currency : %@", currencyValue);
    } else if(pickerView == settingPicker) {
        settingValue = [[[APISingleton Instance] serverArray] objectAtIndex:row];
        NSLog(@"Select Server : %@", settingValue);
    }
}

-(void)doneForLanguage {
    [self dismissPicker];
    [[languageSingleton Instance] setCurrentLanguage:languageValue];
    [[sideMenuSingleton Instance] applyLanguageToView];
    [self setUILanguage];
}

-(void)doneForCurrency {
    [self dismissPicker];
    [[dataSingleton Instance] setCurrentCurrency:currencyValue];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadCurrency" object:self];
}

-(void)doneForSetting {
    [self dismissPicker];
    [[APISingleton Instance] setCurrentServer:settingValue];
}

- (void)countryPicker:(CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code {
    languageValue = code;
    NSLog(@"Select Language : %@", languageValue);
}

- (void)dismissPicker {
    if(dummyTextFieldForLanguage.isFirstResponder) {
        [dummyTextFieldForLanguage resignFirstResponder];
    } else if(dummyTextFieldForCurrency.isFirstResponder) {
        [dummyTextFieldForCurrency resignFirstResponder];
    } else if(dummyTextFieldForSetting.isFirstResponder) {
        [dummyTextFieldForSetting resignFirstResponder];
    }
}

- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    //MFSideMenuContainerViewController *containerViewController = notification.object;
    
    if(event == MFSideMenuStateEventMenuWillOpen) {
        _controller.panMode = MFSideMenuPanModeDefault;
    }
    
    if(event == MFSideMenuStateEventMenuWillClose) {
        [self dismissPicker];
        _controller.panMode = MFSideMenuPanModeNone;
    }
}

- (IBAction)callLanguage:(id)sender {
    [dummyTextFieldForLanguage becomeFirstResponder];
}

- (IBAction)callCurrency:(id)sender {
    [dummyTextFieldForCurrency becomeFirstResponder];
}

- (IBAction)callSetting:(id)sender {
    [dummyTextFieldForSetting becomeFirstResponder];
}

- (IBAction)callAbout:(id)sender {
    //[self performSegueWithIdentifier:@"about" sender:self];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = (UINavigationController *)[storyboard instantiateViewControllerWithIdentifier:@"about_nav"];
    //aboutVC *about = (aboutVC *)[storyboard instantiateViewControllerWithIdentifier:@"about"];
    
    // present
    [self presentViewController:nav animated:YES completion:nil];
}

@end
