//
//  restaurantHeaderCell.m
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "restaurantHeaderCell.h"

@implementation restaurantHeaderCell

- (void)setForHidden:(BOOL)isHidden {
    if(isHidden) {
        _addressAL.constant = -_searchTypeView.frame.size.height + 10;
        _bgAL.constant = -_searchTypeView.frame.size.height + 0;
    } else {
        _addressAL.constant = 30;
        _bgAL.constant = 20;
    }
}

- (IBAction)touchMap:(id)sender {
    [_parentVC performSegueWithIdentifier:@"restaurantMap" sender:self];
}

- (IBAction)touchSearchTypeClose:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeCondition" object:self];
}

@end
