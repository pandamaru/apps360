//
//  aboutVC.h
//  menu360
//
//  Created by Apps360[admin] on 18/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface aboutVC : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction)callBack:(id)sender;

@end
