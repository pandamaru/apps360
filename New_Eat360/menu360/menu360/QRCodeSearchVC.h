//
//  QRCodeSearchViewController.h
//  menu360
//
//  Created by PandaMaru on 6/10/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import "settingMenuController.h"
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "APISingleton.h"
#import "restaurantTabBarVC.h"
#import "restaurantModel.h"
#import <AVFoundation/AVFoundation.h>

@interface QRCodeSearchVC : UIViewController <multiLanguageProtocol, UINavigationControllerDelegate, AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (strong, nonatomic) IBOutlet UIView *viewPreview;

- (BOOL)startReading;
- (void)stopReading;
- (IBAction)callSettingMenu:(id)sender;

@end
