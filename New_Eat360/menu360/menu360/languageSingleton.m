//
//  languageSingleton.m
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "languageSingleton.h"

@implementation languageSingleton

//@synthesize cu;

+ (id)Instance {
    static languageSingleton *languageSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        languageSingletonInstance = [[self alloc] init];
    });
    
    return languageSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        currentLang = [defaults objectForKey:@"lang"];
        
        if(currentLang == nil) {
            NSLog(@"Default Language is NULL, set to US");
            
            currentLang = @"US";
            [defaults setValue:@"US" forKey:@"lang"];
            [defaults synchronize];
        }
        
        [self setCurrentLanguage:currentLang];
    }
    
    return  self;
}

- (void)setCurrentLanguage:(NSString*)language {
    currentLang = language;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@_%@", @"lang", currentLang] ofType:@"json"];
    
    NSData *jsonData = [NSData dataWithContentsOfFile:path];
    
    NSError *jsonError;
    data = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&jsonError];
    
    if(jsonError != nil) {
        NSLog(@"%@", @"Have some error");
        //do something
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:language forKey:@"lang"];
    [defaults synchronize];
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", @"Current Language is", currentLang]);
}

- (NSString*)getCurrentLanguage {
    return currentLang;
}

- (NSString*)getCurrentLanguageForAPI {
    if([currentLang isEqualToString:@"US"]) {
        return @"en";
    } else if([currentLang isEqualToString:@"SA"]) {
        return @"ar";
    } else if([currentLang isEqualToString:@"DE"]) {
        return @"de";
    } else if([currentLang isEqualToString:@"GR"]) {
        return @"el";
    } else if([currentLang isEqualToString:@"ES"]) {
        return @"es";
    } else if([currentLang isEqualToString:@"FR"]) {
        return @"fr";
    } else if([currentLang isEqualToString:@"IN"]) {
        return @"hi";
    } else if([currentLang isEqualToString:@"IT"]) {
        return @"it";
    } else if([currentLang isEqualToString:@"IL"]) {
        return @"iw";
    } else if([currentLang isEqualToString:@"JP"]) {
        return @"ja";
    } else if([currentLang isEqualToString:@"KR"]) {
        return @"ko";
    } else if([currentLang isEqualToString:@"PL"]) {
        return @"pl";
    } else if([currentLang isEqualToString:@"RU"]) {
        return @"ru";
    } else if([currentLang isEqualToString:@"TH"]) {
        return @"th";
    } else if([currentLang isEqualToString:@"TR"]) {
        return @"tr";
    } else if([currentLang isEqualToString:@"CN"]) {
        return @"zh";
    }
    
    return @"";
}

- (NSString*)getTextForKey:(NSString*)key {
    return [data objectForKey:key];
}

@end
