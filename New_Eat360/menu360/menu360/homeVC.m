//
//  homeViewController.m
//  menu360
//
//  Created by PandaMaru on 6/3/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "homeVC.h"

@interface homeVC ()

@end

@implementation homeVC {
    CGFloat animatedDistance;
    CGFloat KEYBOARD_ANIMATION_DURATION;
    CGFloat MINIMUM_SCROLL_FRACTION;
    CGFloat MAXIMUM_SCROLL_FRACTION;
    
    BOOL isAutocompleteReady;
    NSMutableArray *autocompleteArray;
    NSMutableArray *searchTypeArray;
    
    int selectedType; //0 - Walk, 1 - Drive, 2 - Global
    CGFloat advancePoint;
    CGFloat topCategoryPoint;
    
    BOOL isLocationAutocompleteReady;
    NSMutableArray *locationAutocompleteArray;
    
    CLLocation *currentLocation;
    NSString *walkRadius;
    NSString *driveRadius;
    
    NSString *radius;
    NSString *selectLat;
    NSString *selectLng;
    NSString *searchType;
    
    NSMutableArray *topFoodTypeArray;
    NSMutableArray *topFoodArray;
    NSMutableArray *topCuisineTypeArray;
    NSMutableArray *topCuisineArray;
    NSMutableArray *checkArray;
    
    UITapGestureRecognizer *tapTopFood;
    UILongPressGestureRecognizer *lpTopFood;
    UITapGestureRecognizer *tapTopCuisine;
    
    searchConditionModel *SearchConditionModel;
    
    NSTimer * refresher;
    
    UIActivityIndicatorView *spinner;
}

- (void)viewDidLoad {
    //NSLog(@"viewDidLoad");
    [super viewDidLoad];
    
    //Insert logo image to NavBar
    UIImage *Logo_Header = [UIImage imageNamed:@"Logo_Header"];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:Logo_Header];
    //Insert burger image to NavBar right item
    [self.navigationItem.rightBarButtonItem setImage:[[UIImage imageNamed:@"BTN_Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    //Insert original image to TabBar
    UITabBarItem *mapItem = [self.tabBarController.tabBar.items objectAtIndex:1];
    mapItem.image = [[UIImage imageNamed:@"BTN_Tab_Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *qrItem = [self.tabBarController.tabBar.items objectAtIndex:2];
    qrItem.image = [[UIImage imageNamed:@"BTN_Tab_QR"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    _locationBG.layer.shadowColor = [UIColor blackColor].CGColor;
    _locationBG.layer.shadowOffset = CGSizeMake(0, 0);
    _locationBG.layer.shadowOpacity = 1;
    _locationBG.layer.shadowRadius = 2.0;
    _locationBG.clipsToBounds = NO;
    
    [_searchBar customInit];
    _searchBar.isSearchCategory = YES;
    _searchBar.showAutoCompleteTableWhenEditingBegins = YES;
    _searchBar.applyBoldEffectToAutoCompleteSuggestions = NO;
    _searchBar.sortAutoCompleteSuggestionsByClosestMatch = NO;
    _searchBar.autoCompleteTableBorderWidth = 2.0f;
    _searchBar.autoCompleteTableBorderColor = [UIColor blackColor];
    _searchBar.autoCompleteTableBackgroundColor = [UIColor whiteColor];
    _searchBar.autoCompleteTableCellTextColor = [UIColor blackColor];
    _searchBar.autoCompleteRegularFontName = @"OpenSans";
    _searchBar.autoCompleteBoldFontName = @"OpenSans-Bold";
    _searchBar.autoCompleteFetchRequestDelay = 1;
    _searchBar.maximumNumberOfAutoCompleteRows = 5;
    _searchBar.searchCategoryTextColor = [UIColor colorWithRed:0.62 green:0.62 blue:0.62 alpha:1.0];
    
    [_locationBar customInit];
    _locationBar.showAutoCompleteTableWhenEditingBegins = YES;
    _locationBar.applyBoldEffectToAutoCompleteSuggestions = NO;
    _locationBar.autoCompleteTableBorderWidth = 2.0f;
    _locationBar.autoCompleteTableBorderColor = [UIColor blackColor];
    _locationBar.autoCompleteTableBackgroundColor = [UIColor whiteColor];
    _locationBar.autoCompleteTableCellTextColor = [UIColor blackColor];
    _locationBar.autoCompleteRegularFontName = @"OpenSans";
    _locationBar.autoCompleteBoldFontName = @"OpenSans-Bold";
    _locationBar.autoCompleteFetchRequestDelay = 1;
    _locationBar.maximumNumberOfAutoCompleteRows = 4;
    
    [_searchBtn setImage:[[UIImage imageNamed:@"UI_Search_Icon@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [_searchBtn setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    [_locationBtn setImage:[[UIImage imageNamed:@"UI_Search_Icon@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [_locationBtn setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    
    _searchTypeView.layer.cornerRadius = 3;
    _searchTypeView.layer.masksToBounds = YES;
    
    [_searchTypeCloseBtn setBackgroundImage:[[appearanceSingleton Instance] imageTintedWithColor:[_searchTypeCloseBtn backgroundImageForState:UIControlStateNormal] :[UIColor whiteColor]] forState:UIControlStateNormal];
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
        
        [_searchBar setDefaultTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:40]}];
        _searchBar.autoCompleteFontSize = 40;
        _searchBar.autoCompleteRowHeight = 100;
        
        [_selectDist setFont:[UIFont fontWithName:@"OpenSans" size:37.5]];
        [_minDist setFont:[UIFont fontWithName:@"OpenSans" size:32.5]];
        [_maxDist setFont:[UIFont fontWithName:@"OpenSans" size:32.5]];
        [_searchNear setFont:[UIFont fontWithName:@"OpenSans" size:37.5]];
        [_currentKm setFont:[UIFont fontWithName:@"OpenSans" size:37.5]];
        [_anotherLocation setFont:[UIFont fontWithName:@"OpenSans" size:37.5]];
        
        [_locationBar setDefaultTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:40]}];
        _locationBar.autoCompleteFontSize = 40;
        _locationBar.autoCompleteRowHeight = 100;
        
        [_searchTypeLabel setFont:[UIFont fontWithName:@"OpenSans" size:35]];
    }
    
    _searchBar.delegate = self;
    _locationBar.delegate = self;

    KEYBOARD_ANIMATION_DURATION = 0.3;
    MINIMUM_SCROLL_FRACTION = 0.2;
    MAXIMUM_SCROLL_FRACTION = 0.8;
    isAutocompleteReady = NO;
    autocompleteArray = [[NSMutableArray alloc] init];
    searchTypeArray = [[NSMutableArray alloc] init];
    
    isLocationAutocompleteReady = NO;
    locationAutocompleteArray = [[NSMutableArray alloc] init];
    
    _minDist.text = [NSString stringWithFormat:@"%d km", (int)_sliderDist.minimumValue];
    _maxDist.text = [NSString stringWithFormat:@"%d km", (int)_sliderDist.maximumValue];
    radius = [NSString stringWithFormat:@"%d", (int)_sliderDist.value];
    _currentKm.text = [NSString stringWithFormat:@" %@ km", radius];
    
    [_advanceBG setImage:[[UIImage imageNamed:@"UI_Box@x2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    [_topCategoryBG setImage:[[UIImage imageNamed:@"UI_Box@x2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    
    [_sliderBG setImage:[[UIImage imageNamed:@"UI_Box_Slider@x2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10) resizingMode:UIImageResizingModeStretch]];
    UIImage *thumbImage = [UIImage imageNamed:@"BTN_Search_Thumb"];
    CGRect rect = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.height * 0.06, [[UIScreen mainScreen] bounds].size.height * 0.06);
    UIGraphicsBeginImageContext( rect.size );
    [thumbImage drawInRect:rect];
    UIImage *picture1 = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImagePNGRepresentation(picture1);
    UIImage *img = [UIImage imageWithData:imageData];
    
    [[UISlider appearance] setThumbImage:img forState:UIControlStateNormal];
    [[UISlider appearance] setMaximumTrackTintColor:[UIColor clearColor]];
    
    UITapGestureRecognizer *tapSearch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboardForSearch:)];
    [self.view addGestureRecognizer:tapSearch];
    UITapGestureRecognizer *tapLocation = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboardForLocation:)];
    [_advanceView addGestureRecognizer:tapLocation];
    
    UITapGestureRecognizer *tapCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectItem:)];
    [_searchBar.autoCompleteTableView addGestureRecognizer:tapCell];
    
    UITapGestureRecognizer *tapLocationCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectLocation:)];
    [_locationBar.autoCompleteTableView addGestureRecognizer:tapLocationCell];
    
    UISwipeGestureRecognizer *swipeAdvance = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeAdvanceBG:)];
    swipeAdvance.direction = UISwipeGestureRecognizerDirectionDown;
    [_advanceView addGestureRecognizer:swipeAdvance];
    
    UISwipeGestureRecognizer *swipeTopCategory = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeTopCategoryBG:)];
    swipeTopCategory.direction = UISwipeGestureRecognizerDirectionDown;
    [_topCategoryView addGestureRecognizer:swipeTopCategory];
    
    walkRadius = @"1";
    driveRadius = @"10";
    
    searchType = @"";
    
    topFoodTypeArray = [[NSMutableArray alloc] init];
    topFoodArray = [[NSMutableArray alloc] init];
    topCuisineTypeArray = [[NSMutableArray alloc] init];
    topCuisineArray = [[NSMutableArray alloc] init];
    checkArray = [[NSMutableArray alloc] init];
    
    tapTopFood = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTopFood:)];
    [_topFoodCollectionView addGestureRecognizer:tapTopFood];
    lpTopFood = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pressTopFood:)];
    [_topFoodCollectionView addGestureRecognizer:lpTopFood];
    
    tapTopCuisine = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectTopCuisine:)];
    [_topCuisineCollectionView addGestureRecognizer:tapTopCuisine];
    
    SearchConditionModel = [[searchConditionModel alloc] init];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width/2.0, [[UIScreen mainScreen] bounds].size.height/2.0)];
    [_loadingView addSubview:spinner];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    _searchBar.hidden = NO;
    
    [_walkBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Walk"] forState:UIControlStateNormal];
    [_driveBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Drive"] forState:UIControlStateNormal];
    [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global"] forState:UIControlStateNormal];
    
    [_walkBtn setEnabled:NO];
    [_driveBtn setEnabled:NO];
    [_globalBtn setEnabled:NO];
    
    /*if(selectedType == 0) {
        [_walkBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Walk_Hover"] forState:UIControlStateNormal];
    } else if(selectedType == 1) {
        [_driveBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Drive_Hover"] forState:UIControlStateNormal];
    } else if(selectedType == 2) {
        [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global_Hover"] forState:UIControlStateNormal];
    }*/
    
    _advanceView.hidden = YES;
    advancePoint = _advanceView.center.y;
    
    _topCategoryView.hidden = YES;
    topCategoryPoint = _topCategoryView.center.y;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLocationNotification:) name:@"locationNotification" object:nil];
    
    if([[[locationSingleton Instance] status] isEqual: @""]) {
        [locationSingleton Instance];
    } else if([[[locationSingleton Instance] status] isEqual: @"ready"]) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"ready" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
    } else if([[[locationSingleton Instance] status] isEqual: @"deny"]) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"deny" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
    }
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:3];
    
    [topFoodTypeArray removeAllObjects];
    [topFoodArray removeAllObjects];
    [topCuisineTypeArray removeAllObjects];
    [topCuisineArray removeAllObjects];
    [checkArray removeAllObjects];
    
    [[APISingleton Instance] get_top_category:[[languageSingleton Instance] getCurrentLanguageForAPI] :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                for(NSDictionary *index in json[@"results"][@"food_category"]) {
                    [topFoodTypeArray addObject:@"food_category"];
                    [topFoodArray addObject:index];
                }
                
                for(NSDictionary *index in json[@"results"][@"food_meal"]) {
                    [topFoodTypeArray addObject:@"food_meal"];
                    [topFoodArray addObject:index];
                }
                
                for(NSDictionary *index in json[@"results"][@"food_cuisine"]) {
                    [topCuisineTypeArray addObject:@"food_cuisine"];
                    [topCuisineArray addObject:index];
                    [checkArray addObject:@""];
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_topFoodCollectionView reloadData];
        [_topCuisineCollectionView reloadData];
    }];

    
    refresher = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshAutocompleteTable) userInfo:nil repeats:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_searchBar setCustomWithOffset:CGPointMake(-10, 8) andWidth:_searchBarBG.frame.size.width];
        [_locationBar setCustomWithOffset:CGPointMake(-10, 8) andWidth:_locationBarBG.frame.size.width];
    } else {
        [_searchBar setCustomWithOffset:CGPointMake(-10, 0) andWidth:_searchBarBG.frame.size.width];
        [_locationBar setCustomWithOffset:CGPointMake(-10, 0) andWidth:_locationBarBG.frame.size.width];
    }
    
    //[SVProgressHUD dismiss];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [refresher invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setUILanguage {
    self.navigationItem.title = [[languageSingleton Instance] getTextForKey:@"homePage"];
    _searchBar.placeholder = [[languageSingleton Instance] getTextForKey:@"searchForRestaurant"];
    _locationBar.placeholder = [[languageSingleton Instance] getTextForKey:@"searchForLocation"];
    _selectDist.text = [[languageSingleton Instance] getTextForKey:@"selectDistance"];
    _searchNear.text = [[languageSingleton Instance] getTextForKey:@"searchNear"];
    _anotherLocation.text = [[languageSingleton Instance] getTextForKey:@"anotherLocation"];
    
    _topFoodLabel.text = [[languageSingleton Instance] getTextForKey:@"food_category"];
    _topCuisineLabel.text = [[languageSingleton Instance] getTextForKey:@"food_cuisine"];
    _topCuisineDoneLabel.text = [[languageSingleton Instance] getTextForKey:@"done"];
    
    _searchBar.text = @"";
    [self touchSearchTypeClose:self];
    _searchBar.autoCompleteTableViewHidden = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"searchResultList"]) {
        resultTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.searchResultList = [vc.viewControllers objectAtIndex:0];
        vc.searchResultPin = [vc.viewControllers objectAtIndex:1];
        
        vc.searchResultList.searchText = _searchBar.text;
        
        //vc.searchResultList.resultJSON = vc.searchResultPin.resultJSON  = resultJSON;
        vc.searchResultList.resultJSON  = resultJSON;
        vc.searchResultList.SearchConditionModel = [SearchConditionModel clone];
        
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    } else if([[segue identifier] isEqualToString:@"searchFoodList"]) {
        resultFoodTabBarVC *vc = [segue destinationViewController];
        
        vc.searchFoodList = [vc.viewControllers objectAtIndex:0];
        vc.searchFoodPin = [vc.viewControllers objectAtIndex:1];
        
        vc.searchFoodList.selectedType = selectedType;
        vc.searchFoodList.searchText = _searchBar.text;
        
        vc.searchFoodList.resultJSON = resultJSON;
        vc.searchFoodList.SearchConditionModel = [SearchConditionModel clone];
        
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    }
}

- (void)getLocationNotification:(NSNotification *)notification {
    NSDictionary *dictionary = [notification userInfo];
    NSString *statusString = [dictionary valueForKey:@"status"];
    
    if([statusString isEqual: @"ready"]) {
        [_walkBtn setEnabled:YES];
        [_driveBtn setEnabled:YES];
        [_globalBtn setEnabled:YES];
        
        if(selectedType != 2) [self touchWalk:nil];
        else [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global_Hover"] forState:UIControlStateNormal];
    } else if([statusString isEqual: @"deny"]) {
        [_globalBtn setEnabled:YES];
        
        if(selectedType != 2) [self touchGlobal:nil];
        else [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global_Hover"] forState:UIControlStateNormal];
    } else if([statusString isEqual: @"update"]) {
        currentLocation = [[locationSingleton Instance] currentLocation];
    } else if([statusString isEqual: @"error"]) {
        [_globalBtn setEnabled:YES];
        
        if(selectedType != 2) [self touchGlobal:nil];
        else [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global_Hover"] forState:UIControlStateNormal];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _searchBar) {
        [self do_search_by_keyword];
        [self dismissKeyboardForSearch:nil];
    } else if(textField == _locationBar) {
        [self selectLocation];
        [self dismissKeyboardForLocation:nil];
    }
    
    return  YES;
}

- (void)do_search_by_keyword {
    //Old
    /*if(_searchBar.text.length > 0) {
        [[APISingleton Instance] search_by_keyword:_searchBar.text :@"en" :@"en" :YES :^(id json) {
            if([json count] > 0) {
                resultJSON = json;
                
                [self performSegueWithIdentifier:@"searchResultList" sender:self];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
            
        }];
    }*/
    
    if([searchType isEqual: @""]) {
        [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"selectDropdown"] maskType:SVProgressHUDMaskTypeBlack];
        return ;
    }
    
    if(_searchBar.text.length > 0) {
        NSString *searchLat = @"";
        NSString *searchLng = @"";
        NSString *searchRadius = @"";
        
        if(selectedType == 0) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = walkRadius;
        } else if(selectedType == 1) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = driveRadius;
        } else if(selectedType == 2) {
            if(selectLng != nil) {
                searchLat = selectLat;
                searchLng = selectLng;
                searchRadius = radius;
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                return;
            }
        }
        
        [[APISingleton Instance] search2:searchLat :searchLng :searchRadius :_searchBar.text :[[languageSingleton Instance] getCurrentLanguageForAPI] :searchType :@"" :@"" :YES :^(id json) {
            if([json[@"status_code"] isEqual: @"200"]) {
                if([json[@"results"] count] > 0) {
                    [SearchConditionModel initWithJSON:json];
                    resultJSON = json[@"results"];
                    
                    [self getPlaceNameByLat:searchLat withLng:searchLng];
                } else {
                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
            }
        }];
    } else {
        [self showTopCategory];
    }
}

- (void)getPlaceNameByLat:(NSString *)lat withLng:(NSString *)lng {
    [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lng doubleValue]];
        
        [geocoder reverseGeocodeLocation:searchLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            //NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            
            // strAdd -> take bydefault value nil
            NSString *strAdd = nil;
            
            if (error == nil && [placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks lastObject];
                
                /*if ([placemark.subThoroughfare length] != 0)
                    strAdd = placemark.subThoroughfare;
                
                if ([placemark.thoroughfare length] != 0) {
                    // strAdd -> store value of current location
                    if ([strAdd length] != 0)
                        strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                    else {
                        // strAdd -> store only this value,which is not null
                        strAdd = placemark.thoroughfare;
                    }
                }
                
                if ([placemark.postalCode length] != 0) {
                    if ([strAdd length] != 0)
                        strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark postalCode]];
                    else
                        strAdd = placemark.postalCode;
                }*/
                
                if ([placemark.locality length] != 0) {
                    if ([strAdd length] != 0)
                        strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                    else
                        strAdd = placemark.locality;
                }
                
                if ([placemark.administrativeArea length] != 0) {
                    if ([strAdd length] != 0)
                        strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                    else
                        strAdd = placemark.administrativeArea;
                }
                
                if ([placemark.country length] != 0) {
                    if ([strAdd length] != 0)
                        strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                    else
                        strAdd = placemark.country;
                }
            } else {
                strAdd = @"Unknown";
            }
            
            SearchConditionModel.searchPlace = strAdd;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                
                NSArray *splited = [SearchConditionModel.searchType componentsSeparatedByString:@"_"];
                if([(NSString *)(splited[0]) isEqual: @"food"]) {
                    
                    if([splited[1] isEqual: @"name"]) {
                        [self performSegueWithIdentifier:@"searchFoodList" sender:self];
                    } else if([splited[1] isEqual: @"category"]) {
                        [self performSegueWithIdentifier:@"searchFoodList" sender:self];
                    } else if([splited[1] isEqual: @"cuisine"]) {
                        [self performSegueWithIdentifier:@"searchResultList" sender:self];
                    } else if([splited[1] isEqual: @"ingredient"]) {
                        [self performSegueWithIdentifier:@"searchFoodList" sender:self];
                    } else if([splited[1] isEqual: @"meal"]) {
                        [self performSegueWithIdentifier:@"searchFoodList" sender:self];
                    }
                } else if([(NSString *)(splited[0]) isEqual: @"restaurant"]) {
                    [self performSegueWithIdentifier:@"searchResultList" sender:self];
                }
            });
        }];
        
        
    });
}

- (void)showTopCategory {
    _searchBar.hidden = YES;
    _topCategoryView.hidden = NO;
    
    [_topCategoryView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_topCategoryView.frame.size.height / 2))];
    [UIView animateWithDuration:0.2f animations:^{
        [_topCategoryView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, topCategoryPoint)];
    }completion:^(BOOL finished) {
        //Fix stupid bug on iPad
        [_locationBar becomeFirstResponder];
        [_locationBar resignFirstResponder];
    }];
}

/*- (void)do_autocomplete {
    if(_searchBar.text.length > 0) {
        [[APISingleton Instance] autocomplete:_searchBar.text :@"en" :YES :^(id json) {
            if([json count] > 0) {
                
            } else  {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        }];
    }
}*/

- (void)selectLocation {
    if(_locationBar.text.length > 0) {
        if(locationAutocompleteArray.count > 0) {
            GMSAutocompletePrediction *result = nil;
            selectLat = nil;
            selectLng = nil;
            
            for(GMSAutocompletePrediction *index in locationAutocompleteArray) {
                if([[index.attributedFullText.string lowercaseString] hasPrefix:[_locationBar.text lowercaseString]]) {
                    result = index;
                    break;
                }
            }
            
            if(result != nil) {
                _locationBar.text = result.attributedFullText.string;
                
                GMSPlacesClient *placesClient = [[GMSPlacesClient alloc] init];
                [placesClient lookUpPlaceID:result.placeID callback:^(GMSPlace *place, NSError *error) {
                    if (error != nil) {
                        NSLog(@"Place Details error %@", [error localizedDescription]);
                        return;
                    }
                    
                    if (place != nil) {
                        selectLat = [NSString stringWithFormat:@"%f", place.coordinate.latitude];
                        selectLng = [NSString stringWithFormat:@"%f", place.coordinate.longitude];
                    } else {
                        NSLog(@"No place details for %@", result.placeID);
                    }
                }];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }
}

- (void)dismissKeyboardForSearch:(UIGestureRecognizer *)gestureRecognizer {
    [_searchBar resignFirstResponder];
}

- (void)dismissKeyboardForLocation:(UIGestureRecognizer *)gestureRecognizer {
    [_locationBar resignFirstResponder];
}

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField possibleCompletionsForString:(NSString *)string {
    if(textField == _searchBar) {
        if(isAutocompleteReady) {
            return [autocompleteArray copy];
        }
        
        return @[];
    } else if(textField == _locationBar) {
        if(isLocationAutocompleteReady) {
            NSMutableArray *stringResult = [[NSMutableArray alloc] init];
            for(GMSAutocompletePrediction *result in locationAutocompleteArray) {
                [stringResult addObject:result.attributedFullText.string];
            }
            return [stringResult copy];
        }
        
        return @[];
    }
    
    return @[];
}

- (IBAction)callSettingMenu:(id)sender {
    settingMenuController *_settingMenuController = [[settingMenuController alloc] initWithViewController:self];
    [_settingMenuController callSettingMenu];
}

- (IBAction)searchEditing:(id)sender {
    isAutocompleteReady = NO;
    [autocompleteArray removeAllObjects];
    [searchTypeArray removeAllObjects];
    
    if([_searchBar.text length] >= 4) {
     /*[[APISingleton Instance] search_restaurant:_searchBar.text :@"en" :NO :^(id json) {
     if([json count] > 0) {
     for(NSDictionary *index in json[@"results"]) {
     [autocompleteArray addObject:[NSString stringWithFormat:@"%@#%@", index[@"name"], @"Category"]];
     }
     
     isAutocompleteReady = YES;
     }
     }];*/
        
        NSString *searchLat = @"";
        NSString *searchLng = @"";
        NSString *searchRadius = @"";
        
        if(selectedType == 0) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = walkRadius;
        } else if(selectedType == 1) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = driveRadius;
        } else if(selectedType == 2) {
            if(selectLng != nil) {
                searchLat = selectLat;
                searchLng = selectLng;
                searchRadius = radius;
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                return;
            }
        }
        
        _loadingView.hidden = NO;
        [spinner startAnimating];
    
        [[APISingleton Instance] autocomplete:_searchBar.text :searchLat :searchLng :searchRadius :[[languageSingleton Instance] getCurrentLanguageForAPI] :NO :^(id json) {
            if([json[@"status_code"] isEqual: @"200"]) {
                if([json[@"results"] count] > 0) {
                    for(int i = 0; i < [[[dataSingleton Instance] searchCategoryArray] count]; i++) {
                        NSString *searchCategory = (NSString *)[[[dataSingleton Instance] searchCategoryArray] objectAtIndex:i];
                        //NSString *searchCategoryDisplay = (NSString *)[[[dataSingleton Instance] searchCategoryDisplayArray] objectAtIndex:i];
                        //NSString *searchCategoryDisplay = [[languageSingleton Instance] getTextForKey:searchCategory];
                        NSString *searchCategoryDisplay = searchCategory;
                        
                        for(NSDictionary *index in json[@"results"][searchCategory][@"results"]) {
                            [searchTypeArray addObject:searchCategory];
                            if([index[@"id"] isEqual: @"0"]) {
                                [autocompleteArray addObject:[NSString stringWithFormat:@"!%@#%@", index[@"name"], searchCategoryDisplay]];
                            } else {
                                [autocompleteArray addObject:[NSString stringWithFormat:@"%@#%@", index[@"name"], searchCategoryDisplay]];
                            }
                            
                            searchCategoryDisplay = @" ";
                        }
                    }
                    
                    isAutocompleteReady = YES;
                    if(_searchBar.autoCompleteTableViewHidden) _searchBar.autoCompleteTableViewHidden = NO;
                }
            } else {
                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
            }
            
            [spinner stopAnimating];
            _loadingView.hidden = YES;
        }];
    }
}

- (IBAction)searchBegin:(id)sender {
    //CGFloat PORTRAIT_KEYBOARD_HEIGHT = self.view.frame.size.height * 0.35;
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    
    //For oneday someone want to change layout
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        PORTRAIT_KEYBOARD_HEIGHT = self.view.frame.size.height * 0.7;
    } else {
        PORTRAIT_KEYBOARD_HEIGHT = self.view.frame.size.height * 0.7;
    }
    
    CGRect textFieldRect = [self.view.window convertRect:_searchBar.bounds fromView:_searchBar];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0) {
        heightFraction = 0.0;
    } else if (heightFraction > 1.0) {
        heightFraction = 1.0;
    }
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (IBAction)searchEnd:(id)sender {
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (IBAction)touchSearch:(id)sender {
    [self dismissKeyboardForSearch:nil];
    [self do_search_by_keyword];
}

- (IBAction)touchWalk:(id)sender {
    [self dismissKeyboardForSearch:nil];
    selectedType = 0;
    
    [_walkBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Walk_Hover"] forState:UIControlStateNormal];
    [_driveBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Drive"] forState:UIControlStateNormal];
    [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global"] forState:UIControlStateNormal];
}

- (IBAction)touchDrive:(id)sender {
    [self dismissKeyboardForSearch:nil];
    selectedType = 1;
    
    [_walkBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Walk"] forState:UIControlStateNormal];
    [_driveBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Drive_Hover"] forState:UIControlStateNormal];
    [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global"] forState:UIControlStateNormal];
}

- (IBAction)touchGlobal:(id)sender {
    [self dismissKeyboardForSearch:nil];
    selectedType = 2;
    
    [_walkBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Walk"] forState:UIControlStateNormal];
    [_driveBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Drive"] forState:UIControlStateNormal];
    [_globalBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Search_Global_Hover"] forState:UIControlStateNormal];
    
    _searchBar.hidden = YES;
    _advanceView.hidden = NO;
    
    [_advanceView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_advanceView.frame.size.height / 2))];
    [UIView animateWithDuration:0.2f animations:^{
        [_advanceView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, advancePoint)];
    }completion:^(BOOL finished) {
        //Fix stupid bug on iPad
        [_locationBar becomeFirstResponder];
        [_locationBar resignFirstResponder];
    }];
}

- (IBAction)touchAdvanceClose:(id)sender {
    if(_locationBar.isFirstResponder) {
        [self dismissKeyboardForLocation:nil];
    } else {
        _searchBar.hidden = NO;
        
        [UIView animateWithDuration:0.2f animations:^{
            [_advanceView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_advanceView.frame.size.height / 2))];
        }completion:^(BOOL finished) {
            _advanceView.hidden = YES;
        }];
    }
}

- (IBAction)slideAdvanceDist:(id)sender {
    radius = [NSString stringWithFormat:@"%d", (int)_sliderDist.value];
    _currentKm.text = [NSString stringWithFormat:@" %@ km", radius];
}

- (IBAction)touchAdvanceKm:(id)sender {
}

- (IBAction)locationEditing:(id)sender {
    isLocationAutocompleteReady = NO;
    
    if([_locationBar.text length] >= 1) {
        GMSPlacesClient *placesClient = [[GMSPlacesClient alloc] init];
        /*GMSVisibleRegion visibleRegion = _mapView.projection.visibleRegion;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft coordinate:visibleRegion.nearRight];*/
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterRegion;
        
        [placesClient autocompleteQuery:_locationBar.text bounds:nil filter:filter callback:^(NSArray *results, NSError *error) {
            if (error != nil) {
                NSLog(@"Location Autocomplete error %@", [error localizedDescription]);
                return;
            }
            
            //NSLog(@"%d", results.count);
            isLocationAutocompleteReady = YES;
            [locationAutocompleteArray removeAllObjects];
            for(GMSAutocompletePrediction *result in results) {
                //NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
                [locationAutocompleteArray addObject:result];
            }
        }];
    }
}

- (IBAction)locationBegin:(id)sender {
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        PORTRAIT_KEYBOARD_HEIGHT = self.view.frame.size.height * 0.9 * 0.7;
    } else {
        PORTRAIT_KEYBOARD_HEIGHT = self.view.frame.size.height * 0.9;
    }
    
    CGRect textFieldRect = [self.view.window convertRect:_searchBar.bounds fromView:_searchBar];
    CGRect viewRect = [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0) {
        heightFraction = 0.0;
    } else if (heightFraction > 1.0) {
        heightFraction = 1.0;
    }
    
    animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (IBAction)locationEnd:(id)sender {
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (IBAction)touchLocation:(id)sender {
    [self selectLocation];
    [self dismissKeyboardForLocation:nil];
}

- (IBAction)touchSearchTypeClose:(id)sender {
    [self hideSearchType];
}

- (IBAction)touchTopCategory:(id)sender {
    [self showTopCategory];
}

- (IBAction)touchTopCategoryClose:(id)sender {
    _searchBar.hidden = NO;
    
    [UIView animateWithDuration:0.2f animations:^{
        [_topCategoryView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_topCategoryView.frame.size.height / 2))];
    } completion:^(BOOL finished) {
        _topCategoryView.hidden = YES;
    }];
}

- (IBAction)touchTopFood:(id)sender {
    _topFoodBtn.userInteractionEnabled = NO;
    [_topFoodBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Segment_Open"] forState:UIControlStateNormal];
    _topFoodCollectionView.hidden = NO;
    
    _topCuisineBtn.userInteractionEnabled = YES;
    [_topCuisineBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Segment_Close"] forState:UIControlStateNormal];
    _topCuisineCollectionView.hidden = YES;
    
    _topCuisineDoneBtn.hidden = YES;
    _topCuisineDoneLabel.hidden = YES;
    
}

- (IBAction)touchTopCuisine:(id)sender {
    _topCuisineBtn.userInteractionEnabled = NO;
    [_topCuisineBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Segment_Open"] forState:UIControlStateNormal];
    _topCuisineCollectionView.hidden = NO;
    
    _topFoodBtn.userInteractionEnabled = YES;
    [_topFoodBtn setBackgroundImage:[UIImage imageNamed:@"BTN_Segment_Close"] forState:UIControlStateNormal];
    _topFoodCollectionView.hidden = YES;
    
    _topCuisineDoneBtn.hidden = NO;
    _topCuisineDoneLabel.hidden = NO;
    
}

- (IBAction)touchTopCuisineDone:(id)sender {
    NSString *selectList = @"";
    
    for(int i = 0; i < checkArray.count; i++) {
        if(![(NSString *)checkArray[i] isEqual: @""]) {
            if([selectList isEqual: @""]) {
                selectList = topCuisineArray[i][@"id"];
            } else {
                selectList = [NSString stringWithFormat:@"%@,%@", selectList, topCuisineArray[i][@"id"]];
            }
        }
    }
    
    if(![selectList isEqual: @""]) {
        NSString *searchLat = @"";
        NSString *searchLng = @"";
        NSString *searchRadius = @"";
        
        if(selectedType == 0) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = walkRadius;
        } else if(selectedType == 1) {
            searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
            searchRadius = driveRadius;
        } else if(selectedType == 2) {
            if(selectLng != nil) {
                searchLat = selectLat;
                searchLng = selectLng;
                searchRadius = radius;
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                return;
            }
        }
        
        [[APISingleton Instance] search2:searchLat :searchLng :searchRadius :@"" :[[languageSingleton Instance] getCurrentLanguageForAPI] :@"" :selectList :@"food_cuisine":YES :^(id json) {
            if([json[@"status_code"] isEqual: @"200"]) {
                if([json[@"results"] count] > 0) {
                    [SearchConditionModel initWithJSON:json];
                    resultJSON = json[@"results"];
                    
                    [self getPlaceNameByLat:searchLat withLng:searchLng];
                } else {
                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
            }
        }];
    }
    //NSLog(@"%@", selectList);
}

- (void)selectItem:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_searchBar.autoCompleteTableView];
        NSIndexPath *tapLocationIndexPath = [_searchBar.autoCompleteTableView indexPathForRowAtPoint:tapLocation];
        if(tapLocationIndexPath != nil) {
            //UITableViewCell* tapCell = [_searchBar.autoCompleteTableView cellForRowAtIndexPath:tapLocationIndexPath];
            
            if(autocompleteArray.count > 0) {
                NSString *selectString = (NSString *)[autocompleteArray objectAtIndex:tapLocationIndexPath.row];
                NSArray *selectArray = [selectString componentsSeparatedByString:@"#"];
                
                _searchBar.text = selectArray[0];
                
                if([[_searchBar.text substringToIndex:1] isEqual: @"!"]) {
                    _searchBar.text = [selectArray[0] substringFromIndex:1];
                }
                
                searchType = (NSString *)[searchTypeArray objectAtIndex:tapLocationIndexPath.row];
                [self showSearchType];
            }
        }
    }
}

- (void)selectLocation:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_locationBar.autoCompleteTableView];
        NSIndexPath *tapLocationIndexPath = [_locationBar.autoCompleteTableView indexPathForRowAtPoint:tapLocation];
        if(tapLocationIndexPath != nil) {
            UITableViewCell* tapCell = [_locationBar.autoCompleteTableView cellForRowAtIndexPath:tapLocationIndexPath];
            
            _locationBar.text = tapCell.textLabel.text;
            [self textFieldShouldReturn:_locationBar];
        }
    }
}

- (void)swipeAdvanceBG:(UISwipeGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        if(_locationBar.isFirstResponder) {
            [self dismissKeyboardForLocation:nil];
        } else {
            _searchBar.hidden = NO;
            
            [UIView animateWithDuration:0.2f animations:^{
                [_advanceView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_advanceView.frame.size.height / 2))];
            }completion:^(BOOL finished) {
                _advanceView.hidden = YES;
            }];
        }
    }
}

- (void)swipeTopCategoryBG:(UISwipeGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        _searchBar.hidden = NO;
        
        [UIView animateWithDuration:0.2f animations:^{
            [_topCategoryView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_topCategoryView.frame.size.height / 2))];
        }completion:^(BOOL finished) {
            _topCategoryView.hidden = YES;
        }];
    }
}

- (void)showSearchType {
    _searchTypeView.hidden = NO;
    //_searchTypeLabel.text = [[dataSingleton Instance] convertSearchCategoryToSearchCategoryDisplay:searchType];
    _searchTypeLabel.text = [[languageSingleton Instance] getTextForKey:searchType];
    
    /*if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        _searchBarLeading.constant = 250.0f;
        [_searchBar setCustomWithOffset:CGPointMake(-235, 8) andWidth:_searchBarBG.frame.size.width];
    } else {
        _searchBarLeading.constant = 110.0f;
        [_searchBar setCustomWithOffset:CGPointMake(-95, 0) andWidth:_searchBarBG.frame.size.width];
    }*/
}

- (void)hideSearchType {
    _searchTypeView.hidden = YES;
    searchType = @"";
    
    /*if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        _searchBarLeading.constant = 25.0f;
        [_searchBar setCustomWithOffset:CGPointMake(-10, 8) andWidth:_searchBarBG.frame.size.width];
    } else {
        _searchBarLeading.constant = 25.0f;
        [_searchBar setCustomWithOffset:CGPointMake(-10, 0) andWidth:_searchBarBG.frame.size.width];
    }*/
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == _topFoodCollectionView) {
        return CGSizeMake(self.view.frame.size.width / 9.6, self.view.frame.size.width / 9.6);
    } else if(collectionView == _topCuisineCollectionView) {
        return CGSizeMake(_topCuisineCollectionView.frame.size.width, 40);
    }
    
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if(collectionView == _topFoodCollectionView) {
        return UIEdgeInsetsMake(20, 40, 20, 40);
    } else if(collectionView == _topCuisineCollectionView) {
        return UIEdgeInsetsMake(10, 0, 0, 0);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == _topFoodCollectionView) {
        return topFoodArray.count;
    } else if(collectionView == _topCuisineCollectionView) {
        return topCuisineArray.count;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    
    if(collectionView == _topFoodCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantCategoryCell" forIndexPath:indexPath];
        restaurantCategoryCell *thisCell = (restaurantCategoryCell*)cell;
        
        if(![(NSString *)(topFoodArray[indexPath.row][@"icon"]) isEqual: @""]) {
            [thisCell.cellImg setImageWithURL:[[APISingleton Instance] get_img_url:@"" :(NSString *)(topFoodArray[indexPath.row][@"icon"])] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        } else {
            [thisCell.cellImg setImage:[UIImage imageNamed:@"BTN_Tab_Food"]];
        }
    } else if(collectionView == _topCuisineCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"checkboxCell" forIndexPath:indexPath];
        checkboxCell *thisCell = (checkboxCell*)cell;
        
        thisCell.cellLabel.text = (NSString *)topCuisineArray[indexPath.row][@"name"];
        if([(NSString *)checkArray[indexPath.row] isEqual: @""]) {
            thisCell.checkImg.hidden = YES;
        } else {
            thisCell.checkImg.hidden = NO;
        }
    }
    
    return cell;
}

- (void)selectTopFood:(UIGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_topFoodCollectionView];
        NSIndexPath *tapLocationIndexPath = [_topFoodCollectionView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            NSString *searchLat = @"";
            NSString *searchLng = @"";
            NSString *searchRadius = @"";
            
            if(selectedType == 0) {
                searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
                searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
                searchRadius = walkRadius;
            } else if(selectedType == 1) {
                searchLat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
                searchLng = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
                searchRadius = driveRadius;
            } else if(selectedType == 2) {
                if(selectLng != nil) {
                    searchLat = selectLat;
                    searchLng = selectLng;
                    searchRadius = radius;
                } else {
                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                    return;
                }
            }
            
            [[APISingleton Instance] search2:searchLat :searchLng :searchRadius :@"" :[[languageSingleton Instance] getCurrentLanguageForAPI] :@"" :(NSString *)(topFoodArray[tapLocationIndexPath.row][@"id"]) :(NSString *)topFoodTypeArray[tapLocationIndexPath.row]:YES :^(id json) {
                if([json[@"status_code"] isEqual: @"200"]) {
                    if([json[@"results"] count] > 0) {
                        [SearchConditionModel initWithJSON:json];
                        resultJSON = json[@"results"];
                        
                        [self getPlaceNameByLat:searchLat withLng:searchLng];
                    } else {
                        [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                    }
                } else {
                    [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                }
            }];
        }
    }
}

- (void)pressTopFood:(UILongPressGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_topFoodCollectionView];
        NSIndexPath *tapLocationIndexPath = [_topFoodCollectionView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            NSString *toastMsg = (NSString *)topFoodArray[tapLocationIndexPath.row][@"name"];
            [self.view makeToast:toastMsg];
        }
    }
}

- (void)selectTopCuisine:(UIGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_topCuisineCollectionView];
        NSIndexPath *tapLocationIndexPath = [_topCuisineCollectionView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            checkboxCell *cell = (checkboxCell *)[_topCuisineCollectionView cellForItemAtIndexPath:tapLocationIndexPath];
            
            cell.checkImg.hidden = !cell.checkImg.hidden;
            if(cell.checkImg.hidden) {
                checkArray[tapLocationIndexPath.row] = @"";
            } else {
                checkArray[tapLocationIndexPath.row] = @"CHECK";
            }
        }
    }
}

- (void)refreshAutocompleteTable {
    if(_searchBar.isFirstResponder) {
        [_searchBar becomeFirstResponder];
    }
}

@end
