//
//  searchFoodListCell.h
//  menu360
//
//  Created by Apps360[admin] on 3/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "restaurantModel.h"
#import "searchFoodListVC.h"
#import "APISingleton.h"

@interface searchFoodListCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (strong, nonatomic) IBOutlet UIImageView *cellBg;
@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellName;
@property (strong, nonatomic) IBOutlet UILabel *cellPrice;
@property (strong, nonatomic) IBOutlet UILabel *cellRestaurentName;

@property (strong, nonatomic) NSString *restaurantId;
@property (strong, nonatomic) UIViewController *parentVC;

- (IBAction)touchRestaurantName:(id)sender;

@end
