//
//  mapSearchViewController.h
//  menu360
//
//  Created by PandaMaru on 6/10/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import "settingMenuController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import <MLPAutoCompleteTextField.h>
#import <SVProgressHUD.h>
#import "APISingleton.h"
#import "customInfoWindow.h"
#import "restaurantModel.h"
#import <AFNetworking+ImageActivityIndicator.h>
#import "restaurantTabBarVC.h"
#import "customAutoCompleteTextField.h"
#import "locationSingleton.h"

@interface mapSearchVC : UIViewController <multiLanguageProtocol, GMSMapViewDelegate, CLLocationManagerDelegate, MLPAutoCompleteTextFieldDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UIImageView *searchBarBG;
@property (strong, nonatomic) IBOutlet customAutoCompleteTextField *searchBar;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UIButton *locationBtn;
@property (strong, nonatomic) IBOutlet UIImageView *searchBarImg;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchBarAL;

- (IBAction)callSettingMenu:(id)sender;
- (IBAction)searchEditing:(id)sender;
- (IBAction)searchBegin:(id)sender;
- (IBAction)searchEnd:(id)sender;
- (IBAction)touchSearch:(id)sender;
- (IBAction)touchLocation:(id)sender;

@end
