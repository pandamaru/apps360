//
//  multiLanguageProtocol.h
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol multiLanguageProtocol <NSObject>

- (void)setUILanguage;

@end
