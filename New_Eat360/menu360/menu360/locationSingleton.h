//
//  locationSingleton.h
//  menu360
//
//  Created by Apps360[admin] on 19/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface locationSingleton : NSObject <CLLocationManagerDelegate>

+ (id)Instance;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;

@end
