//
//  searchResultListViewController.h
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import "searchResultListCell.h"
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "restaurantTabBarVC.h"
#import "restaurantModel.h"
#import "APISingleton.h"
#import <AFNetworking+ImageActivityIndicator.h>
#import "searchResultPinVC.h"
#import "restaurantMapVC.h"
#import <FBSDKCoreKit.h>
#import <FBSDKShareKit.h>
#import "searchConditionModel.h"

@interface searchResultListVC : UIViewController <multiLanguageProtocol, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITabBarControllerDelegate, UITextFieldDelegate> {
    
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSDictionary *resultJSON;
@property (strong, nonatomic) searchConditionModel *SearchConditionModel;

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UITextField *filterBar;
@property (strong, nonatomic) IBOutlet UIImageView *filterBarImg;
@property (strong, nonatomic) IBOutlet UIButton *filterBtn;

@property (strong, nonatomic) IBOutlet UIView *searchTypeView;
@property (strong, nonatomic) IBOutlet UILabel *searchTypeLabel;
@property (strong, nonatomic) IBOutlet UIButton *searchTypeCloseBtn;

- (IBAction)filterChange:(id)sender;
- (IBAction)filterBegin:(id)sender;
- (IBAction)touchFilter:(id)sender;

- (IBAction)touchSearchTypeClose:(id)sender;

@end
