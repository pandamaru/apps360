//
//  AppDelegate.h
//  menu360
//
//  Created by PandaMaru on 6/3/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MFSideMenuContainerViewController.h>
#import "languageSingleton.h"
#import <GoogleMaps/GoogleMaps.h>
#import "appearanceSingleton.h"
#import <FBSDKCoreKit.h>
#import "dataSingleton.h"
#import "sideMenuVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

