//
//  customAutoCompleteCell.h
//  menu360
//
//  Created by Apps360[admin] on 17/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customAutoCompleteCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *cellCategory;
@property (strong, nonatomic) IBOutlet UILabel *cellText;

@end
