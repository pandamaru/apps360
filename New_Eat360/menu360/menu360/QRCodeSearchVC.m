//
//  QRCodeSearchViewController.m
//  menu360
//
//  Created by PandaMaru on 6/10/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "QRCodeSearchVC.h"

@interface QRCodeSearchVC ()

@end

@implementation QRCodeSearchVC {
    restaurantModel *selectedRestaurant;
    
    CGFloat tabBarHeight;
    
    BOOL isReading;
    BOOL isShouldReading;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Insert burger image to NavBar right item
    [self.navigationItem.rightBarButtonItem setImage:[[UIImage imageNamed:@"BTN_Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UITabBarItem *nameItem = [self.tabBarController.tabBar.items objectAtIndex:0];
    nameItem.image = [[UIImage imageNamed:@"BTN_Tab_Name"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *mapItem = [self.tabBarController.tabBar.items objectAtIndex:1];
    mapItem.image = [[UIImage imageNamed:@"BTN_Tab_Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBarHeight = self.tabBarController.tabBar.frame.size.height;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self startReading];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:3];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.tabBarController.tabBar.hidden = NO;
    self.tabBarController.tabBar.layer.frame = CGRectMake(self.tabBarController.tabBar.layer.frame.origin.x, self.tabBarController.tabBar.layer.frame.origin.y, self.tabBarController.tabBar.layer.frame.size.width, tabBarHeight);
}

- (void)viewWillDisappear:(BOOL)animated {
    [self stopReading];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    self.navigationItem.title = [[languageSingleton Instance] getTextForKey:@"qrCodeSearch"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"restaurantMenu"]) {
        restaurantTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurantFood = [vc.viewControllers objectAtIndex:0];
        vc.restaurantDrink = [vc.viewControllers objectAtIndex:1];
        vc.restaurantOther = [vc.viewControllers objectAtIndex:2];
        
        vc.restaurantFood.RestaurantModel = vc.restaurantDrink.RestaurantModel = vc.restaurantOther.RestaurantModel = selectedRestaurant;
        
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    }
}

- (BOOL)startReading {
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return NO;
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    
    AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("myQueue", NULL);
    [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_viewPreview.layer.bounds];
    [_viewPreview.layer addSublayer:_videoPreviewLayer];
    
    [_captureSession startRunning];
    
    isReading = isShouldReading = YES;
    
    return YES;
}

- (void)stopReading {
    [_captureSession stopRunning];
    _captureSession = nil;
    
    [_videoPreviewLayer removeFromSuperlayer];
    
    isReading = isShouldReading = NO;
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0 && isShouldReading) {
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
            isShouldReading = NO;
            
            [[APISingleton Instance] search_by_qr:[metadataObj stringValue] :@"en" :YES :^(id json) {
                if([json[@"status_code"] isEqual: @"200"]) {
                    if([json[@"results"] count] > 0) {
                        restaurantModel *thisModel = [[restaurantModel alloc] init];
                        [thisModel initWithJSON:json[@"results"][0]];
                        selectedRestaurant = thisModel;
                        
                        self.tabBarController.tabBar.hidden = YES;
                        self.tabBarController.tabBar.layer.frame = CGRectMake(self.tabBarController.tabBar.layer.frame.origin.x, self.tabBarController.tabBar.layer.frame.origin.y, self.tabBarController.tabBar.layer.frame.size.width, 0);
                        [self performSegueWithIdentifier:@"restaurantMenu" sender:self];
                    } else {
                        UIAlertView *alert = [[UIAlertView alloc]
                                              initWithTitle:[[languageSingleton Instance] getTextForKey:@"error"]
                                              message:[[languageSingleton Instance] getTextForKey:@"tryAgain"]
                                              delegate:self
                                              cancelButtonTitle:[[languageSingleton Instance] getTextForKey:@"done"]
                                              otherButtonTitles:nil];
                        [alert show];
                    }
                } else {
                    [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                }
            }];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelectorOnMainThread:@selector(startReading) withObject:nil waitUntilDone:NO];
    }
}

- (IBAction)callSettingMenu:(id)sender {
    settingMenuController* _settingMenuController = [[settingMenuController alloc] initWithViewController:self];
    [_settingMenuController callSettingMenu];
}

@end
