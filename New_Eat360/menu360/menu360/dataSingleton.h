//
//  dataSingleton.h
//  menu360
//
//  Created by PandaMaru on 7/17/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "languageSingleton.h"

@interface dataSingleton : NSObject {
    NSString *currentCurr;
}

@property (strong, nonatomic) NSArray *languageArray;
@property (strong, nonatomic) NSArray *currencyArray;
@property (strong, nonatomic) NSArray *searchCategoryArray;
//@property (strong, nonatomic) NSArray *searchCategoryDisplayArray;
//@property (strong, nonatomic) NSArray *categoryPicArray;
@property (strong, nonatomic) NSArray *restaurantTabBarIconArray;

@property (strong, nonatomic) NSString *baseWeb;

@property (nonatomic, assign, setter=setIsSubMenuOpen:) BOOL isSubMenuOpen;
@property (nonatomic, assign, setter=setLastOpenedSubMenu:) int lastOpenedSubMenu;
@property (strong, nonatomic, setter=setSelectedMeal:) NSString *selectedMeal;
@property (strong, nonatomic, setter=setSelectedCategory:) NSString *selectedCategory;

+ (id)Instance;
- (void)setCurrentCurrency:(NSString *)currency;
- (NSString *)getCurrentCurrency;

/*- (NSString *)convertSearchCategoryDisplayToSearchCategory:(NSString *)text;
- (NSString *)convertSearchCategoryToSearchCategoryDisplay:(NSString *)text;*/

/*- (NSString *)getConditionFormat:(NSString *)searchText withType:(NSString *)searchType withLatitude:(NSString *)searchLatitude withLongtitude:(NSString *)searchLongtitude withRadius:(NSString *)searchRadius;
- (NSString *)getUnconditionFormatWithLatitude:(NSString *)searchLatitude withLongtitude:(NSString *)searchLongtitude withRadius:(NSString *)searchRadius;*/

- (NSString *)getConditionFormat:(NSString *)searchText withType:(NSString *)searchType withPlace:(NSString *)searchPlace withRadius:(NSString *)searchRadius;
- (NSString *)getUnconditionFormatWithPlace:(NSString *)searchPlace withRadius:(NSString *)searchRadius;

@end
