//
//  resultFoodTabBar.h
//  menu360
//
//  Created by Apps360[admin] on 1/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "appearanceSingleton.h"
#import "searchFoodListVC.h"
#import "searchFoodPinVC.h"

@interface resultFoodTabBarVC : UITabBarController

@property (strong, nonatomic) searchFoodListVC *searchFoodList;
@property (strong, nonatomic) searchFoodPinVC *searchFoodPin;

@end
