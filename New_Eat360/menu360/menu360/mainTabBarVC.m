//
//  mainTabBarController.m
//  menu360
//
//  Created by PandaMaru on 6/10/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "mainTabBarVC.h"

@interface mainTabBarVC ()

@end

@implementation mainTabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tabBar setTintColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1.0]];
    [[appearanceSingleton Instance] setTabBarAppearance:3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
