//
//  searchCoditionModel.h
//  menu360
//
//  Created by Apps360[admin] on 21/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface searchConditionModel : NSObject

@property (nonatomic, assign, setter=setIsTopCategory:) BOOL isTopCategory;
@property (strong, nonatomic) NSString *searchLatitude;
@property (strong, nonatomic) NSString *searchLongitude;
@property (strong, nonatomic) NSString *searchPlace;
@property (strong, nonatomic) NSString *searchRadius;
@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSString *searchType;

@property (strong, nonatomic) NSString *searchPossibleKey;
@property (strong, nonatomic) NSString *searchPossibleType; //TypeId

- (void)initWithJSON:(NSDictionary*)JSON;

- (searchConditionModel *)clone;

@end
