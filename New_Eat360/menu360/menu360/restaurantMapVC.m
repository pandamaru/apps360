//
//  restaurantMapVC.m
//  menu360
//
//  Created by PandaMaru on 7/27/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "restaurantMapVC.h"

@interface restaurantMapVC ()

@end

@implementation restaurantMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake([_RestaurantModel.latitude doubleValue], [_RestaurantModel.longitude doubleValue]);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.icon = [UIImage imageNamed:@"UI_Map_Marker"];
    marker.map = _mapView;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:marker.position.latitude longitude:marker.position.longitude zoom:11];
    
    [_mapView setCamera:camera];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@", _RestaurantModel.name];
    navCon.navigationItem.rightBarButtonItem = _rightBarButton;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchDirection:(id)sender {
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?saddr=&daddr=%@,%@&directionsmode=driving", _RestaurantModel.latitude, _RestaurantModel.longitude]]];
    } else {
        NSString *iTunesLink = @"https://itunes.apple.com/in/app/google-maps/id585027354?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
    }
}

@end
