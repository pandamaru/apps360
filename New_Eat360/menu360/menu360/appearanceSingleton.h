//
//  appearanceSingleton.h
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface appearanceSingleton : NSObject {
    UIColor *tabBarSelectedColor;
}

+ (id)Instance;
- (void)setTabBarAppearance:(int)tabAmount;

- (UIColor *)getTabBarColor;

- (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius;
- (UIImage *)combineTwoImage:(UIImage *)bg :(UIImage *)fg;
- (UIImage *)imageTintedWithColor:(UIImage *)image :(UIColor *)color;

@end
