//
//  resultTabBarVC.m
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "resultTabBarVC.h"

@interface resultTabBarVC ()

@end

@implementation resultTabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tabBar setTintColor:[UIColor colorWithRed:255 green:255 blue:255 alpha:1.0]];
    [[appearanceSingleton Instance] setTabBarAppearance:2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}*/

@end
