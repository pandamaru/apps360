//
//  searchFoodListVC.h
//  menu360
//
//  Created by Apps360[admin] on 1/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "searchFoodPinVC.h"
#import "foodModel.h"
#import "restaurantModel.h"
#import "searchFoodListCell.h"
#import "searchConditionModel.h"

@interface searchFoodListVC : UIViewController <multiLanguageProtocol, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITabBarControllerDelegate, UITextFieldDelegate> {
    
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) int selectedType;
@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSDictionary *resultJSON;
@property (strong, nonatomic) searchConditionModel *SearchConditionModel;
@property (strong, nonatomic) restaurantModel *RestaurantModel;

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UIImageView *headerIcon;
@property (strong, nonatomic) IBOutlet UILabel *headerLabel;

@end
