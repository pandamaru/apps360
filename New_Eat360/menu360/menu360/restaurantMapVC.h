//
//  restaurantMapVC.h
//  menu360
//
//  Created by PandaMaru on 7/27/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "restaurantModel.h"
#import "multiLanguageProtocol.h"
#import "sideMenuSingleton.h"
#import <GoogleMaps/GoogleMaps.h>

@interface restaurantMapVC : UIViewController <multiLanguageProtocol, GMSMapViewDelegate>

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) restaurantModel *RestaurantModel;
@property (strong, nonatomic) UIBarButtonItem *rightBarButton;

- (IBAction)touchDirection:(id)sender;

@end
