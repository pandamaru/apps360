//
//  mapSearchViewController.m
//  menu360
//
//  Created by PandaMaru on 6/10/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "mapSearchVC.h"

@interface mapSearchVC ()

@end

@implementation mapSearchVC {
    BOOL isAutocompleteReady;
    NSMutableArray *autocompleteArray;
    
    NSMutableArray *markerArray;
    NSMutableArray *restaurantArray;
    NSMutableArray *imgURLArray;
    NSMutableArray *imgArray;
    
    restaurantModel *selectedRestaurant;
    
    CGFloat tabBarHeight;
    
    CLLocation *currentLocation;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"cur_zoom"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@3 forKey:@"cur_zoom"];
        [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:@"cur_lat"];
        [[NSUserDefaults standardUserDefaults] setObject:@0 forKey:@"cur_lng"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //Insert burger image to NavBar right item
    [self.navigationItem.rightBarButtonItem setImage:[[UIImage imageNamed:@"BTN_Menu"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    UITabBarItem *nameItem = [self.tabBarController.tabBar.items objectAtIndex:0];
    nameItem.image = [[UIImage imageNamed:@"BTN_Tab_Name"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *qrItem = [self.tabBarController.tabBar.items objectAtIndex:2];
    qrItem.image = [[UIImage imageNamed:@"BTN_Tab_QR"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    [_searchBar customInit];
    _searchBar.showAutoCompleteTableWhenEditingBegins = YES;
    _searchBar.applyBoldEffectToAutoCompleteSuggestions = NO;
    _searchBar.autoCompleteTableBorderWidth = 2.0f;
    _searchBar.autoCompleteTableBorderColor = [UIColor blackColor];
    _searchBar.autoCompleteTableBackgroundColor = [UIColor whiteColor];
    _searchBar.autoCompleteTableCellTextColor = [UIColor blackColor];
    _searchBar.autoCompleteRegularFontName = @"OpenSans";
    _searchBar.autoCompleteBoldFontName = @"OpenSans-Bold";
    _searchBar.autoCompleteFetchRequestDelay = 1;
    _searchBar.maximumNumberOfAutoCompleteRows = 6;
    
    //Add search bar shadow
    _searchBarImg.layer.shadowColor = [UIColor blackColor].CGColor;
    _searchBarImg.layer.shadowOffset = CGSizeMake(0, 0);
    _searchBarImg.layer.shadowOpacity = 1;
    _searchBarImg.layer.shadowRadius = 2.0;
    _searchBarImg.clipsToBounds = NO;
    
    [_searchBtn setImage:[[UIImage imageNamed:@"UI_Search_Icon@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [_searchBtn setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    
    [_locationBtn setImage:[[UIImage imageNamed:@"UI_Location_Icon@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [_locationBtn setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_searchBar setDefaultTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:40]}];
        _searchBar.autoCompleteFontSize = 40;
        _searchBar.autoCompleteRowHeight = 100;
    }
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"cur_lat"] floatValue] longitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"cur_lng"] floatValue] zoom:[[[NSUserDefaults standardUserDefaults] objectForKey:@"cur_zoom"] floatValue]];
    
    [_mapView setCamera:camera];
    
    _searchBar.delegate = self;
    
    isAutocompleteReady = NO;
    autocompleteArray = [[NSMutableArray alloc] init];
    markerArray = [[NSMutableArray alloc] init];
    restaurantArray = [[NSMutableArray alloc] init];
    imgURLArray = [[NSMutableArray alloc] init];
    imgArray = [[NSMutableArray alloc] init];
    
    UITapGestureRecognizer *tapCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectItem:)];
    [_searchBar.autoCompleteTableView addGestureRecognizer:tapCell];
    
    tabBarHeight = self.tabBarController.tabBar.frame.size.height;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLocationNotification:) name:@"locationNotification" object:nil];
    
    if([[[locationSingleton Instance] status] isEqual: @""]) {
        [locationSingleton Instance];
    } else if([[[locationSingleton Instance] status] isEqual: @"ready"]) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"ready" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
    } else if([[[locationSingleton Instance] status] isEqual: @"deny"]) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"deny" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
    }
    
    [[appearanceSingleton Instance] setTabBarAppearance:3];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_searchBar setCustomWithOffset:CGPointMake(-10, -17) andWidth:_searchBarBG.frame.size.width];
    } else {
        [_searchBar setCustomWithOffset:CGPointMake(-10, -11) andWidth:_searchBarBG.frame.size.width];
    }
    
    self.tabBarController.tabBar.hidden = NO;
    self.tabBarController.tabBar.layer.frame = CGRectMake(self.tabBarController.tabBar.layer.frame.origin.x, self.tabBarController.tabBar.layer.frame.origin.y, self.tabBarController.tabBar.layer.frame.size.width, tabBarHeight);
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    self.navigationItem.title = [[languageSingleton Instance] getTextForKey:@"mapSearch"];
    _searchBar.placeholder = [[languageSingleton Instance] getTextForKey:@"searchForLocation"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"restaurantMenu"]) {
        restaurantTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurantFood = [vc.viewControllers objectAtIndex:0];
        vc.restaurantDrink = [vc.viewControllers objectAtIndex:1];
        vc.restaurantOther = [vc.viewControllers objectAtIndex:2];
        
        vc.restaurantFood.RestaurantModel = vc.restaurantDrink.RestaurantModel = vc.restaurantOther.RestaurantModel = selectedRestaurant;
        
        vc.navigationItem.rightBarButtonItem = self.navigationItem.rightBarButtonItem;
    }
}

- (void)getLocationNotification:(NSNotification *)notification {
    NSDictionary *dictionary = [notification userInfo];
    NSString *statusString = [dictionary valueForKey:@"status"];
    
    if([statusString isEqual: @"ready"]) {
        _mapView.myLocationEnabled = YES;
    } else if([statusString isEqual: @"deny"]) {
       _mapView.myLocationEnabled = NO;
    } else if([statusString isEqual: @"update"]) {
        currentLocation = [[locationSingleton Instance] currentLocation];
    } else if([statusString isEqual: @"error"]) {
        //Do something
    }
}

- (void)dismissKeyboard {
    [_searchBar resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self goToLocation];
    [self dismissKeyboard];
    
    return  YES;
}

//Go to first match location from Google Map Autocomplete
- (void)goToLocation {
    if(_searchBar.text.length > 0) {
        if(autocompleteArray.count > 0) {
            GMSAutocompletePrediction *result = nil;
            for(GMSAutocompletePrediction *index in autocompleteArray) {
                if([[index.attributedFullText.string lowercaseString] hasPrefix:[_searchBar.text lowercaseString]]) {
                    result = index;
                    break;
                }
            }
            
            if(result != nil) {
                _searchBar.text = result.attributedFullText.string;
                
                GMSPlacesClient *placesClient = [[GMSPlacesClient alloc] init];
                [placesClient lookUpPlaceID:result.placeID callback:^(GMSPlace *place, NSError *error) {
                    if (error != nil) {
                        NSLog(@"Place Details error %@", [error localizedDescription]);
                        return;
                    }
                    
                    if (place != nil) {
                        [_mapView animateToLocation:place.coordinate];
                    } else {
                        NSLog(@"No place details for %@", result.placeID);
                    }
                }];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }
}

- (NSArray *)autoCompleteTextField:(MLPAutoCompleteTextField *)textField possibleCompletionsForString:(NSString *)string {
    if(isAutocompleteReady) {
        NSMutableArray *stringResult = [[NSMutableArray alloc] init];
        for(GMSAutocompletePrediction *result in autocompleteArray) {
            [stringResult addObject:result.attributedFullText.string];
        }
        return [stringResult copy];
    }
    
    return @[];
}

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {
    [self dismissKeyboard];
}

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    [self dismissKeyboard];
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:position.zoom] forKey:@"cur_zoom"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:position.target.latitude] forKey:@"cur_lat"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:position.target.longitude] forKey:@"cur_lng"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    GMSVisibleRegion visibleRegion = _mapView.projection.visibleRegion;
    
    [[APISingleton Instance] search_by_location:[[NSNumber numberWithDouble:visibleRegion.nearLeft.latitude] stringValue] :[[NSNumber numberWithDouble:visibleRegion.nearLeft.longitude] stringValue] :[[NSNumber numberWithDouble:visibleRegion.farRight.latitude] stringValue] :[[NSNumber numberWithDouble:visibleRegion.farRight.longitude] stringValue] :@"en" :NO :^(id json) {

        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                for(NSDictionary *index in json[@"results"]) {
                    restaurantModel *thisModel = [[restaurantModel alloc] init];
                    [thisModel initWithJSON:index];
                    
                    if([markerArray indexOfObject:thisModel.obj_id] == NSNotFound) {
                        CLLocationCoordinate2D position = CLLocationCoordinate2DMake([thisModel.latitude doubleValue], [thisModel.longitude doubleValue]);
                        GMSMarker *marker = [GMSMarker markerWithPosition:position];
                        marker.appearAnimation = kGMSMarkerAnimationPop;
                        marker.icon = [UIImage imageNamed:@"UI_Map_Marker"];
                        marker.title = thisModel.obj_id;
                        marker.map = _mapView;
                        
                        [markerArray addObject:thisModel.obj_id];
                        [restaurantArray addObject:thisModel];
                    }
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }];
    
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    _mapView.selectedMarker = marker;
    return YES;
}

- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    customInfoWindow *infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"infoWindow" owner:self options:nil] objectAtIndex:0];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    infoWindow.frame = CGRectMake(infoWindow.frame.origin.x, infoWindow.frame.origin.y, screenRect.size.width * 0.65, screenRect.size.width * 0.65 * 0.63);
    

    for(restaurantModel *thisModel in restaurantArray) {
        if([thisModel.obj_id isEqualToString:marker.title]) {
            infoWindow.infoName.text = thisModel.name;
            infoWindow.infoDetail.text = thisModel.type;
            
            NSURL *img_path = [[APISingleton Instance] get_img_url:thisModel.img_path :thisModel.img[0]];
            NSInteger indexPath = [imgURLArray indexOfObject:[img_path absoluteString]];
            
            if(indexPath != NSNotFound) {
                [infoWindow.infoImg setImage:imgArray[indexPath]];
            } else {
                dispatch_queue_t imageQueue = dispatch_queue_create("Image Queue", nil);
                
                dispatch_async(imageQueue, ^{
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:img_path];
                    UIImage *image = [UIImage imageWithData:imageData];
                    
                    if (!image) return;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [imgURLArray addObject:[img_path absoluteString]];
                        [imgArray addObject:image];
                        
                        _mapView.selectedMarker = nil;
                        _mapView.selectedMarker = marker;
                    });
                    
                });
            }

            [infoWindow makeAutolayout];
            break;
        }
    }
    
    return infoWindow;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSInteger indexPath = [markerArray indexOfObject:marker.title];
    
    if(indexPath != NSNotFound) {
        selectedRestaurant = [restaurantArray objectAtIndex:indexPath];
        //self.hidesBottomBarWhenPushed = YES;
        self.tabBarController.tabBar.hidden = YES;
        self.tabBarController.tabBar.layer.frame = CGRectMake(self.tabBarController.tabBar.layer.frame.origin.x, self.tabBarController.tabBar.layer.frame.origin.y, self.tabBarController.tabBar.layer.frame.size.width, 0);
        [self performSegueWithIdentifier:@"restaurantMenu" sender:self];
    }
}

- (IBAction)callSettingMenu:(id)sender {
    settingMenuController* _settingMenuController = [[settingMenuController alloc] initWithViewController:self];
    [_settingMenuController callSettingMenu];
}

- (IBAction)searchEditing:(id)sender {
    isAutocompleteReady = NO;
    
    if([_searchBar.text length] >= 1) {
        GMSPlacesClient *placesClient = [[GMSPlacesClient alloc] init];
        GMSVisibleRegion visibleRegion = _mapView.projection.visibleRegion;
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:visibleRegion.farLeft coordinate:visibleRegion.nearRight];
        GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
        filter.type = kGMSPlacesAutocompleteTypeFilterRegion;
        
        [placesClient autocompleteQuery:_searchBar.text bounds:bounds filter:filter callback:^(NSArray *results, NSError *error) {
            if (error != nil) {
                NSLog(@"Autocomplete error %@", [error localizedDescription]);
                return;
            }
            
            //NSLog(@"%d", results.count);
            isAutocompleteReady = YES;
            [autocompleteArray removeAllObjects];
            for(GMSAutocompletePrediction *result in results) {
                //NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
                [autocompleteArray addObject:result];
            }
        }];
    }
}

- (IBAction)searchBegin:(id)sender {
    _searchBarAL.constant = -(_searchBar.layer.frame.size.height * 5);
}

- (IBAction)searchEnd:(id)sender {
    _searchBarAL.constant = 0;
}

- (IBAction)touchSearch:(id)sender {
    [self goToLocation];
}

- (IBAction)touchLocation:(id)sender {
    if(_mapView.myLocationEnabled) {
        [_mapView animateToLocation:currentLocation.coordinate];
        [_mapView animateToBearing:0];
    }
}

-(void)selectItem:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_searchBar.autoCompleteTableView];
        NSIndexPath *tapLocationIndexPath = [_searchBar.autoCompleteTableView indexPathForRowAtPoint:tapLocation];
        if(tapLocationIndexPath != nil) {
            UITableViewCell* tapCell = [_searchBar.autoCompleteTableView cellForRowAtIndexPath:tapLocationIndexPath];
            
            _searchBar.text = tapCell.textLabel.text;
            [self textFieldShouldReturn:nil];
        }
    }
}

@end
