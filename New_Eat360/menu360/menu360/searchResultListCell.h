//
//  searchResultListCell.h
//  menu360
//
//  Created by PandaMaru on 6/16/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "restaurantModel.h"

@interface searchResultListCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *cellBg;
@property (strong, nonatomic) IBOutlet UIImageView *cellImg;
@property (strong, nonatomic) IBOutlet UILabel *cellName;
@property (strong, nonatomic) IBOutlet UITextView *cellDetail;
@property (strong, nonatomic) IBOutlet UIButton *cellMap;
@property (strong, nonatomic) IBOutlet UIView *cellShareView;

@property (strong, nonatomic) UIViewController *parentVC;
@property (strong, nonatomic) restaurantModel *RestaurantModel;

- (IBAction)touchMap:(id)sender;

@end
