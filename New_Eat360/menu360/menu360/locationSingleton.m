//
//  locationSingleton.m
//  menu360
//
//  Created by Apps360[admin] on 19/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "locationSingleton.h"

@implementation locationSingleton

+ (id)Instance {
    static locationSingleton *locationSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        locationSingletonInstance = [[self alloc] init];
    });
    
    return locationSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        _status = @"";
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [_locationManager requestWhenInUseAuthorization];
        }
    }
    
    return  self;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if(status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [_locationManager startUpdatingLocation];
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"ready" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
        _status = @"ready";
    } else if(status == kCLAuthorizationStatusDenied) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"deny" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
        _status = @"deny";
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    //NSLog(@"%@", [locations lastObject]);
    _currentLocation = [locations lastObject];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"update" forKey:@"status"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if(error.domain == kCLErrorDomain) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithObject:@"error" forKey:@"status"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"locationNotification" object:nil userInfo:dictionary];
    }
}

@end
