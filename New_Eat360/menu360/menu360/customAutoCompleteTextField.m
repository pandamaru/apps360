//
//  customAutoCompleteTextField.m
//  menu360
//
//  Created by Apps360[admin] on 11/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "customAutoCompleteTextField.h"

@implementation customAutoCompleteTextField
#pragma mark - Custom

- (void)customInit {
    _isSearchCategory = NO;
}

- (void)setCustomWithOffset:(CGPoint) offset andWidth:(CGFloat) width {
    _autoCompleteTableOffset = offset;
    _autoCompleteTableWidth = width;
}

#pragma mark - TableView Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    id autoCompleteObject = self.autoCompleteSuggestions[indexPath.row];
    NSString *suggestedString;
    NSString *searchCategoryString;
    if([autoCompleteObject isKindOfClass:[NSString class]]){
        NSArray *stringArray = [(NSString *)autoCompleteObject componentsSeparatedByString:@"#"];
        suggestedString = (NSString *)stringArray[0];
        if([stringArray count] > 1) searchCategoryString = (NSString *)stringArray[1];
    } else if ([autoCompleteObject conformsToProtocol:@protocol(MLPAutoCompletionObject)]){
        suggestedString = [(id <MLPAutoCompletionObject>)autoCompleteObject autocompleteString];
        searchCategoryString = [(id <MLPAutoCompletionObject>)autoCompleteObject autocompleteString];
    } else {
        NSAssert(0, @"Autocomplete suggestions must either be NSString or objects conforming to the MLPAutoCompletionObject protocol.");
    }
    
    if(_isSearchCategory) {
        customAutoCompleteCell *cell = nil;
        NSString *cellIdentifier = @"CustomCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [self autoCompleteTableViewCustomCellWithReuseIdentifier:cellIdentifier];
        }
        
        if([searchCategoryString isEqual: @" "]) {
            [self configureCustomCell:cell atIndexPath:indexPath withAutoCompleteString:[NSArray arrayWithObjects:suggestedString, searchCategoryString, nil]];
        } else {
            [self configureCustomCell:cell atIndexPath:indexPath withAutoCompleteString:[NSArray arrayWithObjects:suggestedString, [[languageSingleton Instance] getTextForKey:searchCategoryString], nil]];
        }
        
        
        return cell;
    } else {
        UITableViewCell *cell = nil;
        NSString *cellIdentifier = [MLPAutoCompleteTextField kDefaultAutoCompleteCellIdentifier];
        
        if(!self.reuseIdentifier){
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [self autoCompleteTableViewCellWithReuseIdentifier:cellIdentifier];
            }
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:self.reuseIdentifier];
        }
        NSAssert(cell, @"Unable to create cell for autocomplete table");
        
        [self configureCell:cell atIndexPath:indexPath withAutoCompleteString:suggestedString];
        
        return cell;
    }
}

- (customAutoCompleteCell *)autoCompleteTableViewCustomCellWithReuseIdentifier:(NSString *)identifier {
    
    customAutoCompleteCell *cell;
    
    NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"customAutoCompleteCell" owner:nil options:nil];
    
    for(id currentObject in topLevelObjects)
    {
        if([currentObject isKindOfClass:[customAutoCompleteCell class]])
        {
            cell = (customAutoCompleteCell *)currentObject;
            break;
        }
    }

    return cell;
}

- (void)configureCustomCell:(customAutoCompleteCell *)cell atIndexPath:(NSIndexPath *)indexPath withAutoCompleteString:(NSArray *)string {

    NSAttributedString *boldedString = nil;
    if(self.applyBoldEffectToAutoCompleteSuggestions){
        BOOL attributedTextSupport = [cell.cellText respondsToSelector:@selector(setAttributedText:)];
        NSAssert(attributedTextSupport, @"Attributed strings on UILabels are  not supported before iOS 6.0");
        NSRange boldedRange = [[string[0] lowercaseString]
                               rangeOfString:[self.text lowercaseString]];
        boldedString = [self boldedString:string[0] withRange:boldedRange];
    }
    
    id autoCompleteObject = self.autoCompleteSuggestions[indexPath.row];
    if(![autoCompleteObject conformsToProtocol:@protocol(MLPAutoCompletionObject)]){
        autoCompleteObject = nil;
    }
    
    if([self.autoCompleteDelegate respondsToSelector:@selector(autoCompleteTextField:shouldConfigureCell:withAutoCompleteString:withAttributedString:forAutoCompleteObject:forRowAtIndexPath:)])
    {
        if(![self.autoCompleteDelegate autoCompleteTextField:self shouldConfigureCell:cell withAutoCompleteString:string[0] withAttributedString:boldedString forAutoCompleteObject:autoCompleteObject forRowAtIndexPath:indexPath])
        {
            return;
        }
    }
    
    [cell.cellText setTextColor:self.textColor];
    [cell.cellCategory setTextColor:self.textColor];
    
    if(boldedString){
        if ([cell.cellText respondsToSelector:@selector(setAttributedText:)]) {
            [cell.cellText setAttributedText:boldedString];
        } else{
            if([[string[0] substringToIndex:1] isEqual: @"!"]) {
                [cell.cellText setText:[NSString stringWithFormat:@"%@ \"%@\"", [[languageSingleton Instance]getTextForKey:@"contain"], [string[0] substringFromIndex:1]]];
            } else {
                [cell.cellText setText:string[0]];
            }
            
            [cell.cellText setFont:[UIFont fontWithName:self.font.fontName size:self.autoCompleteFontSize]];
        }
        
    } else {
        if([[string[0] substringToIndex:1] isEqual: @"!"]) {
            [cell.cellText setText:[NSString stringWithFormat:@"%@ \"%@\"", [[languageSingleton Instance]getTextForKey:@"contain"], [string[0] substringFromIndex:1]]];
        } else {
            [cell.cellText setText:string[0]];
        }
        
        [cell.cellText setFont:[UIFont fontWithName:self.font.fontName size:self.autoCompleteFontSize]];
    }
    
    [cell.cellCategory setText:string[1]];
    [cell.cellCategory setFont:[UIFont fontWithName:self.autoCompleteBoldFontName size:self.autoCompleteFontSize]];
    
    if(self.autoCompleteTableCellTextColor) {
        [cell.cellText setTextColor:self.autoCompleteTableCellTextColor];
    }
    
    if(self.searchCategoryTextColor) {
        [cell.cellCategory setTextColor:self.searchCategoryTextColor];
    }
}

/*- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) {
        [cell setBackgroundColor:self.autoCompleteTableCellBackgroundColor];
    } else if(indexPath.section == 1) {
        [cell setBackgroundColor:self.autoCompleteTableCellBackgroundColor];
    } else {
        //Nothing
    }
}*/

#pragma mark - Setters

- (void)resetKeyboardAutoCompleteTableFrameForNumberOfRows:(NSInteger)numberOfRows
{
    [self.autoCompleteTableView.layer setCornerRadius:0];
    
    CGRect newAutoCompleteTableViewFrame = [[self class]
                                            autoCompleteTableViewFrameForTextField:self
                                            forNumberOfRows:numberOfRows
                                            withOffset:_autoCompleteTableOffset
                                            andWidth:_autoCompleteTableWidth];
    [self.autoCompleteTableView setFrame:newAutoCompleteTableViewFrame];
    
    [self.autoCompleteTableView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [self.autoCompleteTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

- (void)resetDropDownAutoCompleteTableFrameForNumberOfRows:(NSInteger)numberOfRows
{
    [self.autoCompleteTableView.layer setCornerRadius:self.autoCompleteTableCornerRadius];
    
    CGRect newAutoCompleteTableViewFrame = [[self class]
                                            autoCompleteTableViewFrameForTextField:self
                                            forNumberOfRows:numberOfRows
                                            withOffset:_autoCompleteTableOffset
                                            andWidth:_autoCompleteTableWidth];
    
    [self.autoCompleteTableView setFrame:newAutoCompleteTableViewFrame];
    [self.autoCompleteTableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

#pragma mark - Factory Methods

+ (UITableView *)newAutoCompleteTableViewForTextField:(MLPAutoCompleteTextField *)textField
{
    CGRect dropDownTableFrame = [[self class] autoCompleteTableViewFrameForTextField:textField];
    
    UITableView *newTableView = [[UITableView alloc] initWithFrame:dropDownTableFrame
                                                             style:UITableViewStylePlain];
    [newTableView setDelegate:textField];
    [newTableView setDataSource:textField];
    [newTableView setScrollEnabled:YES];
    [newTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    return newTableView;
}

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField
                                 forNumberOfRows:(NSInteger)numberOfRows
                                      withOffset:(CGPoint)offset
                                       andWidth:(CGFloat)width;
{
    CGRect newTableViewFrame = [[self class] autoCompleteTableViewFrameForTextField:textField];
    
    CGFloat height = [[self class] autoCompleteTableHeightForTextField:textField
                                                      withNumberOfRows:numberOfRows];
    newTableViewFrame.origin.x += offset.x;
    newTableViewFrame.origin.y += offset.y;
    newTableViewFrame.size = CGSizeMake(width, height);
    
    if(!textField.autoCompleteTableAppearsAsKeyboardAccessory){
        newTableViewFrame.size.height += textField.autoCompleteTableView.contentInset.top;
    }
    
    return newTableViewFrame;
}

+ (CGFloat)autoCompleteTableHeightForTextField:(MLPAutoCompleteTextField *)textField
                              withNumberOfRows:(NSInteger)numberOfRows
{
    CGFloat maximumHeightMultiplier = (textField.maximumNumberOfAutoCompleteRows - 0.5);
    CGFloat heightMultiplier;
    if(numberOfRows >= textField.maximumNumberOfAutoCompleteRows){
        heightMultiplier = maximumHeightMultiplier;
    } else {
        heightMultiplier = numberOfRows;
    }
    
    CGFloat height = textField.autoCompleteRowHeight * heightMultiplier;
    return height;
}

+ (CGRect)autoCompleteTableViewFrameForTextField:(MLPAutoCompleteTextField *)textField
{
    CGRect frame = textField.frame;
    frame.origin.y += textField.frame.size.height;
    frame.origin.x += textField.autoCompleteTableOriginOffset.width;
    frame.origin.y += textField.autoCompleteTableOriginOffset.height;
    frame = CGRectInset(frame, 1, 0);
    
    return frame;
}
@end
