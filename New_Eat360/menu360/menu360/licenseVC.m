//
//  licenseVC.m
//  menu360
//
//  Created by Apps360[admin] on 18/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "licenseVC.h"

@interface licenseVC ()

@end

@implementation licenseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
