//
//  appearanceSingleton.m
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "appearanceSingleton.h"

@implementation appearanceSingleton

+ (id)Instance {
    static appearanceSingleton *appearanceSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        appearanceSingletonInstance = [[self alloc] init];
    });
    
    return appearanceSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        tabBarSelectedColor = [UIColor colorWithRed:1/255.0 green:137/255.0 blue:202/255.0 alpha:1];
        
        [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:20]}];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    return  self;
}

- (void)setTabBarAppearance:(int)tabAmount {
    //+4 for last tab on some small screen not display properly
    [[UITabBar appearance] setSelectionIndicatorImage:[self imageFromColor:tabBarSelectedColor forSize:CGSizeMake(4 + [[UIScreen mainScreen] bounds].size.width / tabAmount, 49) withCornerRadius:0]];
}

- (UIColor *)getTabBarColor {
    return tabBarSelectedColor;
}

- (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius {
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}

-(UIImage *)combineTwoImage:(UIImage *)bg :(UIImage *)fg {
    CGSize newSize = CGSizeMake(bg.size.width, bg.size.height);
    UIGraphicsBeginImageContext( newSize );
    
    // Use existing opacity as is
    [bg drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Apply supplied opacity if applicable
    [fg drawInRect:CGRectMake((newSize.width / 2) - (fg.size.width / 2),(newSize.height / 2) - (fg.size.height / 2),fg.size.width,fg.size.height) blendMode:kCGBlendModeNormal alpha:1.0];
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)imageTintedWithColor:(UIImage *)image :(UIColor *)color {
    UIGraphicsBeginImageContext(image.size);
    CGRect rect = CGRectZero;
    rect.size = [image size];
    
    // tint the image
    [image drawInRect:rect];
    [color set];
    UIRectFillUsingBlendMode(rect, kCGBlendModeNormal);
    
    // restore alpha channel
    [image drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0f];
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
