//
//  customInfoWindow.h
//  menu360
//
//  Created by PandaMaru on 7/21/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APISingleton.h"
#import <AFNetworking+ImageActivityIndicator.h>
#import "restaurantModel.h"

@interface customInfoWindow : UIView

@property (strong, nonatomic) IBOutlet UIImageView *bgImg;
@property (strong, nonatomic) IBOutlet UIView *infoView;
@property (strong, nonatomic) IBOutlet UILabel *infoName;
@property (strong, nonatomic) IBOutlet UIImageView *infoImg;
@property (strong, nonatomic) IBOutlet UILabel *infoDetail;
@property (strong, nonatomic) IBOutlet UIButton *infoBtn;

- (void)makeAutolayout;

@end
