//
//  APISingleton.h
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import <SVProgressHUD.h>
#import "languageSingleton.h"

@interface APISingleton : NSObject {
    NSString *baseURL;
}

@property (strong, nonatomic) NSArray *serverArray;

+ (id)Instance;
- (void)setCurrentServer:(NSString *)server;
- (NSString *)getCurrentServer;
- (void)POST:(NSString*)API :(NSDictionary*)parameters :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_by_keyword:(NSString*)SearchText :(NSString*)SearchLang :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant:(NSString*)SearchText :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_menus:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_food:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_drink:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_menu_category:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_menu_by_category:(NSString*)RestaurantID :(NSString*)CategoryID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_by_location:(NSString*)MinLatitude :(NSString*)MinLongitude :(NSString*)MaxLatitude :(NSString*)MaxLongitude :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)restaurant:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_by_qr:(NSString*)URL :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)food_ingredients:(NSString*)FoodID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
//- (void)currency_converter:(NSString*)Amount :(NSString*)FromCurrency :(NSString*)ToCurrency :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)autocomplete:(NSString *)SearchText :(NSString *)Latitude :(NSString *)Longitude :(NSString *)Radius :(NSString *)SearchLang :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search2:(NSString *)Latitude :(NSString *)Longitude :(NSString *)Radius :(NSString *)SearchText :(NSString *)SearchLang :(NSString *)SearchType :(NSString *)TopCategoryID :(NSString *)TopType :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_menus_by_meal_id:(NSString *)RestaurantID :(NSString *)MealID :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_food_by_meal_id:(NSString *)RestaurantID :(NSString *)MealID :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)get_top_category:(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)multiple_restaurant_details:(NSString *)RestaurantIDs :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_menus_by_cuisine_id:(NSString*)RestaurantID :(NSString*)CuisineID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)search_restaurant_menu_by_food_keyword:(NSString*)RestaurantID :(NSString*)Keyword :(NSString*)KeywordType :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (void)food:(NSString*)FoodID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack;
- (NSURL*)get_img_url:(NSString*)path :(NSString*)img;

@end
