//
//  restaurantFoodVC.m
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "restaurantFoodVC.h"

@interface restaurantFoodVC ()

@end

@implementation restaurantFoodVC {
    NSMutableArray *foodArray;
    UITapGestureRecognizer *tapCell;
    UITapGestureRecognizer *tapSubMenu;
    UILongPressGestureRecognizer *lpSubMenu;
    NSInteger selectedIndex;
    
    CGFloat subMenuPoint;
    
    NSMutableArray *categoryArray;
    
    CGFloat headerCellHeight;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Insert original image to TabBar
    UITabBarItem *drinkItem = [self.tabBarController.tabBar.items objectAtIndex:1];
    drinkItem.image = [[UIImage imageNamed:@"BTN_Tab_Drink"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UITabBarItem *otherItem = [self.tabBarController.tabBar.items objectAtIndex:2];
    otherItem.image = [[UIImage imageNamed:@"BTN_Tab_Other"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
    }
    
    tapCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCell:)];
    [_collectionView addGestureRecognizer:tapCell];
    
    tapSubMenu = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCategory:)];
    [_collectionSubMenuView addGestureRecognizer:tapSubMenu];
    lpSubMenu = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(pressCategory:)];
    [_collectionSubMenuView addGestureRecognizer:lpSubMenu];
    
    foodArray = [[NSMutableArray alloc] init];
    categoryArray = [[NSMutableArray alloc] init];
    
    [[dataSingleton Instance] setIsSubMenuOpen:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"loadCurrency" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"removeCondition" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:3];
    
    if(!_SearchConditionModel) {
        headerCellHeight = self.view.frame.size.width / 1.2;
    } else {
        headerCellHeight = self.view.frame.size.width / 0.97;
    }
    
    [_collectionView reloadData];
    
    subMenuPoint = _subMenuView.center.y;
    
    _subMenuView.hidden = YES;
    self.tabBarController.delegate = self;
    
    [[dataSingleton Instance] setIsSubMenuOpen:NO];
    
    UITabBarItem *foodTabBarItem = [self.tabBarController.tabBar.items objectAtIndex:0];
    UIImage *foodIcon = [[UIImage imageNamed:@"BTN_Tab_Food"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [foodTabBarItem setImage:foodIcon];
    
    UITabBarItem *otherTabBarItem = [self.tabBarController.tabBar.items objectAtIndex:2];
    UIImage *otherIcon = [[UIImage imageNamed:@"BTN_Tab_Other"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [otherTabBarItem setImage:otherIcon];
    
    if(_SearchConditionModel) {
        if(_SearchConditionModel.isTopCategory) {
            if([_SearchConditionModel.searchType isEqual: @"food_category"]) {
                [self loadFoodCategory];
            } else if([_SearchConditionModel.searchType isEqual: @"food_meal"]) {
                [self loadMeal];
            } else if([_SearchConditionModel.searchType isEqual: @"food_cuisine"]) {
                [self loadFoodCuisine];
            }
        } else {
            //NSLog(@"NOT TOP");
            if([_SearchConditionModel.searchText isEqual: @""]) {
                [self loadNormal];
            } else {
                NSArray *splited = [_SearchConditionModel.searchType componentsSeparatedByString:@"_"];
                if([(NSString *)(splited[0]) isEqual: @"food"]) {
                    self.tabBarController.tabBar.hidden = YES;
                    
                    [foodArray removeAllObjects];
                    [foodArray addObject:@"Header"];
                    
                    if([splited[1] isEqual: @"name"]) {
                        [self loadFoodName];
                    } else if([splited[1] isEqual: @"category"]) {
                        [self loadFoodCategory];
                    } else if([splited[1] isEqual: @"cuisine"]) {
                        [self loadFoodCuisine];
                    } else if([splited[1] isEqual: @"ingredient"]) {
                        [self loadFoodIngre];
                    } else if([splited[1] isEqual: @"meal"]) {
                        [self loadMeal];
                    }
                } else {
                    [self loadNormal];
                }
            }
        }
    } else {
        [self loadNormal];
    }
}

- (void)loadNormal {
    self.tabBarController.tabBar.hidden = NO;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    if([[[dataSingleton Instance] selectedMeal] isEqual: @""] || [[[dataSingleton Instance] selectedMeal] isEqual: @"6"]) {
        [[APISingleton Instance] search_restaurant_food:_RestaurantModel.obj_id :@"en" :YES :^(id json) {
            if([json[@"status_code"] isEqual: @"200"]) {
                if([json[@"results"] count] > 0) {
                    resultJSON = json[@"results"];
                    
                    for(NSDictionary *index in resultJSON) {
                        foodModel *thisModel = [[foodModel alloc] init];
                        [thisModel initWithJSON:index];
                        [foodArray addObject:thisModel];
                    }
                } else {
                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
            }
            
            [_collectionView reloadData];
        }];
    } else {
        [[APISingleton Instance] search_food_by_meal_id:_RestaurantModel.obj_id :[[dataSingleton Instance] selectedMeal] :@"en" :YES :^(id json) {
            if([json[@"status_code"] isEqual: @"200"]) {
                if([json[@"results"] count] > 0) {
                    resultJSON = json[@"results"];
                    
                    for(NSDictionary *index in resultJSON) {
                        foodModel *thisModel = [[foodModel alloc] init];
                        [thisModel initWithJSON:index];
                        [foodArray addObject:thisModel];
                    }
                } else {
                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
            }
            
            [_collectionView reloadData];
        }];
    }
    
    [categoryArray removeAllObjects];
    
    [[APISingleton Instance] search_restaurant_menu_category:_RestaurantModel.obj_id :@"en" :NO :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    [categoryArray addObject:index];
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }];
}

- (void)loadFoodName {
    self.tabBarController.tabBar.hidden = YES;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    NSString* keyword = _SearchConditionModel.searchText;
    if([[keyword substringToIndex:1] isEqual: @"!"]) {
        keyword = [keyword substringFromIndex:1];
    }
    
    [[APISingleton Instance] search_restaurant_menu_by_food_keyword:_RestaurantModel.obj_id :keyword :@"food_name" :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    foodModel *thisModel = [[foodModel alloc] init];
                    [thisModel initWithJSON:index];
                    [foodArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (void)loadFoodCategory {
    self.tabBarController.tabBar.hidden = YES;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    [[APISingleton Instance] search_restaurant_menu_by_category:_RestaurantModel.obj_id :_SearchConditionModel.searchPossibleType :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    foodModel *thisModel = [[foodModel alloc] init];
                    [thisModel initWithJSON:index];
                    [foodArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (void)loadFoodCuisine {
    self.tabBarController.tabBar.hidden = YES;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    [[APISingleton Instance] search_menus_by_cuisine_id:_RestaurantModel.obj_id :_SearchConditionModel.searchPossibleType :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    foodModel *thisModel = [[foodModel alloc] init];
                    [thisModel initWithJSON:index];
                    [foodArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (void)loadFoodIngre {
    self.tabBarController.tabBar.hidden = YES;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    [[APISingleton Instance] search_restaurant_menu_by_food_keyword:_RestaurantModel.obj_id :_SearchConditionModel.searchText :@"food_ingredient" :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    foodModel *thisModel = [[foodModel alloc] init];
                    [thisModel initWithJSON:index];
                    [foodArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (void)loadMeal {
    self.tabBarController.tabBar.hidden = YES;
    
    [foodArray removeAllObjects];
    [foodArray addObject:@"Header"];
    
    [[APISingleton Instance] search_menus_by_meal_id:_RestaurantModel.obj_id :_SearchConditionModel.searchPossibleType :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                resultJSON = json[@"results"];
                
                for(NSDictionary *index in resultJSON) {
                    foodModel *thisModel = [[foodModel alloc] init];
                    [thisModel initWithJSON:index];
                    [foodArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@", _RestaurantModel.name];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, subMenuPoint)];
    
    if([[segue identifier] isEqualToString:@"foodDetail"]) {
        foodDetailVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurant_id = _RestaurantModel.obj_id;
        vc.FoodModel = (foodModel*)[foodArray objectAtIndex:selectedIndex];
        
        UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
        
        vc.rightBarButton = navCon.navigationItem.rightBarButtonItem;
        
        /*if([self.navigationController.viewControllers count] > 2) {
            navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:2];
        } else if([self.navigationController.viewControllers count] > 1) {
            navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        }
        
        vc.navigationItem.rightBarButtonItem = navCon.navigationItem.rightBarButtonItem;*/
    } else if([[segue identifier] isEqualToString:@"restaurantMap"]) {
        restaurantMapVC *vc = [segue destinationViewController];
        
        vc.RestaurantModel = _RestaurantModel;
        
        UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
        
        vc.rightBarButton = navCon.navigationItem.rightBarButtonItem;
    }
}

- (void)receiveNotification:(NSNotification *) notification {
    //NSLog(@"Load from Food");
    if ([[notification name] isEqualToString:@"loadCurrency"]) {
        [_collectionView reloadData];
    } else if ([[notification name] isEqualToString:@"removeCondition"]) {
        _SearchConditionModel.searchText = @"";
        _SearchConditionModel.searchType = @"";
        
        [self loadNormal];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(collectionView == _collectionView) {
        if(section == 0) {
            return CGSizeMake(0, 15);
        }
    }
    
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == _collectionView) {
        if(indexPath.row == 0) {
            return CGSizeMake(self.view.frame.size.width, headerCellHeight);
        } else {
            return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width / 2.8);
        }
    } else if(collectionView == _collectionSubMenuView) {
        return CGSizeMake(self.view.frame.size.width / 9.6, self.view.frame.size.width / 9.6);
    }
    
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if(collectionView == _collectionView) {
        return UIEdgeInsetsMake(5, 0, 5, 0); //t,l,b,r
    } else if(collectionView == _collectionSubMenuView) {
        return UIEdgeInsetsMake(20, 40, 20, 40);
    }
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(collectionView == _collectionView) {
        return foodArray.count;
    } else if(collectionView == _collectionSubMenuView) {
        if([[dataSingleton Instance] lastOpenedSubMenu] == 0) {
            return 6;
        } else if([[dataSingleton Instance] lastOpenedSubMenu] == 2) {
            return categoryArray.count;
        }
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    
    if(collectionView == _collectionView) {
        if(indexPath.row == 0) {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantHeaderCell" forIndexPath:indexPath];
            restaurantHeaderCell *thisCell = (restaurantHeaderCell*)cell;
            
            thisCell.cellView.layer.cornerRadius = 6;
            thisCell.cellView.layer.masksToBounds = YES;
            UIImage *nineSliceImg = [UIImage imageNamed:@"UI_Box@x2.png"];
            UIImage *resizableImg = [nineSliceImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
            [thisCell.cellBg setImage:resizableImg];
            
            [thisCell.cellMap setBackgroundImage:[[UIImage imageNamed:@"BTN_Tab_Map@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
            [thisCell.cellMap setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
            
            if(!_SearchConditionModel) {
                [thisCell setForHidden:YES];
                thisCell.searchTypeView.hidden = YES;
            } else {
                [thisCell setForHidden:NO];
                thisCell.searchTypeView.hidden = NO;

                thisCell.searchTypeView.layer.cornerRadius = 3;
                thisCell.searchTypeView.layer.masksToBounds = YES;
                
                [thisCell.searchTypeCloseBtn setBackgroundImage:[[appearanceSingleton Instance] imageTintedWithColor:[thisCell.searchTypeCloseBtn backgroundImageForState:UIControlStateNormal] :[UIColor whiteColor]] forState:UIControlStateNormal];
                
                if([_SearchConditionModel.searchText isEqual: @""]) {
                    thisCell.searchTypeLabel.text = [[dataSingleton Instance] getUnconditionFormatWithPlace:_SearchConditionModel.searchPlace  withRadius:_SearchConditionModel.searchRadius];
                } else {
                    thisCell.searchTypeLabel.text = [[dataSingleton Instance] getConditionFormat:_SearchConditionModel.searchText withType:[[languageSingleton Instance] getTextForKey:_SearchConditionModel.searchType] withPlace:_SearchConditionModel.searchPlace withRadius:_SearchConditionModel.searchRadius];
                }
            }
            
            //Extra setting for iPad
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                [thisCell.cellAddress setFont:[UIFont fontWithName:@"OpenSans" size:27.5]];
                
                [thisCell.searchTypeLabel setFont:[UIFont fontWithName:@"OpenSans" size:35]];
            }
            
            [thisCell.cellImage setImageWithURL:[[APISingleton Instance] get_img_url:_RestaurantModel.img_path :_RestaurantModel.img[0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            thisCell.cellAddress.text = [NSString stringWithFormat:@"%@ / %@ %@ %@ / %@\n%@", _RestaurantModel.address, _RestaurantModel.postCode, _RestaurantModel.state, _RestaurantModel.country, _RestaurantModel.type, _RestaurantModel.openinghour];
            
            thisCell.parentVC = self;
            thisCell.RestaurantModel = _RestaurantModel;
        } else {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantContentCell" forIndexPath:indexPath];
            restaurantContentCell *thisCell = (restaurantContentCell*)cell;
            
            thisCell.cellView.layer.cornerRadius = 6;
            thisCell.cellView.layer.masksToBounds = YES;
            UIImage *nineSliceImg = [UIImage imageNamed:@"UI_Box@x2.png"];
            UIImage *resizableImg = [nineSliceImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
            [thisCell.cellBg setImage:resizableImg];
            
            //Extra setting for iPad
            if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                [thisCell.cellName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:30]];
                [thisCell.cellPrice setFont:[UIFont fontWithName:@"OpenSans" size:30]];
            }
            
            foodModel *thisModel = (foodModel*)[foodArray objectAtIndex:indexPath.row];
            [thisCell.cellImage setImageWithURL:[[APISingleton Instance] get_img_url:thisModel.img_path :thisModel.img[0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            thisCell.cellName.text = thisModel.name;
            thisCell.cellPrice.text = [NSString stringWithFormat:@" %@ %@ ", thisModel.currencies[[[[dataSingleton Instance] getCurrentCurrency] lowercaseString]], [[[dataSingleton Instance] getCurrentCurrency] uppercaseString]];
        }
    } else if(collectionView == _collectionSubMenuView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantCategoryCell" forIndexPath:indexPath];
        restaurantCategoryCell *thisCell = (restaurantCategoryCell*)cell;
        
        if([[dataSingleton Instance] lastOpenedSubMenu] == 0) {
            if(indexPath.row == 0) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Meal_Breakfast"]];
            } else if(indexPath.row == 1) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Meal_Dinner"]];
            } else if(indexPath.row == 2) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Meal_Lightmeal"]];
            } else if(indexPath.row == 3) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Meal_Lunch"]];
            } else if(indexPath.row == 4) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Meal_Supper"]];
            } else {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"BTN_Tab_Food"]];
            }
        } else if([[dataSingleton Instance] lastOpenedSubMenu] == 2) {
            if(![(NSString *)([categoryArray objectAtIndex:indexPath.row][@"icon"]) isEqual: @""]) {
                [thisCell.cellImg setImage:[UIImage imageNamed:@"UI_Category_Special"]];
            } else {
                [thisCell.cellImg setImageWithURL:[[APISingleton Instance] get_img_url:@"" :(NSString *)(categoryArray[indexPath.row][@"icon"])] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            }
        }
    }
    
    return cell;
}

- (void)selectCell:(UIGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if(_subMenuView.hidden) {
            CGPoint tapLocation = [gestureRecognizer locationInView:_collectionView];
            NSIndexPath *tapLocationIndexPath = [_collectionView indexPathForItemAtPoint:tapLocation];
            
            if(tapLocationIndexPath != nil) {
                selectedIndex = (int)tapLocationIndexPath.row;
                //NSLog(@"%d", selectedIndex);
                if(tapLocationIndexPath.row > 0) {
                    [self performSegueWithIdentifier:@"foodDetail" sender:self];
                }
            }
        } else {
            [self dismissSubMenuView];
        }
    }
}

- (void)selectCategory:(UIGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if(!_subMenuView.hidden) {
            CGPoint tapLocation = [gestureRecognizer locationInView:_collectionSubMenuView];
            NSIndexPath *tapLocationIndexPath = [_collectionSubMenuView indexPathForItemAtPoint:tapLocation];
            
            if(tapLocationIndexPath != nil) {
                if([[dataSingleton Instance] lastOpenedSubMenu] == 0) {
                    [self tabBarController:self.tabBarController shouldSelectViewController:[self.tabBarController.viewControllers objectAtIndex:0]];
                    
                    [foodArray removeAllObjects];
                    [foodArray addObject:@"Header"];
                    
                    if(tapLocationIndexPath.row < 5) {
                        [[APISingleton Instance] search_food_by_meal_id:_RestaurantModel.obj_id :[NSString stringWithFormat:@"%zd", (tapLocationIndexPath.row + 1)]: @"en" :YES :^(id json) {
                            if([json[@"status_code"] isEqual: @"200"]) {
                                if([json[@"results"] count] > 0) {
                                    resultJSON = json[@"results"];
                                    
                                    for(NSDictionary *index in resultJSON) {
                                        foodModel *thisModel = [[foodModel alloc] init];
                                        [thisModel initWithJSON:index];
                                        [foodArray addObject:thisModel];
                                    }
                                } else {
                                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                                }
                            } else {
                                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                            }
                            
                            [_collectionView reloadData];
                        }];
                    } else {
                        [[APISingleton Instance] search_restaurant_food:_RestaurantModel.obj_id :@"en" :YES :^(id json) {
                            if([json[@"status_code"] isEqual: @"200"]) {
                                if([json[@"results"] count] > 0) {
                                    resultJSON = json[@"results"];
                                    
                                    for(NSDictionary *index in resultJSON) {
                                        foodModel *thisModel = [[foodModel alloc] init];
                                        [thisModel initWithJSON:index];
                                        [foodArray addObject:thisModel];
                                    }
                                } else {
                                    [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
                                }
                            } else {
                                [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                            }
                            
                            [_collectionView reloadData];
                        }];
                    }
                } else if([[dataSingleton Instance] lastOpenedSubMenu] == 2) {
                    [[dataSingleton Instance] setSelectedCategory:(NSString *)([categoryArray objectAtIndex:tapLocationIndexPath.row][@"food_category_id"])];
                    [self.tabBarController setSelectedIndex:2];
                }
            }
        }
    }
}

- (void)pressCategory:(UILongPressGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_collectionSubMenuView];
        NSIndexPath *tapLocationIndexPath = [_collectionSubMenuView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            if([[dataSingleton Instance] lastOpenedSubMenu] == 0) {
                NSString *toastMsg = @"";
                if(tapLocationIndexPath.row == 0) {
                    toastMsg = @"Breakfast";
                } else if(tapLocationIndexPath.row == 1) {
                    toastMsg = @"Dinner";
                } else if(tapLocationIndexPath.row == 2) {
                    toastMsg = @"Lightmeal";
                } else if(tapLocationIndexPath.row == 3) {
                    toastMsg = @"Lunch";
                } else if(tapLocationIndexPath.row == 4) {
                    toastMsg = @"Supper";
                } else if(tapLocationIndexPath.row == 5) {
                    toastMsg = @"All";
                }
                
                [self.view makeToast:toastMsg];
            } else if([[dataSingleton Instance] lastOpenedSubMenu] == 2) {
                NSString *toastMsg = (NSString *)(categoryArray[tapLocationIndexPath.row][@"description"]);
                 [self.view makeToast:toastMsg];
            }
        }
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    
    int selfIndex = -1;
    if(viewController == [tabBarController.viewControllers objectAtIndex:0]) {
        selfIndex = 0;
    } else if(viewController == [tabBarController.viewControllers objectAtIndex:1]) {
        return YES;
    } else if(viewController == [tabBarController.viewControllers objectAtIndex:2]) {
        if([categoryArray count] == 0) {
            [SVProgressHUD showErrorWithStatus:@"No Category" maskType:SVProgressHUDMaskTypeBlack];
            return NO;
        }
        
        selfIndex = 2;
    }
    
    if([[dataSingleton Instance] isSubMenuOpen]) {
        if([[dataSingleton Instance] lastOpenedSubMenu] == selfIndex) {
            //The last open is me so just close it
            UITabBarItem *targetTabBarItem = [tabBarController.tabBar.items objectAtIndex:selfIndex];
            UIImage *selectedIcon = [[UIImage imageNamed:(NSString *)[[[dataSingleton Instance] restaurantTabBarIconArray] objectAtIndex:selfIndex]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [targetTabBarItem setImage:selectedIcon];
            
            [UIView animateWithDuration:0.2f animations:^{
                [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_subMenuView.frame.size.height / 2))];
            }completion:^(BOOL finished) {
                _subMenuView.hidden = YES;
            }];
            
            [[dataSingleton Instance] setIsSubMenuOpen:NO];
        } else {
            //The last open is other so close another and open me
            UITabBarItem *targetTabBarItem = [tabBarController.tabBar.items objectAtIndex:[[dataSingleton Instance] lastOpenedSubMenu]];
            UIImage *selectedIcon = [[UIImage imageNamed:(NSString *)[[[dataSingleton Instance] restaurantTabBarIconArray] objectAtIndex:[[dataSingleton Instance] lastOpenedSubMenu]]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            [targetTabBarItem setImage:selectedIcon];
            
            [UIView animateWithDuration:0.1f animations:^{
                [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_subMenuView.frame.size.height / 2))];
            }completion:^(BOOL finished) {
                _subMenuView.hidden = YES;
                
                UITabBarItem *targetTabBarItem = [tabBarController.tabBar.items objectAtIndex:selfIndex];
                UIImage *selectedIcon = [[UIImage imageNamed:@"BTN_Tab_Close"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
                
                UIImage *blueTab = [[appearanceSingleton Instance] imageFromColor:[UIColor whiteColor] forSize:CGSizeMake(4 + [[UIScreen mainScreen] bounds].size.width / 3, 53) withCornerRadius:0];
                
                [targetTabBarItem setImage:[[[appearanceSingleton Instance] combineTwoImage:blueTab :selectedIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
                
                _subMenuView.hidden = NO;
                [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_subMenuView.frame.size.height / 2))];
                [UIView animateWithDuration:0.2f animations:^{
                    [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, subMenuPoint)];
                }];
                
                [_collectionSubMenuView reloadData];

                [[dataSingleton Instance] setLastOpenedSubMenu:selfIndex];
                [[dataSingleton Instance] setIsSubMenuOpen:YES];
            }];
        }
    } else {
        //Sub menu is close so open it like normal
        UITabBarItem *targetTabBarItem = [tabBarController.tabBar.items objectAtIndex:selfIndex];
        UIImage *selectedIcon = [[UIImage imageNamed:@"BTN_Tab_Close"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        UIImage *blueTab = [[appearanceSingleton Instance] imageFromColor:[UIColor whiteColor] forSize:CGSizeMake(4 + [[UIScreen mainScreen] bounds].size.width / 3, 53) withCornerRadius:0];
        
        [targetTabBarItem setImage:[[[appearanceSingleton Instance] combineTwoImage:blueTab :selectedIcon] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        
        _subMenuView.hidden = NO;
        [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, [[UIScreen mainScreen] bounds].size.height + (_subMenuView.frame.size.height / 2))];
        [UIView animateWithDuration:0.2f animations:^{
            [_subMenuView setCenter:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, subMenuPoint)];
        }];
        
        [_collectionSubMenuView reloadData];
        
        [[dataSingleton Instance] setLastOpenedSubMenu:selfIndex];
        [[dataSingleton Instance] setIsSubMenuOpen:YES];
    }
    
    return NO;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
}

- (void)dismissSubMenuView {
    [[dataSingleton Instance] setIsSubMenuOpen:NO];
    
    UITabBarItem *otherItem = [self.tabBarController.tabBar.items objectAtIndex:[[dataSingleton Instance] lastOpenedSubMenu]];
    otherItem.image = [[UIImage imageNamed:(NSString *)[[[dataSingleton Instance] restaurantTabBarIconArray] objectAtIndex:[[dataSingleton Instance] lastOpenedSubMenu]]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    _subMenuView.hidden = YES;
}

@end
