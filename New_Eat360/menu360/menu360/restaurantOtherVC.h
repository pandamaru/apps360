//
//  restaurantOtherVC.h
//  menu360
//
//  Created by PandaMaru on 7/17/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "languageSingleton.h"
#import "restaurantModel.h"
#import "restaurantHeaderCell.h"
#import "restaurantContentCell.h"
#import <QuartzCore/QuartzCore.h>
#import "APISingleton.h"
#import <AFNetworking+ImageActivityIndicator.h>
#import "foodModel.h"
#import "foodDetailVC.h"
#import "restaurantCategoryCell.h"
#import "dataSingleton.h"
#import "restaurantMapVC.h"
#import "searchConditionModel.h"
#import <Toast/UIView+Toast.h>

@interface restaurantOtherVC : UIViewController <multiLanguageProtocol, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITabBarControllerDelegate> {
    NSDictionary *resultJSON;
}

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionSubMenuView;
@property (strong, nonatomic) IBOutlet UIView *subMenuView;

@property (strong, nonatomic) IBOutlet UIImageView *bg;

@property (strong, nonatomic) restaurantModel *RestaurantModel;
@property (strong, nonatomic) searchConditionModel *SearchConditionModel;

@end
