//
//  searchResultPinVC.h
//  menu360
//
//  Created by PandaMaru on 6/19/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import <GoogleMaps/GoogleMaps.h>
#import "sideMenuSingleton.h"
#import "appearanceSingleton.h"
#import "customInfoWindow.h"
#import "restaurantTabBarVC.h"

@interface searchResultPinVC : UIViewController <multiLanguageProtocol, GMSMapViewDelegate> {
    
}

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

@property (strong, nonatomic) NSString *searchText;
@property (strong, nonatomic) NSMutableArray *resultArray;

@end
