//
//  settingMenuController.m
//  menu360
//
//  Created by PandaMaru on 6/12/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "settingMenuController.h"

@implementation settingMenuController

@synthesize useView;

-(id)initWithViewController:(UIViewController*)_useView {
    self = [super init];
    if (self != nil) {
        useView = _useView;
    }
    return self;
}

- (void)callSettingMenu {
    [useView.menuContainerViewController toggleRightSideMenuCompletion:nil];
}

@end
