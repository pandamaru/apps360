//
//  searchCoditionModel.m
//  menu360
//
//  Created by Apps360[admin] on 21/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "searchConditionModel.h"

@implementation searchConditionModel

- (void)initWithJSON:(NSDictionary*)JSON {
    self.searchLatitude = JSON[@"search_for"][@"Latitude"];
    self.searchLongitude = JSON[@"search_for"][@"Longitude"];
    self.searchRadius = JSON[@"search_for"][@"Radius"];
    
    if(![JSON[@"search_for"][@"SearchText"] isEqual: @""]) {
        self.searchText = JSON[@"search_for"][@"SearchText"];
        self.searchType = JSON[@"search_for"][@"SearchType"];
        self.searchPossibleType = JSON[@"search_for"][@"possible_types"][self.searchType];
        self.isTopCategory = NO;
    } else {
        self.searchText = JSON[@"search_for"][@"TopCategoryName"];
        self.searchType = JSON[@"search_for"][@"TopType"];
        self.searchPossibleType = JSON[@"search_for"][@"TopCategoryID"];
        self.isTopCategory = YES;
    }
    
    self.searchPossibleKey = JSON[@"possible_keys"];
}

- (searchConditionModel *)clone {
    searchConditionModel *newObj = [[searchConditionModel alloc] init];
    
    newObj.searchLatitude = self.searchLatitude;
    newObj.searchLongitude = self.searchLongitude;
    newObj.searchPlace = self.searchPlace;
    newObj.searchRadius = self.searchRadius;
    
    if(!self.isTopCategory) {
        newObj.searchText = self.searchText;
        newObj.searchType = self.searchType;
        newObj.searchPossibleType = self.searchPossibleType;
        newObj.isTopCategory = self.isTopCategory;
    } else {
        newObj.searchText = self.searchText;
        newObj.searchType = self.searchType;
        newObj.searchPossibleType = self.searchPossibleType;
        newObj.isTopCategory = self.isTopCategory;
    }
    
    newObj.searchPossibleKey = self.searchPossibleKey;
    
    return newObj;
}

@end
