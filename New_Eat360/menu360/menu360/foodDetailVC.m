//
//  foodDetailVC.m
//  menu360
//
//  Created by PandaMaru on 7/9/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "foodDetailVC.h"

@interface foodDetailVC () {
    BOOL isNativeLanguage;
    NSString *nativeLanguage;
}

@end

@implementation foodDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isNativeLanguage = NO;
    
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.rightBarButtonItem = _rightBarButton;
    
    _contentView.layer.cornerRadius = 6;
    _contentView.layer.masksToBounds = YES;
    
    UIImage *nineSliceImg = [UIImage imageNamed:@"UI_Box@x2.png"];
    UIImage *resizableImg = [nineSliceImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [_contentBg setImage:resizableImg];
    
    [_foodDescContent setScrollEnabled:NO];
    [_foodIngrContent setScrollEnabled:NO];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle = _FoodModel.name;
    content.contentDescription = _FoodModel.desc;
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@/%@/%@", [[dataSingleton Instance] baseWeb], @"restaurant", _restaurant_id, @"menu", _FoodModel.obj_id]];
    content.imageURL = [[APISingleton Instance] get_img_url:_FoodModel.img_path :_FoodModel.img[0]];
    FBSDKShareButton *shareBtn = [[FBSDKShareButton alloc] init];
    shareBtn.shareContent = content;
    shareBtn.frame = CGRectMake(0, 0, shareBtn.frame.size.width, shareBtn.frame.size.height);
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
        
        [_foodPrice setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        [_foodName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:35]];
        [_translateBtnLabel setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        [_foodDesc setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:30]];
        [_foodDescContent setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        [_foodIngr setFont:[UIFont fontWithName:@"OpenSans-Semibold" size:30]];
        [_foodIngrContent setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        
        shareBtn.frame = CGRectMake(0, 0, shareBtn.frame.size.width * 1.5, shareBtn.frame.size.height * 1.5);
    }
    
    [_shareView addSubview:shareBtn];
    
    [_foodImg setImageWithURL:[[APISingleton Instance] get_img_url:_FoodModel.img_path :_FoodModel.img[0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _foodPrice.text = [NSString stringWithFormat:@" %@ %@ ", _FoodModel.currencies[[[[dataSingleton Instance] getCurrentCurrency] lowercaseString]], [[[dataSingleton Instance] getCurrentCurrency] uppercaseString]];
    
    _foodIngrContent.hidden = YES;
    [[APISingleton Instance] food:_FoodModel.obj_id :[[languageSingleton Instance] getCurrentLanguageForAPI] :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                //NSLog(@"%@", json[@"results"]);
                
                nativeLanguage = json[@"results"][@"native_lang"];
                
                _foodName.text = _FoodModel.name;
                _foodDescContent.text = _FoodModel.desc;
                
                [[APISingleton Instance] food_ingredients:_FoodModel.obj_id :[[languageSingleton Instance] getCurrentLanguageForAPI] :YES :^(id json) {
                    NSString *allIngr = @"";
                    
                    if([json[@"status_code"] isEqual: @"200"]) {
                        if([json[@"results"][@"ingredients"] count] > 0) {
                            for(NSDictionary *index in json[@"results"][@"ingredients"]) {
                                if([allIngr isEqual: @""]) {
                                    allIngr = index[@"name"];
                                } else {
                                    allIngr = [NSString stringWithFormat:@"%@ / %@", allIngr, index[@"name"]];
                                }
                            }
                        }
                    } else {
                        [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                    }
                    
                    _foodIngrContent.text = allIngr;
                    _foodIngrContent.hidden = NO;
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"loadCurrency" object:nil];
                }];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }];
    
   /* _foodName.text = _FoodModel.name;
    _foodDescContent.text = _FoodModel.desc;
    
    _foodIngrContent.hidden = YES;
    [[APISingleton Instance] food_ingredients:_FoodModel.obj_id :[[languageSingleton Instance] getCurrentLanguageForAPI] :YES :^(id json) {
        NSString *allIngr = @"";
        
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"][@"ingredients"] count] > 0) {
                for(NSDictionary *index in json[@"results"][@"ingredients"]) {
                    if([allIngr isEqual: @""]) {
                        allIngr = index[@"name"];
                    } else {
                        allIngr = [NSString stringWithFormat:@"%@ / %@", allIngr, index[@"name"]];
                    }
                }
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        _foodIngrContent.text = allIngr;
        _foodIngrContent.hidden = NO;
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"loadCurrency" object:nil];*/
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    /*if([self.navigationController.viewControllers count] > 3) {
        navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:3];
    } else if([self.navigationController.viewControllers count] > 2) {
        navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:2];
    }*/
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@", _FoodModel.name];
    _translateBtnLabel.text = [[languageSingleton Instance] getTextForKey:@"translate"];
    _foodName.text = _FoodModel.name;
    _foodDesc.text = [[languageSingleton Instance] getTextForKey:@"description"];
    _foodDescContent.text = _FoodModel.desc;
    _foodIngr.text = [[languageSingleton Instance] getTextForKey:@"ingredient"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)receiveNotification:(NSNotification *) notification {
    //NSLog(@"Load from Detail");
    if ([[notification name] isEqualToString:@"loadCurrency"]) {
        _foodPrice.text = [NSString stringWithFormat:@" %@ %@ ", _FoodModel.currencies[[[[dataSingleton Instance] getCurrentCurrency] lowercaseString]], [[[dataSingleton Instance] getCurrentCurrency] uppercaseString]];
    }
}

- (IBAction)touchTranslate:(id)sender {
    NSString *useLanguage = @"";
    
    if(!isNativeLanguage) {
        useLanguage = nativeLanguage;
    } else {
        useLanguage = [[languageSingleton Instance] getCurrentLanguageForAPI];
    }
    
    _foodIngrContent.hidden = YES;
    
    [[APISingleton Instance] food:_FoodModel.obj_id :useLanguage :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                //NSLog(@"%@", json[@"results"]);
                
                _foodName.text = json[@"results"][@"food_name"];
                _foodDescContent.text = json[@"results"][@"description"];
                
                [[APISingleton Instance] food_ingredients:_FoodModel.obj_id :useLanguage :YES :^(id json) {
                    NSString *allIngr = @"";
                    
                    if([json[@"status_code"] isEqual: @"200"]) {
                        if([json[@"results"][@"ingredients"] count] > 0) {
                            for(NSDictionary *index in json[@"results"][@"ingredients"]) {
                                if([allIngr isEqual: @""]) {
                                    allIngr = index[@"name"];
                                } else {
                                    allIngr = [NSString stringWithFormat:@"%@ / %@", allIngr, index[@"name"]];
                                }
                            }
                        }
                    } else {
                        [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
                    }
                    
                    _foodIngrContent.text = allIngr;
                    _foodIngrContent.hidden = NO;
                    
                    isNativeLanguage = !isNativeLanguage;
                    
                    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"loadCurrency" object:nil];
                }];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }];
}

@end
