//
//  dataSingleton.m
//  menu360
//
//  Created by PandaMaru on 7/17/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "dataSingleton.h"

@implementation dataSingleton

+ (id)Instance {
    static dataSingleton *dataSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataSingletonInstance = [[self alloc] init];
    });
    
    return dataSingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        /*
         
         ### Order is matter change it carefully ###
         
         */
        _languageArray = [NSArray arrayWithObjects:@"US", @"SA", @"CN", @"FR", @"DE", @"GR", @"IL", @"IN", @"IT", @"JP", @"KR", @"PL", @"RU", @"ES", @"TH", @"TR", nil];
        
        _currencyArray = [NSArray arrayWithObjects:@"EUR", @"USD", @"JPY", @"BGN", @"CZK", @"DKK", @"GBP", @"HUF", @"PLN", @"RON", @"SEK", @"CHF", @"NOK", @"HRK", @"RUB", @"TRY", @"AUD", @"BRL", @"CAD", @"CNY", @"HKD", @"IDR", @"ILS", @"INR", @"KRW", @"MXN", @"MYR", @"NZD", @"PHP", @"SGD", @"THB", @"ZAR", nil];
        
        _searchCategoryArray = [NSArray arrayWithObjects:@"restaurant_name", @"restaurant_type", @"restaurant_service", @"food_name", @"food_category", @"food_cuisine", @"food_ingredient", @"food_meal", nil];
        
        /*_searchCategoryDisplayArray = [NSArray arrayWithObjects:@"Restaurant", @"Type", @"Service", @"Food", @"Category", @"Cuisine", @"Ingredient", @"Meal", nil];*/
        
        /*_categoryPicArray = [NSArray arrayWithObjects:@"UI_Category_Starter", @"BTN_Tab_Food", @"UI_Category_Dessert", @"BTN_Tab_Food", @"BTN_Tab_Food", @"BTN_Tab_Food", @"BTN_Tab_Drink", @"BTN_Tab_Food", @"UI_Category_Breakfast", @"UI_Category_Special", @"UI_Category_Starter", @"BTN_Tab_Food", @"UI_Category_Starter", @"BTN_Tab_Drink", nil];*/
        
        _restaurantTabBarIconArray = [NSArray arrayWithObjects:@"BTN_Tab_Food", @"BTN_Tab_Drink", @"BTN_Tab_Other", nil];
        
        _baseWeb = @"http://www.eat360app.com/index.php";
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        currentCurr = [defaults objectForKey:@"curr"];
        
        if(currentCurr == nil) {
            NSLog(@"Default Currency is NULL, set to USD");
            
            currentCurr = @"USD";
            [defaults setValue:@"USD" forKey:@"curr"];
            [defaults synchronize];
        }
        
        [self setCurrentCurrency:currentCurr];
        
        _selectedMeal = @"";
        _selectedCategory = @"";
    }
    
    return  self;
}

- (void)setCurrentCurrency:(NSString*)currency {
    currentCurr = currency;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:currency forKey:@"curr"];
    [defaults synchronize];
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", @"Current Currency is", currentCurr]);
}

- (NSString*)getCurrentCurrency {
    return currentCurr;
}

/*- (NSString *)convertSearchCategoryDisplayToSearchCategory:(NSString *)text {
    NSString *result = @"";
    
    NSInteger index = [_searchCategoryDisplayArray indexOfObject:text];
    
    if(index != NSNotFound) {
        result = [_searchCategoryArray objectAtIndex:index];
    }
    
    return result;
}

- (NSString *)convertSearchCategoryToSearchCategoryDisplay:(NSString *)text {
    NSString *result = @"";
    
    NSInteger index = [_searchCategoryArray indexOfObject:text];
    
    if(index != NSNotFound) {
        result = [_searchCategoryDisplayArray objectAtIndex:index];
    }
    
    return result;
}*/

/*- (NSString *)getConditionFormat:(NSString *)searchText withType:(NSString *)searchType withLatitude:(NSString *)searchLatitude withLongtitude:(NSString *)searchLongtitude withRadius:(NSString *)searchRadius {
    return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ %@ %@ %@ km",
            searchText,
            [[languageSingleton Instance] getTextForKey:@"in"],
            searchType,
            [[languageSingleton Instance] getTextForKey:@"at"],
            [[languageSingleton Instance] getTextForKey:@"latitude"],
            searchLatitude,
            [[languageSingleton Instance] getTextForKey:@"longtitude"],
            searchLongtitude,
            [[languageSingleton Instance] getTextForKey:@"radius"],
            searchRadius];
}

- (NSString *)getUnconditionFormatWithLatitude:(NSString *)searchLatitude withLongtitude:(NSString *)searchLongtitude withRadius:(NSString *)searchRadius {
    return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ km",
            [[languageSingleton Instance] getTextForKey:@"at"],
            [[languageSingleton Instance] getTextForKey:@"latitude"],
            searchLatitude,
            [[languageSingleton Instance] getTextForKey:@"longtitude"],
            searchLongtitude,
            [[languageSingleton Instance] getTextForKey:@"radius"],
            searchRadius];
            
}*/

- (NSString *)getConditionFormat:(NSString *)searchText withType:(NSString *)searchType withPlace:(NSString *)searchPlace withRadius:(NSString *)searchRadius {
    return [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@ %@ km",
            searchText,
            [[languageSingleton Instance] getTextForKey:@"in"],
            searchType,
            [[languageSingleton Instance] getTextForKey:@"at"],
            searchPlace,
            [[languageSingleton Instance] getTextForKey:@"radius"],
            searchRadius];
}

- (NSString *)getUnconditionFormatWithPlace:(NSString *)searchPlace withRadius:(NSString *)searchRadius {
    return [NSString stringWithFormat:@"%@ %@ %@ %@ km",
            [[languageSingleton Instance] getTextForKey:@"at"],
            searchPlace, 
            [[languageSingleton Instance] getTextForKey:@"radius"],
            searchRadius];
}

@end
