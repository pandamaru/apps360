//
//  checkboxCell.h
//  menu360
//
//  Created by Apps360[admin] on 4/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface checkboxCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *checkImg;
@property (strong, nonatomic) IBOutlet UILabel *cellLabel;

@end
