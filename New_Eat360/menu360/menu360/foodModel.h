//
//  foodModel.h
//  menu360
//
//  Created by PandaMaru on 7/9/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface foodModel : NSObject

@property (strong, nonatomic) NSString *obj_id;
@property (strong, nonatomic) NSString *restaurant_id;
@property (strong, nonatomic) NSString *restaurant_name;
@property (strong, nonatomic) NSString *name;
/*@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *currency;*/
@property (strong, nonatomic) NSString *desc;

@property (strong, nonatomic) NSString *img_path;
@property (strong, nonatomic) NSArray *img;

@property (strong, nonatomic) NSDictionary *currencies;

- (void)initWithJSON:(NSDictionary*)JSON;

@end
