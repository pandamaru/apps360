//
//  APISingleton.m
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "APISingleton.h"

@implementation APISingleton

+ (id)Instance {
    static APISingleton *APISingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        APISingletonInstance = [[self alloc] init];
    });
    
    return APISingletonInstance;
}

- (id)init {
    if(self = [super init]) {
        //baseURL = @"http://api.eat360app.com";
        //baseURL = @"http://192.168.1.110/menu360api";
        
        _serverArray = [NSArray arrayWithObjects:@"http://api.eat360app.com", @"http://192.168.1.110/menu360api", nil];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        baseURL = [defaults objectForKey:@"server"];
        
        if(baseURL == nil) {
            NSLog(@"Default Server is NULL, set to http://api.eat360app.com");
            
            baseURL = @"http://api.eat360app.com";
            [defaults setValue:baseURL forKey:@"server"];
            [defaults synchronize];
        }
        
        [self setCurrentServer:baseURL];
    }
    
    return  self;
}

- (void)setCurrentServer:(NSString *)server {
    baseURL = server;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:baseURL forKey:@"server"];
    [defaults synchronize];
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@ %@", @"Current Sever is", baseURL]);
}

- (NSString *)getCurrentServer {
    return baseURL;
}

- (void)POST:(NSString*)API :(NSDictionary*)parameters :(BOOL)showHUD :(void (^)(id json))CallBack {
    NSURL *URL = [NSURL URLWithString:baseURL];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:URL];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    if(showHUD) {
        [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    }
    
    [manager POST:API parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
        if(showHUD) {
            [SVProgressHUD dismiss];
        }
        
        CallBack(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if(showHUD) {
            [SVProgressHUD dismiss];
        }
        NSLog(@"%@", error);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[languageSingleton Instance] getTextForKey:@"error"] message:[[languageSingleton Instance] getTextForKey:@"tryAgain"] delegate:nil cancelButtonTitle:[[languageSingleton Instance] getTextForKey:@"done"] otherButtonTitles:nil];
        [alertView show];
    }];
}

- (void)search_by_keyword:(NSString*)SearchText :(NSString*)SearchLang :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_by_keyword" :@{@"SearchText": SearchText,
                                       @"SearchLang": SearchLang,
                                       @"LanguageID": LanguageID} :showHUD :^(id json) {
                                           CallBack(json);
                                       }];
}

- (void)search_restaurant:(NSString*)SearchText :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant" :@{@"SearchText": SearchText,
                                       @"LanguageID": LanguageID} :showHUD :^(id json) {
                                           CallBack(json);
                                       }];
}

- (void)search_restaurant_menus:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_menus" :@{@"RestaurantID": RestaurantID,
                                            @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                CallBack(json);
                                            }];
}

- (void)search_restaurant_food:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_food" :@{@"RestaurantID": RestaurantID,
                                            @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                CallBack(json);
                                            }];
}

- (void)search_restaurant_drink:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_drink" :@{@"RestaurantID": RestaurantID,
                                             @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                CallBack(json);
                                            }];
}

- (void)search_restaurant_menu_category:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_menu_category" :@{@"RestaurantID": RestaurantID,
                                             @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                 CallBack(json);
                                             }];
}

- (void)search_restaurant_menu_by_category:(NSString*)RestaurantID :(NSString*)CategoryID  :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_menu_by_category" :@{@"RestaurantID": RestaurantID,
                                                        @"CategoryID": CategoryID,
                                                        @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                            CallBack(json);
                                                        }];
}

- (void)search_by_location:(NSString*)MinLatitude :(NSString*)MinLongitude :(NSString*)MaxLatitude :(NSString*)MaxLongitude :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_by_location" :@{@"MinLatitude": MinLatitude,
                                        @"MinLongitude": MinLongitude,
                                        @"MaxLatitude": MaxLatitude,
                                        @"MaxLongitude": MaxLongitude,
                                        @"LanguageID": LanguageID} :showHUD :^(id json) {
                                            CallBack(json);
                                        }];
}

- (void)restaurant:(NSString*)RestaurantID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"restaurant" :@{@"RestaurantID": RestaurantID,
                                @"LanguageID": LanguageID} :showHUD :^(id json) {
                                    CallBack(json);
                                }];
}

- (void)search_by_qr:(NSString*)URL :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_by_qr" :@{@"URL": URL,
                                  @"LanguageID": LanguageID} :showHUD :^(id json) {
                                      CallBack(json);
                                  }];
}

- (void)food_ingredients:(NSString*)FoodID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"food_ingredients" :@{@"FoodID": FoodID,
                                      @"LanguageID": LanguageID} :showHUD :^(id json) {
                                          CallBack(json);
                                      }];
}

/*- (void)currency_converter:(NSString*)Amount :(NSString*)FromCurrency :(NSString*)ToCurrency :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"currency_converter" :@{@"Amount": Amount,
                                        @"FromCurrency": FromCurrency,
                                        @"ToCurrency": ToCurrency} :showHUD :^(id json) {
                                            CallBack(json);
                                        }];
}*/

- (void)autocomplete:(NSString *)SearchText :(NSString *)Latitude :(NSString *)Longitude :(NSString *)Radius :(NSString *)SearchLang :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"autocomplete" :@{@"SearchText": SearchText,
                                  @"Latitude": Latitude,
                                  @"Longitude": Longitude,
                                  @"Radius": Radius,
                                  @"SearchLang": SearchLang} :showHUD :^(id json) {
                                      CallBack(json);
                                  }];
}

- (void)search2:(NSString *)Latitude :(NSString *)Longitude :(NSString *)Radius :(NSString *)SearchText :(NSString *)SearchLang :(NSString *)SearchType :(NSString *)TopCategoryID :(NSString *)TopType :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search2" :@{@"Latitude": Latitude,
                             @"Longitude": Longitude,
                             @"Radius": Radius,
                             @"SearchText": SearchText,
                             @"SearchLang": SearchLang,
                             @"SearchType": SearchType,
                             @"TopCategoryID": TopCategoryID,
                             @"TopType": TopType} :showHUD :^(id json) {
                                 CallBack(json);
                             }];
}

- (void)search_menus_by_meal_id:(NSString *)RestaurantID :(NSString *)MealID :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_menus_by_meal_id" :@{@"RestaurantID": RestaurantID,
                                             @"MealID": MealID,
                                             @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                CallBack(json);
                                            }];
}

- (void)search_food_by_meal_id:(NSString *)RestaurantID :(NSString *)MealID :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_food_by_meal_id" :@{@"RestaurantID": RestaurantID,
                                            @"MealID": MealID,
                                            @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                CallBack(json);
                                            }];
}

- (void)get_top_category:(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"get_top_category" :@{@"LanguageID": LanguageID} :showHUD :^(id json) {
        CallBack(json);
    }];
}

- (void)multiple_restaurant_details:(NSString *)RestaurantIDs :(NSString *)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"multiple_restaurant_details" :@{@"RestaurantIDs": RestaurantIDs,
                                                 @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                     CallBack(json);
                                                 }];
}

- (void)search_menus_by_cuisine_id:(NSString*)RestaurantID :(NSString*)CuisineID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_menus_by_cuisine_id" :@{@"RestaurantID": RestaurantID,
                                                @"CuisineID": CuisineID,
                                                @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                    CallBack(json);
                                                }];
}

- (void)search_restaurant_menu_by_food_keyword:(NSString*)RestaurantID :(NSString*)Keyword :(NSString*)KeywordType :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"search_restaurant_menu_by_food_keyword" :@{@"RestaurantID": RestaurantID,
                                                            @"Keyword": Keyword,
                                                            @"KeywordType": KeywordType,
                                                            @"LanguageID": LanguageID} :showHUD :^(id json) {
                                                                CallBack(json);
                                                            }];
}

- (void)food:(NSString*)FoodID :(NSString*)LanguageID :(BOOL)showHUD :(void (^)(id json))CallBack {
    [self POST:@"food" :@{@"FoodID": FoodID,
                          @"LanguageID": LanguageID} :showHUD :^(id json) {
                              CallBack(json);
                          }];
}

- (NSURL*)get_img_url:(NSString*)path :(NSString*)img {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@%@", baseURL, path, img]];
}

@end
