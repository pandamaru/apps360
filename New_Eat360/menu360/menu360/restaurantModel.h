//
//  restaurantModel.h
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface restaurantModel : NSObject

@property (strong, nonatomic) NSString *obj_id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *postCode;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *openinghour;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;

@property (strong, nonatomic) NSString *img_path;
@property (strong, nonatomic) NSArray *img;

- (void)initWithJSON:(NSDictionary*)JSON;

@end
