//
//  searchResultPinVC.m
//  menu360
//
//  Created by PandaMaru on 6/19/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "searchResultPinVC.h"

@implementation searchResultPinVC {
    NSMutableArray *markerArray;
    NSMutableArray *imgURLArray;
    NSMutableArray *imgArray;
    
    restaurantModel *selectedRestaurant;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Insert original image to TabBar
    UITabBarItem *mapItem = [self.tabBarController.tabBar.items objectAtIndex:0];
    mapItem.image = [[UIImage imageNamed:@"BTN_Tab_Name"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    markerArray = [[NSMutableArray alloc] init];
    imgURLArray = [[NSMutableArray alloc] init];
    imgArray = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:2];
    
    [_mapView clear];
    [markerArray removeAllObjects];
    NSMutableArray *allMarker = [[NSMutableArray alloc] init];
    for(restaurantModel *thisModel in _resultArray) {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake([thisModel.latitude doubleValue], [thisModel.longitude doubleValue]);
        GMSMarker *marker = [GMSMarker markerWithPosition:position];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.icon = [UIImage imageNamed:@"UI_Map_Marker"];
        marker.title = thisModel.obj_id;
        marker.map = _mapView;
        
        [markerArray addObject:thisModel.obj_id];
        [allMarker addObject:marker];
    }
    
    GMSMutablePath *path = [[GMSMutablePath alloc] init];
    
    for(GMSMarker *marker in allMarker) {
        [path addCoordinate:marker.position];
    }
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:path];
    
    [_mapView setCamera:[_mapView cameraForBounds:bounds insets:UIEdgeInsetsMake(50, 50, 50, 50)]];
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@ \"%@\"", [[languageSingleton Instance] getTextForKey:@"search"], _searchText];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"restaurantMenu"]) {
        restaurantTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurantFood = [vc.viewControllers objectAtIndex:0];
        vc.restaurantDrink = [vc.viewControllers objectAtIndex:1];
        vc.restaurantOther = [vc.viewControllers objectAtIndex:2];
        
        vc.restaurantFood.RestaurantModel = vc.restaurantDrink.RestaurantModel = vc.restaurantOther.RestaurantModel = selectedRestaurant;
        
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        vc.navigationItem.rightBarButtonItem = navCon.navigationItem.rightBarButtonItem;
    }
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    _mapView.selectedMarker = marker;
    return YES;
}

- (UIView*)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    customInfoWindow *infoWindow = [[[NSBundle mainBundle] loadNibNamed:@"infoWindow" owner:self options:nil] objectAtIndex:0];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    infoWindow.frame = CGRectMake(infoWindow.frame.origin.x, infoWindow.frame.origin.y, screenRect.size.width * 0.65, screenRect.size.width * 0.65 * 0.63);
    
    
    for(restaurantModel *thisModel in _resultArray) {
        if([thisModel.obj_id isEqualToString:marker.title]) {
            infoWindow.infoName.text = thisModel.name;
            infoWindow.infoDetail.text = thisModel.type;
            
            NSURL *img_path = [[APISingleton Instance] get_img_url:thisModel.img_path :thisModel.img[0]];
            NSInteger indexPath = [imgURLArray indexOfObject:[img_path absoluteString]];
            
            if(indexPath != NSNotFound) {
                [infoWindow.infoImg setImage:imgArray[indexPath]];
            } else {
                dispatch_queue_t imageQueue = dispatch_queue_create("Image Queue", nil);
                
                dispatch_async(imageQueue, ^{
                    
                    NSData *imageData = [NSData dataWithContentsOfURL:img_path];
                    UIImage *image = [UIImage imageWithData:imageData];
                    
                    if (!image) return;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [imgURLArray addObject:[img_path absoluteString]];
                        [imgArray addObject:image];
                        
                        _mapView.selectedMarker = nil;
                        _mapView.selectedMarker = marker;
                    });
                    
                });
            }
            
            [infoWindow makeAutolayout];
            break;
        }
    }
    
    return infoWindow;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker {
    NSInteger indexPath = (int)[markerArray indexOfObject:marker.title];
    
    if(indexPath != NSNotFound) {
        selectedRestaurant = [_resultArray objectAtIndex:indexPath];
        /*self.tabBarController.tabBar.hidden = YES;
        self.tabBarController.tabBar.layer.frame = CGRectMake(self.tabBarController.tabBar.layer.frame.origin.x, self.tabBarController.tabBar.layer.frame.origin.y, self.tabBarController.tabBar.layer.frame.size.width, 0);*/
        [self performSegueWithIdentifier:@"restaurantMenu" sender:self];
    }
}

@end

