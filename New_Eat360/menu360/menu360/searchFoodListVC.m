//
//  searchFoodListVC.m
//  menu360
//
//  Created by Apps360[admin] on 1/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "searchFoodListVC.h"

@interface searchFoodListVC ()

@end

@implementation searchFoodListVC {
    NSMutableArray *foodArray;
    NSMutableArray *restaurantIdArray;
    NSMutableArray *restaurantArray;
    UITapGestureRecognizer *tapCell;
    NSInteger selectedIndex;
    
    CGFloat collectionOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Insert original image to TabBar
    UITabBarItem *mapItem = [self.tabBarController.tabBar.items objectAtIndex:1];
    mapItem.image = [[UIImage imageNamed:@"BTN_Tab_Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    if(_selectedType == 0) {
        _headerIcon.image = [[UIImage imageNamed:@"BTN_Search_Walk"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    } else if(_selectedType == 1) {
        _headerIcon.image = [[UIImage imageNamed:@"BTN_Search_Drive"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    } else if(_selectedType == 2) {
        _headerIcon.image = [[UIImage imageNamed:@"BTN_Search_Global"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
        
        [_headerLabel setFont:[UIFont fontWithName:@"OpenSans" size:35]];
    }
    
    _collectionView.hidden = YES;
     
     tapCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCell:)];
     [_collectionView addGestureRecognizer:tapCell];
    
    foodArray = [[NSMutableArray alloc] init];
    restaurantIdArray = [[NSMutableArray alloc] init];
    for(NSDictionary *index in _resultJSON) {
        foodModel *thisModel = [[foodModel alloc] init];
        [thisModel initWithJSON:index];
        [foodArray addObject:thisModel];
        
        BOOL isContain = false;
        for(NSString *restaurantId in restaurantIdArray) {
            if(restaurantId == thisModel.restaurant_id) {
                isContain = true;
                break;
            }
        }
        
        if(!isContain) {
            [restaurantIdArray addObject:thisModel.restaurant_id];
        }
    }
    
    NSString *restaurantIdList = @"";
    for(NSString *restaurantId in restaurantIdArray) {
        if([restaurantIdList isEqual: @""]) {
            restaurantIdList = restaurantId;
        } else {
            restaurantIdList = [NSString stringWithFormat:@"%@,%@", restaurantIdList, restaurantId];
        }
    }
    
    [[APISingleton Instance] multiple_restaurant_details:restaurantIdList :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                restaurantArray = [[NSMutableArray alloc] init];
                
                for(NSDictionary *index in json[@"results"] ) {
                    restaurantModel *thisModel = [[restaurantModel alloc] init];
                    [thisModel initWithJSON:index];
                    [restaurantArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        _collectionView.hidden = NO;
    }];
    
    self.tabBarController.delegate = self;
    
    _headerLabel.text = _SearchConditionModel.searchPlace;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"loadCurrency" object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:2];
    
    collectionOffset = 20 + _headerIcon.frame.size.height + 5;
    [_collectionView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@ \"%@\"", [[languageSingleton Instance] getTextForKey:@"search"], _SearchConditionModel.searchText];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"foodDetail"]) {
        foodDetailVC *vc = [segue destinationViewController];
        //Data sending here
        vc.FoodModel = (foodModel*)[foodArray objectAtIndex:selectedIndex];
        
        UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
        
        vc.rightBarButton = navCon.navigationItem.rightBarButtonItem;
    } else if([[segue identifier] isEqualToString:@"restaurantMenu"]) {
        restaurantTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurantFood = [vc.viewControllers objectAtIndex:0];
        vc.restaurantDrink = [vc.viewControllers objectAtIndex:1];
        vc.restaurantOther = [vc.viewControllers objectAtIndex:2];
        
        vc.restaurantFood.RestaurantModel = vc.restaurantDrink.RestaurantModel = vc.restaurantOther.RestaurantModel = _RestaurantModel;
        
        vc.restaurantFood.SearchConditionModel = vc.restaurantDrink.SearchConditionModel = vc.restaurantOther.SearchConditionModel = [_SearchConditionModel clone];
        
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        vc.navigationItem.rightBarButtonItem = navCon.navigationItem.rightBarButtonItem;
    }
}

- (void)receiveNotification:(NSNotification *) notification {
    //NSLog(@"Load from Food");
    if ([[notification name] isEqualToString:@"loadCurrency"]) {
        [_collectionView reloadData];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return CGSizeMake(0, collectionOffset);
    }
    
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width / 2.8);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 0, 5, 0); //t,l,b,r
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return foodArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"searchFoodListCell" forIndexPath:indexPath];
    searchFoodListCell *thisCell = (searchFoodListCell*)cell;
    
    thisCell.cellView.layer.cornerRadius = 6;
    thisCell.cellView.layer.masksToBounds = YES;
    UIImage *nineSliceImg = [UIImage imageNamed:@"UI_Box@x2.png"];
    UIImage *resizableImg = [nineSliceImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [thisCell.cellBg setImage:resizableImg];

    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [thisCell.cellName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:30]];
        [thisCell.cellPrice setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        [thisCell.cellRestaurentName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:30]];
    }
    
    foodModel *thisModel = (foodModel*)[foodArray objectAtIndex:indexPath.row];
    [thisCell.cellImage setImageWithURL:[[APISingleton Instance] get_img_url:thisModel.img_path :thisModel.img[0]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    thisCell.cellName.text = thisModel.name;
    thisCell.cellPrice.text = [NSString stringWithFormat:@" %@ %@ ", thisModel.currencies[[[[dataSingleton Instance] getCurrentCurrency] lowercaseString]], [[[dataSingleton Instance] getCurrentCurrency] uppercaseString]];
    thisCell.cellRestaurentName.text = thisModel.restaurant_name;
    
    thisCell.restaurantId = thisModel.restaurant_id;
    thisCell.parentVC = self;
    
    return cell;
}

- (void)selectCell:(UIGestureRecognizer *)gestureRecognizer {
    if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_collectionView];
        NSIndexPath *tapLocationIndexPath = [_collectionView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            selectedIndex = tapLocationIndexPath.row;
            //NSLog(@"%d", selectedIndex);
            [self performSegueWithIdentifier:@"foodDetail" sender:self];
        }
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if(viewController == [tabBarController.viewControllers objectAtIndex:1]) {
        searchFoodPinVC *vc = (searchFoodPinVC*)viewController;

        vc.searchText = _searchText;
        vc.resultArray = restaurantArray;
    }
    
    return YES;
}

@end
