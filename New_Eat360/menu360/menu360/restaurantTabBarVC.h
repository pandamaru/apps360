//
//  restaurantTabBarVC.h
//  menu360
//
//  Created by PandaMaru on 7/3/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "appearanceSingleton.h"
#import "restaurantFoodVC.h"
#import "restaurantDrinkVC.h"
#import "restaurantOtherVC.h"

@interface restaurantTabBarVC : UITabBarController

@property (strong, nonatomic) restaurantFoodVC *restaurantFood;
@property (strong, nonatomic) restaurantDrinkVC *restaurantDrink;
@property (strong, nonatomic) restaurantOtherVC *restaurantOther;

@end
