//
//  restaurantHeaderCell.h
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "restaurantModel.h"

@interface restaurantHeaderCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (strong, nonatomic) IBOutlet UIImageView *cellBg;
@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellAddress;
@property (strong, nonatomic) IBOutlet UIButton *cellMap;

@property (strong, nonatomic) UIViewController *parentVC;
@property (strong, nonatomic) restaurantModel *RestaurantModel;

@property (strong, nonatomic) IBOutlet UIView *searchTypeView;
@property (strong, nonatomic) IBOutlet UILabel *searchTypeLabel;
@property (strong, nonatomic) IBOutlet UIButton *searchTypeCloseBtn;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *addressAL;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *bgAL;

- (void)setForHidden:(BOOL)isHidden;

- (IBAction)touchMap:(id)sender;

- (IBAction)touchSearchTypeClose:(id)sender;

@end
