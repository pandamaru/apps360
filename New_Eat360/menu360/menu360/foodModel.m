//
//  foodModel.m
//  menu360
//
//  Created by PandaMaru on 7/9/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "foodModel.h"

@implementation foodModel

- (void)initWithJSON:(NSDictionary*)JSON {
    self.obj_id = JSON[@"id"];
    self.restaurant_id = JSON[@"restaurant_id"];
    self.restaurant_name = JSON[@"restaurant_name"];
    self.name = JSON[@"food_name"];
    /*self.price = JSON[@"price"];
    self.currency = JSON[@"currency"];*/
    self.desc = JSON[@"description"];
    self.img_path = JSON[@"images"][@"img_path"];
    self.img = JSON[@"images"][@"images"];
    
    self.currencies = JSON[@"currencies"];
}

@end
