//
//  customAutoCompleteTextField.h
//  menu360
//
//  Created by Apps360[admin] on 11/08/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "MLPAutoCompleteTextField.h"
#import "customAutoCompleteCell.h"
#import "languageSingleton.h"

@interface customAutoCompleteTextField : MLPAutoCompleteTextField

@property (assign) BOOL isSearchCategory;
@property (assign) CGPoint autoCompleteTableOffset;
@property (assign) CGFloat autoCompleteTableWidth;
@property (strong) UIColor *searchCategoryTextColor;

- (void)customInit;
- (void)setCustomWithOffset:(CGPoint) offset andWidth:(CGFloat) width;

@end
