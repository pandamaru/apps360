//
//  sideMenuViewController.h
//  menu360
//
//  Created by PandaMaru on 6/12/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "MyCountryPicker.h"
#import <MFSideMenuContainerViewController.h>
#import "languageSingleton.h"
#import "sideMenuSingleton.h"
#import "dataSingleton.h"
#import "aboutVC.h"
#import "APISingleton.h"

@interface sideMenuVC : UITableViewController <multiLanguageProtocol, CountryPickerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property(nonatomic) MFSideMenuContainerViewController* controller;
@property (strong, nonatomic) IBOutlet UIButton *UI_Language;
@property (strong, nonatomic) IBOutlet UIButton *UI_Currency;
@property (strong, nonatomic) IBOutlet UIButton *UI_Setting;
@property (strong, nonatomic) IBOutlet UIButton *UI_About;

- (IBAction)callLanguage:(id)sender;
- (IBAction)callCurrency:(id)sender;
- (IBAction)callSetting:(id)sender;
- (IBAction)callAbout:(id)sender;


@end
