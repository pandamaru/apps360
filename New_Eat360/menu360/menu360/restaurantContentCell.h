//
//  restaurantContentCell.h
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface restaurantContentCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *cellView;
@property (strong, nonatomic) IBOutlet UIImageView *cellBg;
@property (strong, nonatomic) IBOutlet UIImageView *cellImage;
@property (strong, nonatomic) IBOutlet UILabel *cellName;
@property (strong, nonatomic) IBOutlet UILabel *cellPrice;

@end
