//
//  customInfoWindow.m
//  menu360
//
//  Created by PandaMaru on 7/21/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "customInfoWindow.h"

@implementation customInfoWindow

- (void)makeAutolayout {
    CGFloat widthRatio = self.frame.size.width / _bgImg.frame.size.width;
    CGFloat heightRatio = self.frame.size.height / _bgImg.frame.size.height;
    _bgImg.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_infoName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:35]];
        [_infoDetail setFont:[UIFont fontWithName:@"OpenSans" size:27.5]];
        [_infoBtn.titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:30]];
        
        _infoView.frame = CGRectMake(_infoView.frame.origin.x * widthRatio, _infoView.frame.origin.y * heightRatio, _infoView.frame.size.width * widthRatio, _infoView.frame.size.height * heightRatio);
        
        _infoName.frame = CGRectMake(_infoName.frame.origin.x, _infoName.frame.origin.y, _infoName.frame.size.width * widthRatio, _infoName.frame.size.height * heightRatio);
        
        _infoImg.frame = CGRectMake(_infoImg.frame.origin.x, _infoName.frame.origin.y + _infoName.frame.size.height + (5 * heightRatio), _infoImg.frame.size.width * widthRatio, _infoImg.frame.size.height * heightRatio);
        
        _infoDetail.frame = CGRectMake(_infoImg.frame.origin.x + _infoImg.frame.size.width + (10 * widthRatio), _infoName.frame.origin.y + _infoName.frame.size.height + (5 * heightRatio), _infoDetail.frame.size.width * widthRatio, _infoDetail.frame.size.height * heightRatio);
        [_infoDetail sizeToFit];
        
        _infoBtn.frame = CGRectMake(_infoImg.frame.origin.x + _infoImg.frame.size.width + (10 * widthRatio), _infoView.frame.size.height - (_infoBtn.frame.size.height * heightRatio), _infoBtn.frame.size.width * widthRatio, _infoBtn.frame.size.height * heightRatio);
    }
}

@end
