//
//  sideMenuSingleton.m
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "sideMenuSingleton.h"

@implementation sideMenuSingleton

+ (id)Instance {
    static sideMenuSingleton *sideMenuSingletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sideMenuSingletonInstance = [[self alloc] init];
    });
    
    return sideMenuSingletonInstance;
}

- (id)init {
    if(self = [super init]) {

    }
    
    return  self;
}

- (void)setCurrentView:(id <multiLanguageProtocol>)view {
    backView = currentView;
    currentView = view;
}

- (void)applyLanguageToView {
    if(backView != nil) {
        [backView setUILanguage];
    }
    
    if(currentView != nil) {
        [currentView setUILanguage];
    }
}

@end
