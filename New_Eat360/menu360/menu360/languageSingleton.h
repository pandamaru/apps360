//
//  languageSingleton.h
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface languageSingleton : NSObject {
    NSString *currentLang;
    NSDictionary *data;
}

//@property (strong, nonatomic) NSString *text;

+ (id)Instance;
- (void)setCurrentLanguage:(NSString*)language;
- (NSString*)getCurrentLanguage;
- (NSString*)getCurrentLanguageForAPI;
//- (NSInteger*)get
- (NSString*)getTextForKey:(NSString*)key;

@end
