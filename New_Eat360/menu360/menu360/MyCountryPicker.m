//
//  MyCountryPicker.m
//  menu360
//
//  Created by PandaMaru on 6/19/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "MyCountryPicker.h"

@implementation MyCountryPicker

+ (NSDictionary *)countryNamesByCode
{
    static NSDictionary *_countryNamesByCode = nil;
    if (!_countryNamesByCode)
    {
        NSMutableDictionary *namesByCode = [NSMutableDictionary dictionary];
        for(NSString *code in [NSLocale ISOCountryCodes]) {
            NSString *countryName = @"";

            if([[[dataSingleton Instance] languageArray] containsObject:code]) {
                if([code  isEqual: @"US"]) {
                    countryName = @"English";
                } else if([code  isEqual: @"SA"]) {
                    countryName = @"Arabic";
                } else if([code  isEqual: @"CN"]) {
                    countryName = @"Chinese";
                } else if([code  isEqual: @"FR"]) {
                    countryName = @"French";
                } else if([code  isEqual: @"DE"]) {
                    countryName = @"German";
                } else if([code  isEqual: @"GR"]) {
                    countryName = @"Greek";
                } else if([code  isEqual: @"IL"]) {
                    countryName = @"Hebrew";
                } else if([code  isEqual: @"IN"]) {
                    countryName = @"Hindi";
                } else if([code  isEqual: @"IT"]) {
                    countryName = @"Italian";
                } else if([code  isEqual: @"JP"]) {
                    countryName = @"Japanese";
                } else if([code  isEqual: @"KR"]) {
                    countryName = @"Korean";
                } else if([code  isEqual: @"PL"]) {
                    countryName = @"Polish";
                } else if([code  isEqual: @"RU"]) {
                    countryName = @"Russian";
                } else if([code  isEqual: @"ES"]) {
                    countryName = @"Spanish";
                } else if([code  isEqual: @"TH"]) {
                    countryName = @"Thai";
                } else if([code  isEqual: @"TR"]) {
                    countryName = @"Turkish";
                }
                //NSString *countryName = [[NSLocale currentLocale] displayNameForKey:NSLocaleCountryCode value:code];
                
                //NSLog(@"%@ : %@", countryName, code);
                
                //workaround for simulator bug
                if (!countryName)
                {
                    countryName = [[NSLocale localeWithLocaleIdentifier:@"en_US"] displayNameForKey:NSLocaleCountryCode value:code];
                }
                
                namesByCode[code] = countryName ?: code;
            }
        }
        _countryNamesByCode = [namesByCode copy];
    }
    return _countryNamesByCode;
}

@end
