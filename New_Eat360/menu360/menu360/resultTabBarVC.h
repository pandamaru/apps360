//
//  resultTabBarVC.h
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "appearanceSingleton.h"
#import "searchResultListVC.h"
#import "searchResultPinVC.h"

@interface resultTabBarVC : UITabBarController

@property (strong, nonatomic) searchResultListVC *searchResultList;
@property (strong, nonatomic) searchResultPinVC *searchResultPin;

@end
