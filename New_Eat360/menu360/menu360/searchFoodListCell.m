//
//  searchFoodListCell.m
//  menu360
//
//  Created by Apps360[admin] on 3/09/2015.
//  Copyright (c) 2015 apps360. All rights reserved.
//

#import "searchFoodListCell.h"

@implementation searchFoodListCell

- (IBAction)touchRestaurantName:(id)sender {
    [[APISingleton Instance] restaurant:_restaurantId :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                restaurantModel *thisModel = [[restaurantModel alloc] init];
                [thisModel initWithJSON:json[@"results"][0]];
                
                searchFoodListVC *SearchFoodListVC = (searchFoodListVC *)_parentVC;
                SearchFoodListVC.RestaurantModel = thisModel;
                
                [_parentVC performSegueWithIdentifier:@"restaurantMenu" sender:self];
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
    }];
}

@end
