//
//  foodDetailVC.h
//  menu360
//
//  Created by PandaMaru on 7/9/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "foodModel.h"
#import "APISingleton.h"
#import "sideMenuSingleton.h"
#import <AFNetworking+ImageActivityIndicator.h>
#import <FBSDKCoreKit.h>
#import <FBSDKShareKit.h>
#import "dataSingleton.h"
#import "languageSingleton.h"

@interface foodDetailVC : UIViewController <multiLanguageProtocol>

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UIImageView *contentBg;
@property (strong, nonatomic) IBOutlet UIImageView *foodImg;
@property (strong, nonatomic) IBOutlet UILabel *foodPrice;
@property (strong, nonatomic) IBOutlet UILabel *foodName;
@property (strong, nonatomic) IBOutlet UILabel *translateBtnLabel;
@property (strong, nonatomic) IBOutlet UILabel *foodDesc;
@property (strong, nonatomic) IBOutlet UITextView *foodDescContent;
@property (strong, nonatomic) IBOutlet UILabel *foodIngr;
@property (strong, nonatomic) IBOutlet UITextView *foodIngrContent;
@property (strong, nonatomic) IBOutlet UIView *shareView;

@property (strong, nonatomic) NSString *restaurant_id;
@property (strong, nonatomic) foodModel *FoodModel;
@property (strong, nonatomic) UIBarButtonItem *rightBarButton;

- (IBAction)touchTranslate:(id)sender;

@end
