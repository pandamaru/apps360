//
//  restaurantCategoryCell.h
//  menu360
//
//  Created by PandaMaru on 7/14/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface restaurantCategoryCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *cellImg;

@end
