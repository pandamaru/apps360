//
//  sideMenuSingleton.h
//  menu360
//
//  Created by PandaMaru on 6/24/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "multiLanguageProtocol.h"

@interface sideMenuSingleton : NSObject {
    id <multiLanguageProtocol> currentView;
    id <multiLanguageProtocol> backView;
}

+ (id)Instance;
- (void)setCurrentView:(id <multiLanguageProtocol>)view;
- (void)applyLanguageToView;

@end
