//
//  restaurantModel.m
//  menu360
//
//  Created by PandaMaru on 7/6/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "restaurantModel.h"

@implementation restaurantModel

- (void)initWithJSON:(NSDictionary*)JSON {
    self.obj_id = JSON[@"restaurant_id"];
    self.name = JSON[@"name"];
    self.address = JSON[@"address"];
    self.postCode = JSON[@"postcode"];
    self.state = JSON[@"state"];
    self.country = JSON[@"country"];
    self.type = JSON[@"restaurant_types"][0][@"restaurant_type_name"];
    self.openinghour = JSON[@"openinghour"];
    self.latitude = JSON[@"latitude"];
    self.longitude = JSON[@"longitude"];
    
    self.img_path = JSON[@"images"][@"img_path"];
    self.img = JSON[@"images"][@"images"];
}

@end
