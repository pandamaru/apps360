//
//  homeViewController.h
//  menu360
//
//  Created by PandaMaru on 6/3/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "multiLanguageProtocol.h"
#import "languageSingleton.h"
#import "settingMenuController.h"
#import "sideMenuSingleton.h"
#import "APISingleton.h"
#import "resultTabBarVC.h"
#import <MLPAutoCompleteTextField.h>
#import "customAutoCompleteTextField.h"
#import "locationSingleton.h"
#import "searchConditionModel.h"
#import "resultFoodTabBarVC.h"
#import "checkboxCell.h"
#import <Toast/UIView+Toast.h>

@interface homeVC : UIViewController <multiLanguageProtocol, UITextFieldDelegate, MLPAutoCompleteTextFieldDelegate> {
    NSDictionary *resultJSON;
}

@property (strong, nonatomic) IBOutlet UIImageView *bg;
@property (strong, nonatomic) IBOutlet UIImageView *searchBarBG;
@property (strong, nonatomic) IBOutlet customAutoCompleteTextField *searchBar;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchBarLeading;
@property (strong, nonatomic) IBOutlet UIButton *searchBtn;
@property (strong, nonatomic) IBOutlet UIButton *walkBtn;
@property (strong, nonatomic) IBOutlet UIButton *driveBtn;
@property (strong, nonatomic) IBOutlet UIButton *globalBtn;
@property (strong, nonatomic) IBOutlet UIView *advanceView;
@property (strong, nonatomic) IBOutlet UIImageView *advanceBG;
@property (strong, nonatomic) IBOutlet UILabel *selectDist;
@property (strong, nonatomic) IBOutlet UILabel *minDist;
@property (strong, nonatomic) IBOutlet UILabel *maxDist;
@property (strong, nonatomic) IBOutlet UIImageView *sliderBG;
@property (strong, nonatomic) IBOutlet UISlider *sliderDist;
@property (strong, nonatomic) IBOutlet UILabel *searchNear;
@property (strong, nonatomic) IBOutlet UILabel *currentKm;
@property (strong, nonatomic) IBOutlet UILabel *anotherLocation;
@property (strong, nonatomic) IBOutlet UIImageView *locationBG;
@property (strong, nonatomic) IBOutlet UIImageView *locationBarBG;
@property (strong, nonatomic) IBOutlet customAutoCompleteTextField *locationBar;
@property (strong, nonatomic) IBOutlet UIButton *locationBtn;
@property (strong, nonatomic) IBOutlet UIView *searchTypeView;
@property (strong, nonatomic) IBOutlet UILabel *searchTypeLabel;
@property (strong, nonatomic) IBOutlet UIButton *searchTypeCloseBtn;
@property (strong, nonatomic) IBOutlet UIView *topCategoryView;
@property (strong, nonatomic) IBOutlet UIImageView *topCategoryBG;
@property (strong, nonatomic) IBOutlet UICollectionView *topFoodCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *topCuisineCollectionView;
@property (strong, nonatomic) IBOutlet UIButton *topFoodBtn;
@property (strong, nonatomic) IBOutlet UILabel *topFoodLabel;
@property (strong, nonatomic) IBOutlet UIButton *topCuisineBtn;
@property (strong, nonatomic) IBOutlet UILabel *topCuisineLabel;
@property (strong, nonatomic) IBOutlet UIButton *topCuisineDoneBtn;
@property (strong, nonatomic) IBOutlet UILabel *topCuisineDoneLabel;
@property (strong, nonatomic) IBOutlet UIView *loadingView;

- (IBAction)callSettingMenu:(id)sender;
- (IBAction)searchEditing:(id)sender;
- (IBAction)searchBegin:(id)sender;
- (IBAction)searchEnd:(id)sender;
- (IBAction)touchSearch:(id)sender;
- (IBAction)touchWalk:(id)sender;
- (IBAction)touchDrive:(id)sender;
- (IBAction)touchGlobal:(id)sender;
- (IBAction)touchAdvanceClose:(id)sender;
- (IBAction)slideAdvanceDist:(id)sender;
- (IBAction)touchAdvanceKm:(id)sender;
- (IBAction)locationEditing:(id)sender;
- (IBAction)locationBegin:(id)sender;
- (IBAction)locationEnd:(id)sender;
- (IBAction)touchLocation:(id)sender;
- (IBAction)touchSearchTypeClose:(id)sender;
- (IBAction)touchTopCategory:(id)sender;
- (IBAction)touchTopCategoryClose:(id)sender;
- (IBAction)touchTopFood:(id)sender;
- (IBAction)touchTopCuisine:(id)sender;
- (IBAction)touchTopCuisineDone:(id)sender;

@end
