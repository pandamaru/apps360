//
//  searchResultListViewController.m
//  menu360
//
//  Created by PandaMaru on 6/15/2558 BE.
//  Copyright (c) 2558 apps360. All rights reserved.
//

#import "searchResultListVC.h"

@interface searchResultListVC ()

@end

@implementation searchResultListVC {
    NSMutableArray *restaurantArray;
    UITapGestureRecognizer *tapCell;
    NSInteger selectedIndex;
    
    CGFloat collectionOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Insert original image to TabBar
    UITabBarItem *mapItem = [self.tabBarController.tabBar.items objectAtIndex:1];
    mapItem.image = [[UIImage imageNamed:@"BTN_Tab_Map"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    //Add search bar shadow
    _filterBarImg.layer.shadowColor = [UIColor blackColor].CGColor;
    _filterBarImg.layer.shadowOffset = CGSizeMake(0, 0);
    _filterBarImg.layer.shadowOpacity = 1;
    _filterBarImg.layer.shadowRadius = 2.0;
    _filterBarImg.clipsToBounds = NO;
    
    [_filterBtn setImage:[[UIImage imageNamed:@"UI_Search_Icon@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [_filterBtn setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    
    _searchTypeView.layer.cornerRadius = 3;
    _searchTypeView.layer.masksToBounds = YES;
    
    [_searchTypeCloseBtn setBackgroundImage:[[appearanceSingleton Instance] imageTintedWithColor:[_searchTypeCloseBtn backgroundImageForState:UIControlStateNormal] :[UIColor whiteColor]] forState:UIControlStateNormal];
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [_bg setContentMode:UIViewContentModeScaleAspectFill];
        
        [_filterBar setDefaultTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:@"OpenSans" size:40]}];
        
        [_searchTypeLabel setFont:[UIFont fontWithName:@"OpenSans" size:35]];
    }
    
    _filterBar.delegate = self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    tapCell = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectCell:)];
    [_collectionView addGestureRecognizer:tapCell];
    
    restaurantArray = [[NSMutableArray alloc] init];
    for(NSDictionary *index in _resultJSON) {
        restaurantModel *thisModel = [[restaurantModel alloc] init];
        [thisModel initWithJSON:index];
        [restaurantArray addObject:thisModel];
    }
    
    self.tabBarController.delegate = self;
    
    //NSLog(@"%@", _SearchConditionModel.searchType);
    /*if(![_SearchConditionModel.searchType isEqual: @"all"]) {
        _searchTypeView.hidden = NO;
        
        collectionOffset = 20 + _filterBarImg.frame.size.height + 10 + _searchTypeView.frame.size.height + 5;
        
        _searchTypeLabel.text = [NSString stringWithFormat:@"%@ in %@ at Latitude %@ Longitude %@ Radius %@", _SearchConditionModel.searchText, _SearchConditionModel.searchType, _SearchConditionModel.searchLatitude, _SearchConditionModel.searchLongitude, _SearchConditionModel.searchRadius];
    } else {
        _searchTypeView.hidden = YES;
        
        collectionOffset = 20 + _filterBarImg.frame.size.height + 15;
    }*/

    _searchTypeLabel.text = [[dataSingleton Instance] getConditionFormat:_SearchConditionModel.searchText withType:[[languageSingleton Instance] getTextForKey:_SearchConditionModel.searchType] withPlace:_SearchConditionModel.searchPlace withRadius:_SearchConditionModel.searchRadius];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[sideMenuSingleton Instance] setCurrentView:self];
    [[sideMenuSingleton Instance] applyLanguageToView];
    
    [[appearanceSingleton Instance] setTabBarAppearance:2];
    
    collectionOffset = 20 + _filterBarImg.frame.size.height + 10 + _searchTypeView.frame.size.height + 5;
    
    [_collectionView reloadData];
}

/*- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUILanguage {
    UINavigationController *navCon = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1];
    
    navCon.navigationItem.title = [NSString stringWithFormat:@"%@ \"%@\"", [[languageSingleton Instance] getTextForKey:@"search"], _SearchConditionModel.searchText];
    
    /*if([self.navigationController.viewControllers count] > 1) {
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        
        navCon.navigationItem.title = [NSString stringWithFormat:@"%@ %@%@%@", [[languageSingleton Instance] getTextForKey:@"searchResultList"], @"\"", _searchText, @"\""];
    }*/
    
    _filterBar.placeholder = [[languageSingleton Instance] getTextForKey:@"filterResult"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier] isEqualToString:@"restaurantMenu"]) {
        restaurantTabBarVC *vc = [segue destinationViewController];
        //Data sending here
        vc.restaurantFood = [vc.viewControllers objectAtIndex:0];
        vc.restaurantDrink = [vc.viewControllers objectAtIndex:1];
        vc.restaurantOther = [vc.viewControllers objectAtIndex:2];
        
        vc.restaurantFood.RestaurantModel = vc.restaurantDrink.RestaurantModel = vc.restaurantOther.RestaurantModel = (restaurantModel*)[restaurantArray objectAtIndex:selectedIndex];
        
        vc.restaurantFood.SearchConditionModel = vc.restaurantDrink.SearchConditionModel = vc.restaurantOther.SearchConditionModel = [_SearchConditionModel clone];
        
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        vc.navigationItem.rightBarButtonItem = navCon.navigationItem.rightBarButtonItem;
    } if([[segue identifier] isEqualToString:@"restaurantMap"]) {
        restaurantMapVC *vc = [segue destinationViewController];
        
        vc.RestaurantModel = ((searchResultListCell*)sender).RestaurantModel;
        
        UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
        vc.navigationItem.rightBarButtonItem = navCon.navigationItem.rightBarButtonItem;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self dismissKeyboard];
    
    return  YES;
}

- (void)dismissKeyboard {
    [_filterBar resignFirstResponder];
    [_collectionView addGestureRecognizer:tapCell];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return CGSizeMake(0, collectionOffset);
    }
    
    return CGSizeMake(0, 0);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.view.frame.size.width, self.view.frame.size.width / 2.2);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 0, 5, 0); //t,l,b,r
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return restaurantArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    searchResultListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"searchResultListCell" forIndexPath:indexPath];
    restaurantModel *thisModel = (restaurantModel*)[restaurantArray objectAtIndex:indexPath.row];
    
    UIImage *nineSliceImg = [UIImage imageNamed:@"UI_Box@x2.png"];
    UIImage *resizableImg = [nineSliceImg resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [cell.cellBg setImage:resizableImg];
    
    [cell.cellMap setBackgroundImage:[[UIImage imageNamed:@"BTN_Tab_Map@x2.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateHighlighted];
    [cell.cellMap setTintColor:[[appearanceSingleton Instance] getTabBarColor]];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle = thisModel.name;
    content.contentDescription = [NSString stringWithFormat:@"%@ / %@ %@ %@ / %@\n%@", thisModel.address, thisModel.postCode, thisModel.state, thisModel.country, thisModel.type, thisModel.openinghour];
    content.contentURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%@", [[dataSingleton Instance] baseWeb], @"restaurant", thisModel.obj_id]];
    content.imageURL = [[APISingleton Instance] get_img_url:thisModel.img_path :thisModel.img[0]];
    FBSDKShareButton *shareBtn = [[FBSDKShareButton alloc] init];
    shareBtn.shareContent = content;
    
    //Extra setting for iPad
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        [cell.cellName setFont:[UIFont fontWithName:@"OpenSans-Bold" size:35]];
        [cell.cellDetail setFont:[UIFont fontWithName:@"OpenSans" size:27.5]];
        
        shareBtn.frame = CGRectMake(0, 0, shareBtn.frame.size.width * 1.3, shareBtn.frame.size.height * 1.3);
        [cell.cellShareView addSubview:shareBtn];
    } else {
        shareBtn.frame = CGRectMake(0, 0, shareBtn.frame.size.width * 0.8, shareBtn.frame.size.height * 0.55);
        [cell.cellShareView addSubview:shareBtn];
    }
    
    [cell.cellImg setImageWithURL:[[APISingleton Instance] get_img_url:thisModel.img_path :[thisModel.img[0] stringByReplacingOccurrencesOfString:@"." withString:@"_50."]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    cell.cellName.text = thisModel.name;
    cell.cellDetail.text = [NSString stringWithFormat:@"%@ / %@ %@ %@ / %@\n%@", thisModel.address, thisModel.postCode, thisModel.state, thisModel.country, thisModel.type, thisModel.openinghour];
    
    cell.parentVC = self;
    cell.RestaurantModel = thisModel;
    
    return cell;
}

-(void)selectCell:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tapLocation = [gestureRecognizer locationInView:_collectionView];
        NSIndexPath *tapLocationIndexPath = [_collectionView indexPathForItemAtPoint:tapLocation];
        
        if(tapLocationIndexPath != nil) {
            selectedIndex = tapLocationIndexPath.row;
            //NSLog(@"%d", selectedIndex);
            [self performSegueWithIdentifier:@"restaurantMenu" sender:self];
        }
    }
}

- (IBAction)filterChange:(id)sender {
    [restaurantArray removeAllObjects];
    
    BOOL defaultResult = [_filterBar.text isEqual: @""];
    
    for(NSDictionary *index in _resultJSON) {
        if([[index[@"name"] lowercaseString] containsString:[_filterBar.text lowercaseString]] || defaultResult) {
            restaurantModel *thisModel = [[restaurantModel alloc] init];
            [thisModel initWithJSON:index];
            [restaurantArray addObject:thisModel];
        }
    }
    
    [_collectionView reloadData];
}

- (IBAction)filterBegin:(id)sender {
    [_collectionView removeGestureRecognizer:tapCell];
}

- (IBAction)touchFilter:(id)sender {
    //Just in case
}

- (IBAction)touchSearchTypeClose:(id)sender {
    _SearchConditionModel.searchText = @"";
    _SearchConditionModel.searchType = @"";
    
    _searchTypeLabel.text = [[dataSingleton Instance] getUnconditionFormatWithPlace:_SearchConditionModel.searchPlace withRadius:_SearchConditionModel.searchRadius];
    
    _searchTypeCloseBtn.hidden = YES;
    
    [restaurantArray removeAllObjects];
    
    [[APISingleton Instance] multiple_restaurant_details:_SearchConditionModel.searchPossibleKey :@"en" :YES :^(id json) {
        if([json[@"status_code"] isEqual: @"200"]) {
            if([json[@"results"] count] > 0) {
                for(NSDictionary *index in json[@"results"]) {
                    restaurantModel *thisModel = [[restaurantModel alloc] init];
                    [thisModel initWithJSON:index];
                    [restaurantArray addObject:thisModel];
                }
            } else {
                [SVProgressHUD showErrorWithStatus:[[languageSingleton Instance] getTextForKey:@"noSearchResult"] maskType:SVProgressHUDMaskTypeBlack];
            }
        } else {
            [SVProgressHUD showErrorWithStatus:json[@"status_msg"] maskType:SVProgressHUDMaskTypeBlack];
        }
        
        [_collectionView reloadData];
    }];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if(viewController == [tabBarController.viewControllers objectAtIndex:1]) {
        searchResultPinVC *vc = (searchResultPinVC*)viewController;
        
        vc.searchText = _searchText;
        vc.resultArray = restaurantArray;
    }
    
    return YES;
}

@end
